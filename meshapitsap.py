# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
TSAP commands

Created on 8.9.2017

'''
from collections import OrderedDict
from meshapicommand import MeshApiCommand, MeshApiException
from meshapiindication import MeshApiIndication

class TsapTestModeEnterResult:
    # Success: Test mode entered successfully
    TSAP_ENTER_TEST_MODE_SUCCESS = 0
    # Failure: Given network address is not valid */
    TSAP_ENTER_TEST_MODE_NWK_ADDRESS_INVALID = 1
    # Failure: Stack is running, it should be stopped first before
    #  enabling test mode to avoid conflicting radio access. */
    TSAP_ENTER_TEST_MODE_REJECTED = 2

class TsapTestModeExitResult:
    # Success: Test mode exited successfully
    TSAP_EXIT_TEST_MODE_SUCCESS     = 0
    # Failure: Device is not in test mode
    TSAP_EXIT_TEST_MODE_REJECTED    = 1

class TsapSetRadioChannelResult:
    # Success:  Radio channel set successfully
    TSAP_RADIO_CHANNEL_SUCCESS  = 0
    # Failure: Given channel number is not valid
    TSAP_RADIO_CHANNEL_INVALID  = 1
    # Failure: Device was not in test mode
    TSAP_RADIO_CHANNEL_REJECTED = 2

class TsapSetRadioTXPowerResult:
    # Success: Radio power level set successfully
    TSAP_RADIO_TX_POWER_SUCCESS     = 0
    # Failure: Given power level is not valid
    TSAP_RADIO_TX_POWER_INVALID     = 1
    #Failure: Device was not in test mode
    TSAP_RADIO_TX_POWER_REJECTED    = 2

class TsapSendDataResult:
    # Success: Date sent successfully
    TSAP_RADIO_DATA_SEND_SUCCESS    = 0
    # Failure: Sending failed due to busy channel (CCA fail)
    TSAP_RADIO_DATA_SEND_CCA_FAIL   = 1
    # Failure: Transmission failed, check parameters
    TSAP_RADIO_DATA_SEND_FAILED     = 2
    # Failure: Device was not in test mode
    TSAP_RADIO_DATA_SEND_REJECTED   = 3

class TsapEnableReceptionResult:
    #Success: Reception enabled/disabled succesfully
    TSAP_RADIO_RECEPTION_SET_SUCCESS  = 0
    #Failure: Device was not in test mode
    TSAP_RADIO_RECEPTION_SET_REJECTED = 1

class TsapReadDataReadResult:
    #Success: Test data read from the buffer
    TSAP_RADIO_DATA_READ_SUCCESS    = 0
    # Failure: No received test data in buffer
    TSAP_RADIO_DATA_READ_NO_DATA    = 1
    # Failure: Device was not in test mode
    TSAP_RADIO_READ_DATA_REJECTED   = 2

class TsapReadMaxDataSizeResult:
    # Maximum data size read successfully
    TSAP_RADIO_DATA_SIZE_READ_SUCCESS   = 0
    # Failure: Device was not in test mode
    TSAP_RADIO_DATA_SIZE_READ_REJECTED  = 1

class TsapTestModeEnter(MeshApiCommand):
    '''
    Composes "test mode enter" command
    '''
    def __init__(self , networkAddress):
        '''
        Initializes a new instance of TsapTestModeEnter class
        '''

        super(TsapTestModeEnter, self).__init__()
        self.primitive_id = 0x43
        self.request['nwkAddr'] = (networkAddress, 'uint32')

        self.confirmation['result'] = (None, 'uint8')

class TsapTestModeExit(MeshApiCommand):
    '''
    Composes "test mode exit" command
    '''
    def __init__(self ):
        '''
        Initializes a new instance of TsapTestModeExit class
        '''

        super(TsapTestModeExit, self).__init__()
        self.primitive_id = 0x44

        self.confirmation['result'] = (None, 'uint8')

class TsapSetRadioChannel(MeshApiCommand):
    '''
    Composes "set channel" command
    '''
    def __init__(self, channel ):
        '''
        Initializes a new instance of TsapSetRadioChannel class
        '''

        super(TsapSetRadioChannel, self).__init__()
        self.primitive_id = 0x45
        self.request['channel'] = (channel, 'uint8')

        self.confirmation['result'] = (None, 'uint8')

class TsapSetRadioTXPower(MeshApiCommand):
    '''
    Composes "set radio TX power" command
    '''
    def __init__(self, channel ):
        '''
        Initializes a new instance of TsapSetRadioTXPower class
        '''

        super(TsapSetRadioTXPower, self).__init__()
        self.primitive_id = 0x46
        self.request['txpower'] = (channel, 'uint8')

        self.confirmation['result'] = (None, 'uint8')

class TsapSendData(MeshApiCommand):
    '''
    Composes "send data" command
    '''
    def __init__(self, dataLen, data, seq=0, bursts=1, cca_duration=0):
        '''
        Initializes a new instance of TsapSendData class
        '''

        super(TsapSendData, self).__init__()
        self.primitive_id = 0x47
        self.request['bursts'] = (bursts, 'uint32')
        self.request['cca_duration'] = (cca_duration, 'uint32')
        self.request['seq'] = (seq, 'uint32')
        self.request['dataLen'] = (dataLen, 'length uint8')
        self.request['data'] = (data,'string')
        self.confirmation['result'] = (None, 'uint8')
        self.confirmation['sendBurst'] = (None, 'uint32')

class TsapGetMaxDataSize(MeshApiCommand):
    '''
    Composes "get max data size" command
    '''
    def __init__(self):
        '''
        Initializes a new instance of TsapGetMaxDataSize class
        '''

        super(TsapGetMaxDataSize, self).__init__()
        self.primitive_id = 0x4A
        self.confirmation['result'] = (None, 'uint8')
        #self.confirmation['dataLen'] = (None, 'length uint8')
        self.confirmation['size'] = (None, 'uint8')

class TsapAllowReception(MeshApiCommand):
    '''
    Composes "allow receptiopn" command
    '''
    def __init__(self, rxEnabled, dataIndiEnabled):
        '''
        Initializes a new instance of TsapGetMaxDataSize class
        '''

        super(TsapAllowReception, self).__init__()
        self.primitive_id = 0x48
        self.request['rxEnabled'] = (rxEnabled, 'uint8')
        self.request['dataIndiEnabled'] = (dataIndiEnabled, 'uint8')
        self.confirmation['result'] = (None, 'uint8')

class TsapReadTestData(MeshApiCommand):
    '''
    Composes "test data read" command
    '''
    def __init__(self):
        '''
        Initializes a new instance of TsapReadTestData class
        '''

        super(TsapReadTestData, self).__init__()
        self.primitive_id = 0x4B
        self.confirmation['result'] = (None, 'uint8')
        self.confirmation['rx_counter'] = (None, 'uint32')
        self.confirmation['duplicates'] = (None, 'uint32')
        self.confirmation['sequence_number'] = (None, 'uint32')
        self.confirmation['data_length'] = (None, 'length uint8')
        self.confirmation['data'] = (None, 'string')

class TsapDataRx(MeshApiIndication):
    '''
    This class is for the TSAP-DATA_RX indication parsing.

    Checks the source_endpoint and dest_endpoint to match with the parsed
    datapacket when parse_indication is run. Also automatically parses
    the payload if it is defined in the self.payload ordered dictionary.
    '''

    def __init__(self):
        '''
        The __init__ function.

        '''

        super(TsapDataRx, self).__init__()

        self.primitive_id = 0x49

        self.indication['rx_counter'] = (None, 'uint32')
        self.indication['duplicates'] = (None, 'uint32')
        self.indication['sequence_number'] = (None, 'uint32')
        self.indication['data_length'] = (None, 'length uint8')
        self.indication['data'] = (None, 'string')

        # This is a ordered dictionary for parsing the payload
        self.payload = OrderedDict()

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        '''
        Parse DataRx specific indication.

        Parses also the payload according to self.payload ordered
        dictionary if it has been defined.

        Args:

        - primitive_id: The Dual-MCU API primitive ID for this frame
        - frame_id: The frame ID
        - payload_length: Payload length for the whole frame
        - payload: The frame payload
        - compare: True if compared to original values during decoding,
          False otherwise

        Returns: A tuple containing two booleans. First value is True
        if TsapDataRx could be parsed, otherwise False.
        The second is True only if the indication should be handled only once by
        the CommInterface receiving it.

        '''
        try:
            [a, b] = super(TsapDataRx, self).parse_indication(primitive_id,
                             frame_id,
                             payload_length,
                             payload,
                             compare)
            # Parse TsapDataRx specific metadata
            if a:
                # Parse the payload using payload dictionary if available.
                a = self._parse_payload()

            # Pass on the return values.
            return a, b

        except MeshApiException, e:
            # Parsing caused an exception, so set as could not parse.
            return False, False

    def _parse_payload(self):
        '''
        Parse the payload based on self.payload ordered dictionary
        and store results in self.indication.

        Returns:
        - True: Parsed successfully (or not at all)
        - False: Parsing failed.
        '''
        if len(self.payload) == 0:
            return True
        try:
            if self._generic_parse(self.payload, self.indication['apdu'][0]):
                # Copy these to actual indication values
                self.indication.update(self.payload)
            return True
        except:
            return False
