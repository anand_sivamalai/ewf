# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

This is the DsapUDPinterface interface class that is inherited from the
SerialInterface class.

Created on 23.11.2016

'''

from configuration import config
import serialinterface
import debug
import socket
import binascii
import json
from threading import Thread, current_thread
from Queue import Queue, Empty
from comminterface import CommInterface
from framecodec import Framecodec, FramecodecException
from struct import *
from meshapicsap import *
import  threading
import crc16

class FrameCodecUdp(Framecodec):
    frame_id = 0

    itemlist = list()
    debug_print_queue = Queue()

    def __init__(self):
        super(Framecodec, self).__init__()
        self.rx_data = ""

    def encode(self, msg):
        '''
        Encode a message into SLIP frame for UART transmission. Adds CRC.

        Parameters:
        msg - The message as MeshApiCommand instance.

        Returns: The encoded SLIP frame as bytearray.
        '''

        # store item
        self.itemlist.append(msg)

        # Use the messages encode instance to convert the message to bytearray.
        frame = msg.encode(self.frame_id)

        # Insert CRC.
        crc = crc16.crc16xmodem(frame, 0xffff)
        crcframe = pack('<H', crc)

        frame = frame + crcframe

        # Increase frame ID.
        self.frame_id = (self.frame_id + 1) & 0xff

        # Return the message inside a SLIP frame.
        return frame

    def decode(self, frame):
        '''
        Dummy
        '''
        raise NotImplemented

    def get_item(self):

        if len(self.itemlist) > 0:
            return self.itemlist.pop()

        return None



INIT_MSG = 'Initiate Diagnostics Connection'
WAIT_MSG = 'waiting to receive'
ACK_MSG  = 'ACK_'
MAX_MSG_SIZE = 200

def find_devices():
    devices = []

    debug.info("Finding DsapUDP devices..")

    if hasattr(config, 'udp_hosts'):
        hosts = config.udp_hosts


    #IPV6 format [1fff:0:a88:85a3::ac1f]:10000
    for host in hosts:
        (address, port) = host[1:].split(']')
        devices.append(DsapUDPinterface(address, int(port[1:])))

    debug.info('Found '+str(len(devices))+' serial over DsapUDPinterface devices.')

    return devices


class DsapUDPinterface(serialinterface.SerialInterface):


    '''
    A class for UDP Interface.
    Inherited from the SerialInterface class.
    '''
    # Is the port connected?
    connected = False

    # The UDP socket
    sock = None
    # Server address (IPv6)
    server_address = None

    #sink configuration received in handshake string
    node_address = None
    network_address = None

    frames_received = 0
    frames_acked = 0

    def __init__(self, address, port):

        CommInterface.__init__(self)

        self.server_address = (address, port)

        # Store parent thread information.
        self.parent_thread = current_thread()

        self.debug_info("Creating interface, address: "+address+", port "+str(port))

        try:
            # Create a UDP socket
            self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)

        except socket.error:
            self.debug_info("Could not connect")
            return


        # Queues for starting the RX & TX threads so they don't need to busy wait.
        self.rx_start_queue = Queue()
        self.tx_start_queue = Queue()

        # Start this thread
        self.start()

        # Open the port
        self.open()

    def open(self):
        '''
        A method to open the interface.
        '''

        self.debug_info("Opening the "+str(self))
        if self.connected:
            self.debug_info("Serial over UDP port already open")
            return

        # Border Router expects simple handshake
        # Send data
        sent = self.sock.sendto(INIT_MSG, self.server_address)

        #wait for sink configuration
        data, server = self.sock.recvfrom(MAX_MSG_SIZE)

        print 'CONFIG:' + data

        sink_config = json.loads(data)

        self.node_address    = str(sink_config["node_address"])
        self.network_address = str(sink_config["network_address"])

        #self.debug_info(str(data))
        #self.debug_info(len(data))


        # Start a dummy framecodec.
        self.framecodec = FrameCodecUdp()

        # Inform the RX and TX threads that they can switch mode.
        self.rx_start_queue.put(1)
        self.tx_start_queue.put(1)

        self.connected = True

        CommInterface.open(self)


    def get_connected(self):
        '''
        Just to get connection status.

        - Returns True if connection is open, False if not.
        '''
        return True;
        #return self.connected

    def is_local(self):
        '''
        To check if connection is local (not over TCP/IP or similar).

        - Return True if connection is local, False if not.
        '''

        return False

    def kill(self):
        '''
        Kill the interface.

        Kills all threads. After this the interface cannot be reopened.
        '''
        #print 'UDP IF: KILL'
        CommInterface.kill(self)

        try:
            self.sock.shutdown(socket.SHUT_RDWR)
            self.sock.close()
            self.sock = None
        except:
            self.debug_info("Closing the UDP port failed!")


    def get_host_name(self):
        '''
        Get the host name.

        Returns the host name.
        '''

        return self.server_address[0]

    def get_port(self):
        '''
        Get the UDP port number.

        Returns the port number.
        '''

        return self.server_address[1]

    def __repr__(self):
        return 'UDP Port'# ['+ str(self.server_address[0]) + ']:'# +str(self.server_address[1])

    def _send_ack(self):

        sent = self.sock.sendto(ACK_MSG + str(self.frames_acked), self.server_address)
        #print 'Send: ' + ACK_MSG + str(self.frames_acked + 1)
        self.frames_acked = self.frames_acked + 1

    def _read_bytes(self):
        '''
        Reads data from UDP port.

        - Returns the data in a bytearray.
          If no data was available then an empty bytearray is returned.
        '''
        LOCK = threading.Lock()
        try:

            LOCK.acquire(True)
            #print 'Wait Indication RX:' + str(self.frames_received) + '/ACK:' + str(self.frames_acked) + '\n'
            udp_data, server = self.sock.recvfrom(MAX_MSG_SIZE)
            #print 'Got:' + str(len(udp_data)) + 'bytes'
            self.frames_received  = self.frames_received + 1
            self._send_ack()
            LOCK.release()

            data = bytearray(udp_data)

            if len(data) == 0:
                # Other end probably closed connection.
                raise socket.error
            else:
                self.debug_info("Socket read: %s" % binascii.hexlify(data))
        except socket.timeout:
           # Timed out, ignore.
            print '### Timeout Error'
            return bytearray()
        except socket.error:
           # Error, close connection.
            print '### Socket Error'
            self.sock.close()

            return bytearray()

        return data

    def _send_frame(self, frame):

        #print 'UDP: _send_frame() suppressed\n'
        return 0

    def _generate_response(self):
        '''
        Generates response CsapAttributeRead requests basend on cached data.
        UDP Interface is uni-directonal, so it's read-only.
        '''
        reply = None

        item = self.framecodec.get_item()

        if isinstance(item, MeshApiCommand) and item  is not None and item.primitive_id != 0x3:

            #fake response
            attrib_id = item.request['attribute_id'][0]

            if( attrib_id == CsapAttribId.NodeRole):
                reply = bytearray([item.primitive_id | 0x80, item.frame_id, 5, 0, attrib_id,0,1,1])
            elif ( attrib_id == CsapAttribId.NodeAddress):
                tmp = bytearray(pack('I', int(self.node_address)))
                reply = bytearray([item.primitive_id | 0x80, item.frame_id, 8, 0, attrib_id, 0, 4, tmp[0],tmp[1],tmp[2],tmp[3]])
            elif ( attrib_id == CsapAttribId.NetworkAddress):
                tmp = bytearray(pack('I', int(self.network_address)))
                reply = bytearray([item.primitive_id | 0x80, item.frame_id, 8, 0, attrib_id, 0, 4, tmp[0],tmp[1],tmp[2],tmp[3]])

        return reply


    def read_response(self):
        '''
        Reads a response from serial port and decodes it. Waits until a full frame is collected.

        - Returns the decoded frame.
        '''

        retval = None

        #get cached tx item from dummy frame codec
        retval = self._generate_response()

        while retval is None and self.connected:
            retval = self._read_bytes()

        # Return the frame.
        return retval


    def get_network_address(self, timeout=30):
        '''
        Returns the node address received on connect.

        - timeout: dummy

        Returns: The response frame.
        '''

        return int(self.network_address)


    def get_address(self, timeout=30):
        '''
        Returns the node address after reading it from the device.

        Attributes:

        - timeout: dummy

         Return the address in integer
        '''
        return int(self.node_address)
