# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
BT beacon data commands

Created 16.11.2016

'''

import struct
import binascii
from collections import OrderedDict

# Various endpoints
BEACON_ENDPOINT_REQUEST_SRC = 255
BEACON_ENDPOINT_REQUEST_DST = 242
BEACON_ENDPOINT_RESPONSE_SRC = 242
BEACON_ENDPOINT_RESPONSE_DST = 255

# The length of the UUID
uuid_length_ibeacon =16

class ModeValue:
    Disable = 0
    TX = 1
    RX = 2

class PhyConfiguration:
    BlePhy = 1

class BeaconMessageType:
    Mode = 0x01         # "B": unsigned 8-bit value
    Response_Mode = 0x81
    Phy_Config =0x02     # "B": unsigned 8-bit value
    Response_Phy_Config =0x82
    Read_Phy_Config =0x03  # "": no value
    Response_Read_Phy_Config = 0x83
    Tx_Interval = 0x04     # "H": unsigned 16-bit value
    Response_Tx_Interval = 0x84
    Tx_Channel_List = 0x05  # "B": unsigned 8-bit value
    Response_Tx_Channel_List = 0x85
    Tx_Power = 0x06       # "b": signed 8-bit value
    Response_Tx_Power = 0x86
    Tx_Payload =0x07      # Bd   "d": bytearray
    Response_Tx_Payload = 0x87
    Read_Tx_Config = 0x08   # "": no value
    Response_Read_Tx_Config = 0x88
    # TBD = 0xNN   not implemented yet
    Status_Interval =0x09    # "H": unsigned 16-bit value
    Response_Status_Interval = 0x89
    Read_Status =0x0A       # "": no value
    Response_Read_Status = 0x8A
    Status = 0x8B
    NoSpaceForResponse = 0xFC
    InvalidValue = 0xFD
    InvalidLength = 0xFE
    UnknownRequest = 0xFF

class PRPCBeacon:
    channels = {
        "37":       0,
        "38":       1,
        "39":       2,
        "37+38":    3,
        "37+39":    4,
        "38+39":    5,
        "37+38+39": 6,
        "all":      6
    }

# The basic beacon command
class BeaconCommand(object):
    '''
    The abstract class for beacon command, all specific commands class can be inherited from this class
    '''
    def __init__(self):
        self.command = OrderedDict()
        self.messagetype = None
        self.response = OrderedDict()
        self.responsemessagetype = None

    def _desc_to_format(self, desc):
        '''

        Args:
            desc: the format of the data

        Returns: the format for packing and unpacking

        '''
        d = {'uint8':'B',
             'int8':'b',
             'uint16':'H',
             'uint32':'I',
             'string':'s',
             'uint24':'3s'
             }

        payload_fmt = d[desc]
        return payload_fmt

    def _desc_to_len(self, desc):
        '''

        Args:
            desc: the format of the data

        Returns: the length of the data

        '''
        d = {'uint8':1,
             'int8':1,
             'uint16':2,
             'uint32':4,
             'uint24':3,
             }

        length = d[desc]
        return length

    def encode(self, messagetype):
        '''

        Args:
            messagetype: the message type

        Returns: the encoded message

        '''
        return self.encode_message(messagetype,self.command)

    def encode_message(self, messagetype, dict):
        '''

        Args:
            messagetype: the message type
            dict: the dictionary which is used for encode

        Returns: the encoded string message

        '''
        # Check whether the message has value
        hasvalue = False
        for key in dict:
            if key == "value":
                hasvalue =True
                value = dict['value']
                break
        #If no value the msg has only message type and length(00)
        #Else the msg has message type and length and value
        if hasvalue == False:
            values = bytearray()
        # elif hasvalue == True and dict['format'] =="int8":
        #     values = str(value)
        elif hasvalue == True and dict['format'] =="string":
            values = value
        else:
            values = struct.pack("<" + self._desc_to_format(dict['format']), value)

        header = struct.pack("<BB", messagetype, len(values))

        byteresult =  bytearray(header + values)
        return str(byteresult)

    def decode_response(self,responsedata):
        '''

        Args:
            responsedata: the message which needs to be decoded

        Returns: the length of the currect message

        '''

        singlemessagelength = self.parse_message(self.response,responsedata)
        return singlemessagelength

    def parse_message(self, dict, singleresponse):
        '''

        Args:
            dict: the dictionary which is used for decoding
            singleresponse: the message which is needed to be decoded

        Returns: the length of the message which is decoded by the dictionary

        '''
        length = 0
        payload_fmt = '<'

        # First to get the message length , and the pack format
        for value in dict.itervalues():
            if not value[1].startswith('string'):
                packformat = self._desc_to_format(value[1])
                length += self._desc_to_len(value[1])
            else:
                string_length = int(value[1].split()[1])
                packformat = str(string_length)+'s'
                length += string_length
            payload_fmt = payload_fmt + packformat

        messagelength = length +1
        singleresponse = singleresponse[1:messagelength]

        try:
            unpacked = struct.unpack(payload_fmt, singleresponse)
        except:
            raise BeaconDecodeException("Parse Error: Invalid payload. (format: "+payload_fmt+", payload: "+binascii.hexlify(singleresponse)+")")

        i = 0
        for key, value in dict.iteritems():

            if len(unpacked) <= i:
                dict[key] = (None, value[1])
            elif value[1] == 'uint24':
                # Convert 24-bit values back
                dict[key] = (struct.unpack('<I', unpacked[i] + '\x00')[0], value[1])
            else:
                dict[key] = (unpacked[i], value[1])
            i = i + 1

        return messagelength

class Mode(BeaconCommand):
    '''
    The command mode ----PRPC PHY CONFIGURATION
    '''
    def __init__(self,
                 value =0,
                 value_format = 'uint8'
                 ):
        super(Mode, self).__init__()
        self.messagetype = BeaconMessageType.Mode
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint8')


class PhyConfig(BeaconCommand):
    '''
     The command phy config ----PRPC PHY CONFIGURATION
    '''
    def __init__(self,
                 value=0,
                 value_format = 'uint8'
                 ):
        super(PhyConfig, self).__init__()
        self.messagetype = BeaconMessageType.Phy_Config
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint8')


class ReadPRPCPhyConfig(BeaconCommand):
    '''
     Read PRPC phy config  ----PRPC PHY CONFIGURATION
    '''
    def __init__(self
                 ):
        super(ReadPRPCPhyConfig, self).__init__()
        self.messagetype = BeaconMessageType.Read_Phy_Config

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')


class FrameTXInterval(BeaconCommand):
    '''
     The command ro set the tx interval ---PRPC TX CONFIGURATION
    '''
    def __init__(self,
                 value =0,
                 value_format = 'uint16'
                 ):
        super(FrameTXInterval, self).__init__()
        self.messagetype = BeaconMessageType.Tx_Interval
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint16')


class TXChannelList(BeaconCommand):
    '''
     The request to set the channel  ---PRPC TX CONFIGURATION
    '''
    def __init__(self,
                 value =0,
                 value_format = 'uint8'
                 ):
        super(TXChannelList, self).__init__()
        self.messagetype = BeaconMessageType.Tx_Channel_List
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint8')


class TXPower(BeaconCommand):
    '''
     The request to set the tx power  ---PRPC TX CONFIGURATION
    '''
    def __init__(self,
                 value =0,
                 value_format = 'int8'
                 ):
        super(TXPower, self).__init__()
        self.messagetype = BeaconMessageType.Tx_Power
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'int8')


class PayloadItem(BeaconCommand):
    '''
     The request for set the payload  ---PRPC TX CONFIGURATION
    '''
    def __init__(self,
                 value =0,
                 value_format = 'string'):
        super(PayloadItem, self).__init__()
        self.messagetype = BeaconMessageType.Tx_Payload
        self.command['value'] = value
        self.command['format'] =value_format

        # Seems there is some problem with the format
        # for i in range(1,8):
        #     self.responsemessagetype = None
        #     self.response['response_length_{}'.format(i)] = (None, 'uint8')
        #     self.response['response_index_{}'.format(i)] = (None, 'uint8')
        #     self.response['response_payloadlength_{}'.format((i))] =(None, 'uint8')

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_index'] = (None, 'uint8')
        self.response['response_payloadlength'] =(None, 'uint8')


class ReadPRPCTXConfiguration(BeaconCommand):
    '''
     The request for read PRPC TX configuration  ---PRPC TX CONFIGURATION
    '''
    def __init__(self
                 ):
        super(ReadPRPCTXConfiguration, self).__init__()
        self.messagetype = BeaconMessageType.Read_Tx_Config

        self.responsemessagetype = None
        self.response['response_length'] =(None, 'uint8')


class StatusInterval(BeaconCommand):
    '''
     The request for get the node status interval --- NODE STATUS
    '''
    def __init__(self,
                 value=0,
                 value_format='uint16'
                 ):
        super(StatusInterval, self).__init__()
        self.messagetype = BeaconMessageType.Response_Status_Interval
        self.command['value'] = value
        self.command['format'] =value_format

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint16')


class ReadStatus(BeaconCommand):
    '''
     The request for read the status --- NODE STATUS
    '''
    def __init__(self
                 ):
        super(ReadStatus, self).__init__()
        self.messagetype = BeaconMessageType.Read_Status

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')

class Error(BeaconCommand):
    '''
    the error command
    '''
    def __init__(self
                 ):
        super(Error, self).__init__()

        self.responsemessagetype = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_command'] = (None, 'uint8')

class UnknownID(BeaconCommand):
    '''
    the unknown id command
    '''
    def __init__(self,
                 commandid=77,
                 value =0,
                 value_format='int8'
                 ):
        super(UnknownID, self).__init__()
        self.messagetype = commandid
        self.command['value'] = value
        self.command['format'] =value_format


class Status(BeaconCommand):
    '''
     The periodic message for the node status --- NODE STATUS
    '''
    def __init__(self
                 ):
        super(Status, self).__init__()

        self.responsemessagetype = None
        self.response['length'] = (None, 'uint8')
        self.response['value'] = (None, 'uint8')

class BeaconCommandMessage(BeaconCommand):
    '''
    A class for the beacon payload and response, inherit from BeaconCommand
    '''

    # A selection of possible Eddystone-URL prefixes
    eddystone_url_prefixes = [
        "http://www.",
        "https://www.",
        "http://",
        "https://"
    ]

    # Eddystone-URL text substitutions for common URL elements
    eddystone_url_substitutions = [
        ("\x00", ".com/"),
        ("\x01", ".org/"),
        ("\x02", ".edu/"),
        ("\x03", ".net/"),
        ("\x04", ".info/"),
        ("\x05", ".biz/"),
        ("\x06", ".gov/"),
        ("\x07", ".com"),
        ("\x08", ".org"),
        ("\x09", ".edu"),
        ("\x0a", ".net"),
        ("\x0b", ".info"),
        ("\x0c", ".biz"),
        ("\x0d", ".gov")
    ]

    def create_eddystone_url_beacon(self, index = None, device_address = None, measured_power = None, url = None ):
        '''

        Args:
            index: the payload index
            device_address:  the device address
            measured_power:  the power
            url: the url in eddystone beacon

        Returns: the eddystone beacon command

        '''

        self.eddystone_url_prefixes
        self.eddystone_url_substitutions

        if measured_power is None and url is None:
            data = bytearray([index])
        else:

            # Figure out the URL prefix.
            url_lower = url.lower()
            for n in range(len(self.eddystone_url_prefixes)):
                if url_lower.startswith(self.eddystone_url_prefixes[n]):
                    url_prefix = n
                    url = url[len(self.eddystone_url_prefixes[n]):]
                    break
            # else:
            #     raise ValueError("invalid URL prefix")

            # Do text substitutions to make the URL smaller.
            for s in self.eddystone_url_substitutions:
                url = url.replace(s[1], s[0])

            # Convert 48-bit device address to a list of six byte values.
            try:
                device_address = struct.pack("<Q", device_address)          # 8 chars
                device_address = struct.unpack("<8B", device_address)[:6]   # 6 values
                device_address = list(device_address)
            except struct.error, e:
                raise ValueError(str(e))

            # Convert signed power measurement value to an unsigned byte.
            measured_power &= 0xff
            index = struct.pack("<B", index)

            data = bytearray(
                [index] +
                [0x42] +                    # ADV_NONCONN_IND with random device address
                device_address +            # Device address
                [0x02, 0x01, 0x04] +        # AD flags: LE only, not discoverable
                [0x03, 0x03, 0xaa, 0xfe] +  # Eddystone Service UUID
                [len(url) + 6] +            # Length of bytes to follow
                [0x16, 0xaa, 0xfe] +        # Eddystone Service Data Type
                [0x10] +                    # Eddystone Frame Type: URL
                [measured_power] +          # Measured power at 0 meter
                [url_prefix] +              # URL prefix: "http://", etc.
                [ord(c) for c in url])      # URL

            # Check resulting data length.
            #
            # A Bluetooth LE advertising PDU is at most 39 bytes long. The radio
            # hardware adds the length byte automatically, so a maximum of 38 bytes
            # can be set using PRPC.
            # if len(data) > 39:
            #     raise ValueError("URL too long")

        return str(data)

    def create_le_advertisement(self, index=None, device_address=None, flag=None, payload=None):

        '''

        Args:
            index: the payload index
            device_address:  the device address
            flag:  the flag for the payload
            payload: the payload

        Returns: the le advertisement command

        '''


        # Convert 48-bit device address to a list of six byte values.
        try:
            device_address = struct.pack("<Q", device_address)          # 8 chars
            device_address = struct.unpack("<8B", device_address)[:6]   # 6 values
            device_address = list(device_address)
        except struct.error, e:
            raise ValueError(str(e))

        if flag is None:
            flag = 0x42
        else:
            flag = flag

        if payload is None:
            data = bytearray(
                [index] +
                [flag] +                    # ADV_NONCONN_IND with random device address
                device_address             # Device address
                )
        else:
            data = bytearray(
                [index] +
                [flag] +                    # ADV_NONCONN_IND with random device address
                device_address +            # Device address
                [ord(c) for c in payload])

        return str(data)

    def create_ibeacon(self, index=None, device_address=None, uuid=None, major=None, minor=None, measured_power=None):
        '''

        Args:
            index: the payload index
            device_address:  the device address
            uuid:  the UUID of the beacon
            major: The major version of the beacon, if None, the value is from addreee
            minor: The minor version of the beacon, if None, the value is from addreee
            measured_power: the power

        Returns: the ibeacon command

        '''
        if device_address is None:
            raise ValueError("please set the device address")
        if measured_power is None:
            measured_power = 0
        if index is None:
            index = 0

        # The address's small endian but uuid and version should be big endian 
        try:
            address_tmp_big_endian = struct.pack(">Q", device_address) 
            address_tmp_small_endian = struct.pack("<Q", device_address)
            address = list(address_tmp_small_endian)[:6]
        except struct.error, e:
            raise ValueError(str(e))

        # Convert signed power measurement value to an unsigned byte.
        measured_power &= 0xff
        index = struct.pack("<B", index)

        # Convertthe version, if Null it comes from the address, if the user gives version it will take the user input
        # The major and minor should be big endian
        if major is None:
            try:
                major=list(address_tmp_big_endian[-4:-2])
            except struct.error, e:
                raise ValueError(str(e))
        else:
            major=list(struct.pack('>H', major))

        if minor is None:
            try:
                minor=list(address_tmp_big_endian[-2:])
            except struct.error, e:
                raise ValueError(str(e))
        else:
            minor=list(struct.pack('>H', minor))

        # Set UUID , the UUID should be big endian
        if uuid is None:
            try:
                uuid = (uuid_length_ibeacon-len(address_tmp_big_endian[2:]))\
                       *'\0'+address_tmp_big_endian[2:]
            except struct.error, e:
                raise ValueError(str(e))

        else:
            uuid=binascii.unhexlify(uuid)
            if len(uuid) != uuid_length_ibeacon:
                raise ValueError("the UUID in iBeacon should be 16 bytes")

        data = bytearray(
            [index] +
            [0x42] +                    # ADV_NONCONN_IND with random device address
             address +                  # Device address
            [0x02, 0x01, 0x04] +        # AD flags: LE only, not discoverable
            [0x1a, 0xff] +              # length=0x1a=26, ff is data type Manufacturer Specific Data
            [0x4c, 0x00] +              # Company id (LSB,MSB): no name Bluetooth Sig ID
            [0x02, 0x15] +              # iBeacon advertisement indicator
            [ord(i) for i in uuid] +    # UUID
             major       +              # Major version
             minor       +              # Minor version
            [measured_power]            # Measured power at 0 meter
             )

        return str(data)

    def generate_mode_command(self, value, value_format):
        '''
        To generate the mode command
        '''
        msg = Mode(value, value_format)
        command = msg.encode(0x01)
        return command

    def generate_phy_config(self, value, value_format):
        '''
        To generate the phy config command
        '''
        msg = PhyConfig(value, value_format)
        command = msg.encode(0x02)
        return command

    def generate_read_prpc_phy_config(self):
        '''
        To generate the read prpc phy config command
        '''
        msg = ReadPRPCPhyConfig()
        command = msg.encode(0x03)
        return command

    def generate_frame_tx_interval(self, value, value_format):
        '''
        To generate the Frame tx interval command
        '''
        msg = FrameTXInterval(value, value_format)
        command = msg.encode(0x04)
        return command

    def generate_tx_channel_list(self, value, value_format):
        '''
        To generate the tx channel list command
        '''
        msg = TXChannelList(value, value_format)
        command = msg.encode(0x05)
        return command

    def generate_tx_power(self, value, value_format):
        '''
        To generate the tx power command
        '''
        msg = TXPower(value, value_format)
        command = msg.encode(0x06)
        return command

    def generate_payload(self, value, value_format):
        '''
        To generate the payload
        '''
        msg = PayloadItem(value, value_format)
        command = msg.encode(0x07)
        return command

    def generate_read_prpc_tx_config(self):
        '''
        To generate the read prpc tx config command
        '''
        msg = ReadPRPCTXConfiguration()
        command = msg.encode(0x08)
        return command

    def generate_status_interval(self, value, value_format):
        '''
        To generate the status interval command
        '''
        msg = StatusInterval(value, value_format)
        command = msg.encode(0x09)
        return command

    def generate_read_status(self):
        '''
        To generate the read status command
        '''
        msg = ReadStatus()
        command = msg.encode(0x0A)
        return command

    def generate_status(self, value, value_format):
        '''
        To generate the status command
        '''
        msg = Status(value, value_format)
        command = msg.encode(0x8B)
        return command

    def generate_unknownID_command(self,commandid, value, value_format):
        '''
        To generate the mode command
        '''
        msg = UnknownID(commandid,value, value_format)
        command = msg.encode(commandid)
        return command

    def handle_response(self, responsemessage):
        '''

        Args:
            responsemessage: the whole response message got from the remote device

        Returns: the decoded message

        '''
        # The message length
        undecodemessagelength = len(responsemessage)

        # The result will be in a list
        thismessagelength =0
        responses = []
        while undecodemessagelength > 0:
            # The message type
            messagetype =responsemessage[0]
            messagetype = ord(messagetype)

            # use different instance to call the function according to the message type, and save the messageid
            if messagetype == 0x81:
                res = Mode()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x82:
                res = PhyConfig()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x83:
                prpcconfig =[]
                # This message is combined 3 different messages
                res1 = ReadPRPCPhyConfig()
                thismessagelength1 = res1.decode_response(responsemessage)
                res1.responsemessagetype = messagetype
                prpcconfig.append(res1)
                responsemessage_83_loop =responsemessage[thismessagelength1:]

                res2 = Mode()
                thismessagelength2 = res2.decode_response(responsemessage_83_loop)
                res2.responsemessagetype = ord(responsemessage_83_loop[0])
                prpcconfig.append(res2)
                responsemessage_83_loop =responsemessage_83_loop[thismessagelength2:]

                res3 = PhyConfig()
                thismessagelength3 = res3.decode_response(responsemessage_83_loop)
                res3.responsemessagetype = ord(responsemessage_83_loop[0])
                prpcconfig.append(res3)

                responses.append(prpcconfig)
                thismessagelength = thismessagelength1 + thismessagelength2 + thismessagelength3

            elif messagetype == 0x84:
                res = FrameTXInterval()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x85:
                res = TXChannelList()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x86:
                res = TXPower()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x87:
                res = PayloadItem()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x88:
                # This response is composed by all tx configuration, the length is 2+4+3+3+4*8
                prpc_tx_config =[]
                res1 = ReadPRPCTXConfiguration()
                thismessagelength1 = res1.decode_response(responsemessage)
                res1.responsemessagetype = messagetype
                prpc_tx_config.append(res1)
                responsemessage_this_loop =responsemessage[thismessagelength1:]

                res2 = FrameTXInterval()
                thismessagelength2 = res2.decode_response(responsemessage_this_loop)
                res2.responsemessagetype = ord(responsemessage_this_loop[0])
                prpc_tx_config.append(res2)
                responsemessage_this_loop =responsemessage_this_loop[thismessagelength2:]

                res3 = TXChannelList()
                thismessagelength3 = res3.decode_response(responsemessage_this_loop)
                res3.responsemessagetype = ord(responsemessage_this_loop[0])
                prpc_tx_config.append(res3)
                responsemessage_this_loop =responsemessage_this_loop[thismessagelength3:]

                res4 = TXPower()
                thismessagelength4 = res4.decode_response(responsemessage_this_loop)
                res4.responsemessagetype = ord(responsemessage_this_loop[0])
                prpc_tx_config.append(res4)
                responsemessage_this_loop =responsemessage_this_loop[thismessagelength4:]

                payload = []
                for i in range(0,8):
                    res5 = PayloadItem()
                    thismessagelength5 = res5.decode_response(responsemessage_this_loop)
                    res5.responsemessagetype = ord(responsemessage_this_loop[0])
                    payload.append(res5)
                    responsemessage_this_loop =responsemessage_this_loop[thismessagelength5:]

                prpc_tx_config.append(payload)
                responses.append(prpc_tx_config)
                thismessagelength = thismessagelength1 +\
                                    thismessagelength2 +\
                                    thismessagelength3 +\
                                    thismessagelength4 +\
                                    thismessagelength5*8

            elif messagetype == 0x89:
                res = StatusInterval()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype == 0x8A:
                # This will return all the node status configurations
                node_status_config =[]
                res1 = ReadStatus()
                thismessagelength1 = res1.decode_response(responsemessage)
                res1.responsemessagetype = messagetype
                node_status_config.append(res1)
                responsemessage_8A_loop =responsemessage[thismessagelength1:]

                res2 = Status()
                thismessagelength2 = res2.decode_response(responsemessage_8A_loop)
                res2.responsemessagetype = ord(responsemessage_8A_loop[0])
                node_status_config.append(res2)

                responses.append(node_status_config)
                thismessagelength = thismessagelength1+thismessagelength2

            elif messagetype == 0x8B:
                res = Status()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            elif messagetype in (0xFC, 0xFD, 0xFE, 0xFF):
                res = Error()
                thismessagelength = res.decode_response(responsemessage)
                res.responsemessagetype = messagetype
                responses.append(res)

            else:
                raise BeaconDecodeException("Parse Error: Invalid payload. (payload: "+binascii.hexlify(responsemessage)+")")

            responsemessage =responsemessage[thismessagelength:]
            undecodemessagelength -= thismessagelength

        return responses


class BeaconDecodeException(Exception):
    '''
     For decode exception
    '''
    def __init__(self, message):
        super(BeaconDecodeException, self).__init__(message)
