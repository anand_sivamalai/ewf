# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
OTAP related functions for MeshApiDevice

Created on 21.12.2016

'''

from meshapi_common import *
from meshapicsap import *
from meshapimsap import *
from meshapidsap import *
from commutils import *
from Queue import Queue

def get_otap_image_status(self, timeout=30):
    '''
    Get the OTAP Image status.

    Attributes:

    - timeout: timeout in seconds

    Returns the OTAP image status.
    '''

    starttime = datetime.datetime.now()
    response = None
    while response == None:
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapScratchpadStatus())

    return response.confirmation

def clear_otap_image(self, timeout=30):
    '''
    Clear the OTAP image from device.

    Attributes:

    - timeout: timeout in seconds

    Raises an exception if failed.
    '''

    starttime = datetime.datetime.now()
    response = None
    while response == None:
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapScratchpadClear())

    result = response.confirmation['result'][0]

    if result == MsapScratchpadClearResult.Success:
        return response
    elif result == MsapScratchpadClearResult.FailureInvalidStackState:
        raise MeshApiDeviceException('The stack is in invalid status',result)
    elif result == MsapScratchpadClearResult.FailureAccessDenied:
        raise MeshApiDeviceException('Access Denied',result)

def load_otap_image(self, otap_image, otap_sequence=None,
                    progress_callback=None, progress_step=10, timeout=30):
    '''
    Load the OTAP image to the device.

    Attributes:

    - otap_image: An OTAPImage instance containing the OTAP image
    - otap_sequence: Optional OTAP sequence number. If not defined will use the sequence number from the otap_image
    - progress_callback: Optional callback for progress function progress_callback(percentage)
    - progress_step: The step size in percents for progress_callback
    - timeout: Timeout for EACH command.

    '''

    image = otap_image.data
    length = len(otap_image.data)

    if otap_sequence == None:
        otap_sequence = otap_image.sequence_number

    starttime_start = datetime.datetime.now()
    response = None

    # Transfer the image in maximum size blocks.
    blocksize = self._get_msap_attribute(MsapAttribId.ImageBlockMax)
    adder = uint8_to_int(blocksize.confirmation['attribute_value'][0])

    while response == None:
        if (datetime.datetime.now() - starttime_start).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapScratchpadStart(otap_sequence, length))

    result = response.confirmation['result'][0]

    if result == 1:
        raise MeshApiDeviceException('Stack is running', result)
    elif result == 2:
        raise MeshApiDeviceException('Invalid image length', result)
    elif result == 3:
        raise MeshApiDeviceException('Invalid sequence number', result)
    elif result == 4:
        raise MeshApiDeviceException('Access Denied', result)

    if progress_callback != None:
        old_percentage = 0
        progress_callback(0)

    for address in xrange(0, length, adder):

        # Do the progress callback if necessary.
        if progress_callback != None:
            current_percentage = 100*address/length
            number_of_steps = int((current_percentage-old_percentage)/progress_step)
            if number_of_steps > 0:
                old_percentage += number_of_steps*progress_step
                progress_callback(old_percentage)

        starttime_block = datetime.datetime.now()
        response = None
        block = image[address:address+adder]

        while response == None:
            if (datetime.datetime.now() - starttime_block).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)

            response = self.comminterface.send_request(MsapScratchpadBlock(address, str(block)))

        result = response.confirmation['result'][0]

        if result == MsapScratchpadBlockResult.SuccessAllData:
            break
        elif result == MsapScratchpadBlockResult.FailureAllData:
            raise MeshApiDeviceException('All data received but scratchpad is invalid', result)
        elif result == MsapScratchpadBlockResult.FailureInvalidStackState:
            raise MeshApiDeviceException('Stack in invalid state', result)
        elif result == MsapScratchpadBlockResult.FailureNoStartRequest:
            debug.info("The scratchpad start time is" + str(starttime_start))
            debug.info("The scratchpad block time is" + str(starttime_block))
            raise MeshApiDeviceException('No scratchpad start request', result)
        elif result == MsapScratchpadBlockResult.FailureStartAddressInvalid:
            raise MeshApiDeviceException('Invalid start address for a block', result)
        elif result == MsapScratchpadBlockResult.FailureNumberOfBytesInvalid:
            raise MeshApiDeviceException('Number of bytes for a block was invalid', result)
        elif result == MsapScratchpadBlockResult.FailureInvalidScratchpad:
            raise MeshApiDeviceException('Invalid scratchpad block', result)

    if progress_callback != None:
        progress_callback(100)

    if result == MsapScratchpadBlockResult.SuccessAllData:
        return result
    else:
        raise MeshApiDeviceException('Scratchpad load failed', result)

def set_otap_image_bootable(self, timeout=30):
    '''
    Set the OTAP image in device bootable.

    Attributes:

    - timeout: timeout in seconds

    Raises an exception if failed.
    '''

    starttime = datetime.datetime.now()
    response = None
    while response == None:
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapScratchpadUpdate())

    result = response.confirmation['result'][0]

    if result == MsapScratchpadUpdateResult.Success:
        return response
    elif result == MsapScratchpadUpdateResult.FailureInvalidStackState:
        raise MeshApiDeviceException('The stack is in invalid status', result)
    elif result == MsapScratchpadUpdateResult.FailureNoValidScratchpad:
        raise MeshApiDeviceException('Invalid scratchpad', result)
    elif result == MsapScratchpadUpdateResult.FailureAccessDenied:
        raise MeshApiDeviceException('Access Denied', result)

def otap_remote_status(self, dst_address=ADDRESS_BROADCAST, timeout=30):
    '''
    Check the status of the remote node.

    Attributes:

    - dst_address: The address of the remote node

    Raises an exception if failed.
    '''

    starttime = datetime.datetime.now()
    response = None
    while response == None:
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapRemoteStatus(dst_address))

    result = response.confirmation['result'][0]

    if result != 0:
        raise MeshApiDeviceException('Remote status query Failed', result)

def wait_remote_status_indication_rx(self):
    '''
    Start receiving of remote status indication
    '''
    if self.remote_status_indication != None:
        raise MeshApiDeviceException('Remote status indication already registered', None)

    indication = MsapRemoteStatusIndication()
    self.register_indication_callback(indication, self._ind_callback_gen(self.remote_status_queue))
    self.remote_status_indication = indication

def stop_remote_status_indication_rx(self):
    '''
    Start receiving of remote status indication
    '''
    if self.remote_status_indication != None:
        self.comminterface.deregister_indication(self.remote_status_indication)
        self.remote_status_indication = None
        # Clear the queue also.
        self.remote_status_queue = Queue()

def get_remote_status_indication_rx(self, timeout = 30):
    '''
    Get remote status indication (started with wait_remote_status_indication_rx)

    Returns:
    Dictionary with following keys:

    - 'source_address'
    - 'scratchpad_length'
    - 'crc'
    - 'otap_seq'
    - 'scratchpad_type'
    - 'scratchpad_status'
    - 'processed_length'
    - 'processed_crc'
    - 'processed_seq'
    - 'fw_mem_area_id'
    - 'fw_major_version'
    - 'fw_minor_version'
    - 'fw_maintenance_version'
    - 'fw_development_version'

    '''
    rxpkt = self.remote_status_queue.get(timeout=timeout)
    retval = {}

    for key, val in rxpkt['ind'].indication.iteritems():
        retval[key] = val[0]

    return retval

def otap_remote_update_request(self, address, otap_seq,reboot_delay=60,indication_callback = None, timeout=30):
    '''
    Send an OTAP remote update request.

    Attributes:

    - address: Target node address
    - otap_seq: Image sequence number to set as bootable
    - reboot_delay: Number of seconds before the scratchpad is marked for processing
    - indication_callback: An optional indication callback that will be called when remote status indications are received after the request.
    - timeout: Timeout for the update request command.

    '''

    if indication_callback != None:
        if self.remote_status_indication != None:
            raise MeshApiDeviceException('Remote status indication already registered')
        indication = MsapRemoteStatusIndication()
        self.register_indication_callback(indication, indication_callback)
        self.remote_status_indication = indication

    starttime = datetime.datetime.now()
    response = None
    while response == None:
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise MeshApiDeviceException('Timeout expired', None)
        response = self.comminterface.send_request(MsapRemoteUpdate(address, otap_seq,reboot_delay))

    result = response.confirmation['result'][0]

    if result != 0:
        raise MeshApiDeviceException('Remote update Failed', result)
