# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This is an example of sending a data packet between two devices.
Two devices need to be connected. One has to be a sink and the other
headnode or subnode. Network parameters have to be same for both nodes.
Also the stacks in both nodes need to be running.

'''

# meshapi module contains the MeshApiDevice and MeshApiNetwork classes.
from wirepas.meshapi import *

# meshapicsap module contains the CSAP definitions.
from wirepas.meshapicsap import *

def dataexample():
    # Create a network instance.
    network = MeshApiNetwork()

    # Find MeshApi devices connected to the computer.
    # config.py has the configuration for device and interface types that are used.
    print "Finding devices.."
    network.find_devices()

    print "Found: "+str(network.get_devices())

    # Get the sink.
    sink = network.get_sinks()[0]
    print "Sink: "+str(sink)

    # Get sink address.
    sink_address = sink.get_address()
    print "Sink address: "+str(sink_address)

    # Get the node.
    node = network.get_nodes()[0]
    print "Node: "+str(node)

    # Get node address.
    node_address = node.get_address()
    print "Node address: "+str(node_address)

    # Start reception mode for node.
    print "Start data rx for node"
    node.start_data_rx()

    # Send data from sink to node.
    print "Send data from sink to node"
    sink.data_tx('Hello, world!', dest_address = node_address)

    # Receive data.
    # Time out is 120s because it takes some time for the
    # node to connect to the sink.
    print "Wait to receive data.."
    retval = node.get_data_rx(timeout = 120)

    # Print the message
    print retval['apdu']

    print "Kill network.."
    network.kill()

if __name__ == "__main__":
    dataexample()
