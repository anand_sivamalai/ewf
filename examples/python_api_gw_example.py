# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This is an example of receiving data in the gateway.

One sink needs to be connected.
'''

import binascii
import time

# Load custom configuration from config.ini file
from configuration import config
config.load_file('config.ini')

# meshapi module contains the MeshApiDevice and MeshApiNetwork classes.
from meshapi import *

# meshapicsap module contains the CSAP definitions.
from meshapidsap import DsapDataRx

def _data_rx_callback_gen():
    '''
    Used to create a callback with. It is possible to add arguments to callback
    gen that are visible in f(command)
    '''

    def f(command):
        '''
        This is a callback function called when data is received. This should be
        kept very quick and simple!
        '''
        print 'Data rx:src_ep:{},dst_ep:{},data:{}'\
            .format(command.indication['source_endpoint'][0],
                    command.indication['destination_endpoint'][0],
                    binascii.hexlify(command.indication['apdu'][0]))

    return f

def gwexample():
    # Create a network instance.
    network = MeshApiNetwork()

    # Find MeshApi devices connected to the computer.
    # config.py has the configuration for device and interface types that
    # are used.
    print "Finding devices.."
    network.find_devices()

    print "Found: " + str(network.get_devices())

    print 'Devices ready'

    # Get the sink.
    sink = network.get_sinks()[0]
    print "Sink: " + str(sink)

    # Get sink address.
    sink_address = sink.get_address()
    print "Sink address: " + str(sink_address)

    # Start reception mode for sink
    print "Start data rx for sink"

    sink.register_indication_callback(DsapDataRx(), _data_rx_callback_gen())

    # Just loop eternally
    while (True):
        time.sleep(1)

if __name__ == "__main__":
    gwexample()
