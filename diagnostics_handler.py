# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#


from Queue import Queue

import meshapidsap

def callback_generator(handler):
    '''
    Callback generator for diagnostics
    Args:
        handler: Diagnostics handler instance

    Returns:
        Function pointer to call

    '''
    def f(packet):
        try:
            handler.receive_diagnostics(packet)
        except Exception, ex:
            print 'Diagnostics handling error:{}'.format(ex)
    return f

class DiagnosticsHandler(object):
    '''
    This is a base class for generic diagnostics handler. It takes received diagnostics messages as input and then
    do something with those. For example, store them to database, to a file or whatever.
    '''
    def __init__(self, device, measurements=[]):
        self.device = device
        self.queue = Queue()

        # Do registration for all diagnostics type
        callback = callback_generator(self)
        for indication in [meshapidsap.DsapNodeDiagnostics(),
                           meshapidsap.DsapTrafficDiagnostics(),
                           meshapidsap.DsapNeighborDiagnostics(),
                           meshapidsap.DsapBootDiagnostics()]:
            self.device.register_indication_callback(indication, callback)
            self.device.register_diagnostics_handler(self)
        for measurement in measurements:
            self.device.register_indication_callback(measurement, callback)
            self.device.register_diagnostics_handler(self)


    def receive_diagnostics(self, packet):
        '''
        Diagnostics packet is received

        Args:
            packet: Incoming packet
        '''
        raise NotImplemented()

    def abort(self):
        '''
        Abort the execution
        '''
        pass

