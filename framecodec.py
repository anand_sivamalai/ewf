# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
The framecodec abstract class from where the interface specific classes can be inherited.

Created on: 18.09.2015

'''

from struct import *
import debug
from configuration import config
from Queue import Queue

class Framecodec(object):
    '''
    The framecodec abstract class from where the interface specific classes can be inherited.
    '''
    
    def __init__(self):
        self.skip_errors = False
        if hasattr(config, 'framecodec_skip_errors'):
            if config.framecodec_skip_errors:
                self.skip_errors = True

        # A queue for debug messages outside the normal frames
        self.debug_print_queue = Queue()
            
    def encode(self, msg):
        ''' 
        Encode message
        
        Arguments:
        - msg: message
        '''
        raise NotImplemented
    
    def decode(self, frame):
        '''
        Decode frame
        
        Arguments:
        - frame: Received data
        
        Returns:
        - Parsed binary frame
        '''
        raise NotImplemented

    def _handle_debug_print(self, debug_print):
        '''
        Adds the debug messages into a queue.

        Arguments:
          - debug_print: The debug message
        '''

        self.debug_print_queue.put(debug_print)
    
class FramecodecException(Exception):
    def __init__(self, message):
        super(FramecodecException, self).__init__(message)
        debug.error(message)
