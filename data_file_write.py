import json

configfile = 'nodes.cfg'

data = {}  
data['dataTypesUsed'] = ['boolean', 'uint16', 'uint32', 'float16', 'float32', 'string']
data['TREON_SOLAR_NODE'] = []  
data['TREON_SOLAR_NODE'].append({  
    'Name': 'KwH',
    'Desc': 'KiloWatt Hours',
    'Value': '33.33',
    'Type': 5
})
data['TREON_SOLAR_NODE'].append({  
    'Name': 'Temp',
    'Desc': 'Panel Temperature',
    'Value': '',
    'Type': 4
})
data['TREON_SOLAR_NODE'].append({  
    'Name': 'Amps',
    'Desc': 'Total AC Current',
    'Value': '3.33',
    'Type': 5
})



data['TREON_ENVIRONMENTAL_SENSOR'] = []  
data['TREON_ENVIRONMENTAL_SENSOR'].append({  
    'Name': 'Humidity',
    'Desc': 'Humidity',
    'Value': '2.2',
    'Type': 5
})
data['TREON_ENVIRONMENTAL_SENSOR'].append({  
    'Name': 'AL',
    'Desc': 'Ambient Light Level',
    'Value': '',
    'Type': 5
})
data['TREON_ENVIRONMENTAL_SENSOR'].append({  
    'Name': 'Temp',
    'Desc': 'Temperature in degrees',
    'Value': '22.2',
    'Type': 4
})

with open(configfile, 'w') as outfile:  
    json.dump(data, outfile, indent=4)
    
    
# Read entire file as a data structure    
with open(configfile) as json_file:  
    data = json.load(json_file)
    
# Create list of data types
datatypes = data['dataTypesUsed'] 

# Debug output
for p in data['TREON_SOLAR_NODE']:
    print('Name: ' + p['Name'])
    print('Desc: ' + p['Desc'])
    print('Value: ' + p['Value'])
    print('Type: ' + datatypes[int(p['Type'])])
