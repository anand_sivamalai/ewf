# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

This is the serial port over TCP/IP bridge interface class that is inherited from the 
SerialInterface class.

Created on 11.02.2016

'''

from configuration import config
import serialinterface
import debug
import socket
import binascii
import time
from threading import Thread, current_thread
from Queue import Queue, Empty

from framecodec_serial import FrameCodecSerial
from comminterface import CommInterface

def find_devices():
    devices = []

    debug.info("Finding serial over TCP/IP devices..")
    
    for host in config.serialtcpip_hosts:
        (address, port) = host.split(':')
        devices.append(SerialOverTCPIPInterface(address, int(port)))

    devices = filter(lambda x: x.get_connected(), devices)

    debug.info('Found '+str(len(devices))+' serial over TCP/IP devices.')

    return devices
    
class SerialOverTCPIPInterface(serialinterface.SerialInterface):
    '''
    A class for serial over TCP/IP Interface. 
    Inherited from the SerialInterface class.
    '''

    # The address for the device
    address = None

    # Port for the device
    port = None

    # Is the port connected?
    connected = False

    # The TCP/IP socket
    socket = None
    
    
    def __init__(self, address, port):
        self.address = address
        self.port = port

        CommInterface.__init__(self)

        # Store parent thread information.
        self.parent_thread = current_thread()

        self.debug_info("Creating interface, address: "+address+", port "+str(port))

        
        try:
            self.debug_info("Connecting..")
            self.socket = socket.create_connection((address, port), 5.0)
            self.socket.settimeout(0.05)                               
        except socket.error:
            self.debug_info("Could not connect")
            return

        # Get write chunk size
        if hasattr(config, 'serialtcpip_chunk_size'):
            self.chunk_size = config.serialtcpip_chunk_size
        else:
            self.chunk_size = 32
        
        # Queues for starting the RX & TX threads so they don't need to busy wait.
        self.rx_start_queue = Queue()
        self.tx_start_queue = Queue()
        
        # Start this thread
        self.start()
        
        # Open the port
        self.open()

    def open(self):
        '''
        A method to open the interface.
        '''

        self.debug_info("Opening the "+str(self))
        if self.connected:
            self.debug_info("Serial over TCP/IP port already open")
            return

        # Start a new framecodec.
        self.framecodec = FrameCodecSerial() 

        # Inform the RX and TX threads that they can switch mode.
        self.rx_start_queue.put(1)
        self.tx_start_queue.put(1)
        self.connected = True

        # Call parent class.
        CommInterface.open(self)

        # Start the indication thread.
        if not config.serialtcpip_disable_indication_poll:
            self.start_indication_poll()
        
    def get_connected(self):
        '''
        Just to get connection status.

        - Returns True if connection is open, False if not.
        '''

        return self.connected

    def is_local(self):
        '''
        To check if connection is local (not over TCP/IP or similar).

        - Return True if connection is local, False if not.
        '''

        return False
    
    def kill(self):
        '''
        Kill the interface.

        Kills all threads. After this the interface cannot be reopened.
        '''

        # Kill indication thread first.
        if not config.serialtcpip_disable_indication_poll:
            self.stop_indication_poll()
        
        CommInterface.kill(self)
        
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            self.socket = None
        except:
            self.debug_info("Closing the serial port failed!")


    def get_host_name(self):
        '''
        Get the host name.

        Returns the host name.
        '''

        return self.address

    def get_port(self):
        '''
        Get the TCP port number.

        Returns the port number.
        '''

        return self.port
    
    def __repr__(self):
        return "Serial over TCP/IP Port "+str(self.address)+":"+str(self.port)

    def _read_bytes(self):
        '''
        Reads data from serial port. 

        - Returns the data in a bytearray. 
          If no data was available then an empty bytearray is returned.
        '''
        
        try:
            data = bytearray(self.socket.recv(128))
            if len(data) == 0:
                # Other end probably closed connection.
                raise socket.error
            else:
                self.debug_info("Socket read: %s" % binascii.hexlify(data))
        except socket.timeout:
           # Timed out, ignore.
            return bytearray()
        except socket.error:
           # Error, close connection.
            self.close()
            return bytearray()

        return data


    def _send_frame(self, frame):
        '''
        Sends a frame of data to the serial port.

        - frame, the frame in bytearray
        '''
        self.debug_info("Serial write:{}".format(binascii.hexlify(frame)))
        if (self.connected):
            try:
                self.socket.sendall(frame)
                time.sleep(0.005)
            except socket.error:
                # Error, close connection.
                self.close()

    def read_response(self):
        '''
        Reads a response from serial port and decodes it. Waits until a full frame is collected.

        - Returns the decoded frame.
        '''

        # Try to decode an empty bytearray which causes the frame codec to use it's internal buffer.
        retval = self.framecodec.decode(bytearray())
        if retval != None:
            # There was a full frame in frame codec buffer. Return it.
            return retval

        # Read data until we have a full frame.
        # Note this is spinlock to avoid waiting which would result buffer overrun
        # We let Python threading/GIL to give fairness to other threads
        while retval is None and self.connected:
            data = self._read_bytes()
            retval = self.framecodec.decode(data)

        # Return the frame.
        return retval
