# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#


import socket
from uuid import getnode as get_mac

class GatewayIdentity(object):
    '''
    This static class contains the gateway identity issues so that identity is described commonly in each module
    '''

    ip_addr = None
    mac_addr = None
    hostname = None

    @staticmethod
    def get_mac_addr():
        if GatewayIdentity.mac_addr is None:
            GatewayIdentity.mac_addr = get_mac()
            GatewayIdentity.mac_addr = ':'.join(("%012X" % GatewayIdentity.mac_addr)[i:i+2] for i in range(0, 12, 2))
        return GatewayIdentity.mac_addr

    @staticmethod
    def get_hostname():
        if GatewayIdentity.hostname is None:
            GatewayIdentity.hostname = socket.gethostname()
        return GatewayIdentity.hostname


