# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

General interface classes for remote API.

Created on 14.11.2016

'''

# Command line tool is usually run from testapi directory but test cases
# from outside. Import accordingly.
try:
    from meshapicsap import *
    from meshapimsap import *
except ImportError:
    from wirepas.meshapicsap import *
    from wirepas.meshapimsap import *
import binascii
from collections import OrderedDict
from struct import *

# Various erndpoints
ENDPOINT_REQUEST_SRC = 255
ENDPOINT_REQUEST_DST = 240
ENDPOINT_RESPONSE_SRC = 240
ENDPOINT_RESPONSE_DST = 255

class MessageType:
    Ping = 0x00
    BeginWithoutKey = 0x01
    BeginWithKey = 0x02
    EndRequest = 0x03
    CancelRequest = 0x04
    Delay = 0x05
    MsapWrite = 0x0B
    MsapRead = 0x0C
    CsapWrite = 0x0D
    CsapRead = 0x0E
    AccessDenied = 0xF8
    WriteOnly = 0xF9
    InvalidBroadcast = 0xFA
    InvalidBegin = 0xFB
    NoSpace = 0xFC
    InvalidValue = 0xFD
    InvalidLength = 0xFE
    UnknownRequest = 0xFF
    ResponseMask = 0x80



class AttributeFormat():
    csap_attribute = \
        { CsapAttribId.NodeAddress: 'uint32',
          CsapAttribId.NetworkAddress: 'uint24',
          CsapAttribId.NetworkChannel: 'uint8',
          CsapAttribId.NodeRole: 'uint8',
          CsapAttribId.MTU: 'uint8',
          CsapAttribId.PDUBufferSize: 'uint8',
          CsapAttribId.ImageSequence: 'uint8',
          CsapAttribId.MeshApiVersion: 'uint16',
          CsapAttribId.FirmwareMajor: 'uint16',
          CsapAttribId.FirmwareMinor :'uint16',
          CsapAttribId.FirmwareMaintenance: 'uint16',
          CsapAttribId.FirmwareDevelopment: 'uint16',
          CsapAttribId.CipherKey: '16s',
          CsapAttribId.AuthenticationKey: '16s',
          CsapAttribId.ChannelLimits: 'uint16',
          CsapAttribId.AppConfigDataSize: '16s',
          CsapAttribId.HwMagic: 'uint16',
          CsapAttribId.StackProfile: 'uint16',
          CsapAttribId.OfflineScan: 'uint16',
          CsapAttribId.ChannelMap: 'uint32',
          CsapAttribId.FeatureLockBits: 'uint32',
          CsapAttribId.FeatureLockKey: '16s'
         }

    msap_attribute = \
        { MsapAttribId.StackStatus: 'uint8',
          MsapAttribId.PDUBufferUsage: 'uint8',
          MsapAttribId.PDUBufferCapacity: 'uint8',
          #MsapAttribId.NeighbourCount: 'uint8',
          MsapAttribId.Energy: 'uint8',
          MsapAttribId.Autostart: 'uint8',
          MsapAttribId.RouteCount: 'uint8',
          MsapAttribId.SystemTime: 'uint32',
          MsapAttribId.AccessCycleRange: 'uint32',
          MsapAttribId.AccessCycleLimits: 'uint32',
          MsapAttribId.CurrentAccessCycle: 'uint16',
          MsapAttribId.ImageBlockMax: 'uint8'
         }


class RemoteAPI(object):
    '''
    The basic class for the remote API
    '''
    def __init__(self):
        self.command = OrderedDict()
        self.message_type = None
        self.response = OrderedDict()
        self.response_message_type = None

    def _desc_to_format(self, desc):
        '''

        Args:
            desc: the data format

        Returns:the pack/unpacking format

        '''
        d = {'uint8': 'B',
             'int8': 'b',
             'uint16': 'H',
             'uint32': 'I',
             'string': 's',
             'uint24': '3s',
             '16s': '16s'
             }

        payload_fmt = d[desc]
        return payload_fmt

    def _desc_to_len(self, desc):
        '''

        Args:
            desc: the data format

        Returns: the length of the input format

        '''
        d = {'uint8': 1,
             'int8': 1,
             'uint16': 2,
             'uint32': 4,
             'uint24': 3,
             '16s': 16
             }

        length = d[desc]
        return length

    # def csap_attribute_value_convert(self,attributeid, value):
    #     fmt = '<'
    #     dic_value = AttributeFormat.csap_attribute[attributeid]
    #     fmt = fmt + self._desc_to_format(dic_value)
    #     pack_value = pack(fmt, value)
    #     return pack_value

    def csap_attribute_format(self, attributeid):
        '''

        Args:
            attributeid: the attribute ID

        Returns:
            The format of the attribute and also the packet format
        '''
        if attributeid in (CsapAttribId.AuthenticationKey, CsapAttribId.AppConfigDataSize,
                           CsapAttribId.CipherKey,CsapAttribId.FeatureLockKey):
            dic_value = AttributeFormat.csap_attribute[attributeid]
            fmt = AttributeFormat.csap_attribute[attributeid]
        else:
            dic_value = AttributeFormat.csap_attribute[attributeid]
            fmt = self._desc_to_format(dic_value)
        return dic_value,fmt

    # def msap_attribute_value_convert(self,attributeid, value):
    #     fmt = '<'
    #     # The key of the dictionary
    #     dic_value = AttributeFormat.msap_attribute[attributeid]
    #     fmt = fmt + self._desc_to_format(dic_value)
    #     pack_value = pack(fmt, value)
    #     return pack_value

    def msap_attribute_format(self, attributeid):
        '''
        Args:
            attributeid: the attribute id
        Returns:
            The format of the attribute and also the packet format
        '''
        # The key of the dictionary
        dic_value = AttributeFormat.msap_attribute[attributeid]
        fmt = self._desc_to_format(dic_value)
        return dic_value,fmt

    def encode(self, message_type):
        return self.encode_message(message_type,self.command)

    def encode_message(self, message_type, dict):
        '''
        Args:
            message_type: the message type
            dict: the dictionary used for encoding
        Returns:
            The packet command
        '''
        self.message_type = message_type
        # The packet format
        total_fmt = '<B'
        # The values
        payload_value = []
        # The total values = message type + payload values
        total_values = [message_type]
        # The length in the command
        length = 0

        # The single command is: message type + length + attribute ID + value OR
        #                        message type + length  OR
        #                        message type + length + value
        if len(dict) == 1:
            length = length

        elif len(dict) >= 2:
            if dict.has_key('msap_attribute_value'):
                # The attribute format needs to be changed according to the attribute ID
                msap_value_format, msap_value_packet_format = self.msap_attribute_format(dict['attribute_id'][0])
                dict['msap_attribute_value'] = (dict['msap_attribute_value'][0],msap_value_format)
                length = self._desc_to_len(dict['attribute_id'][1]) + self._desc_to_len(dict['msap_attribute_value'][1])

            elif dict.has_key('csap_attribute_value'):
                # The attribute format needs to be changed according to the attribute ID
                csap_value_format, csap_value_packet_format = self.csap_attribute_format(dict['attribute_id'][0])
                dict['csap_attribute_value'] = (dict['csap_attribute_value'][0],csap_value_format)
                length = self._desc_to_len(dict['attribute_id'][1]) + self._desc_to_len(dict['csap_attribute_value'][1])

            else:
                for value in dict.itervalues():
                    length += self._desc_to_len(value[1])
                length -= 1
        dict['length'] = (length, 'uint8')

        for key,value in dict.iteritems():
            if value[1] == 'uint24':
                dict[key] = (pack('<I', value[0])[:-1],value[1])

        for value in dict.itervalues():
            payload_value.append(value[0])
            total_fmt += self._desc_to_format(value[1])

        total_values = total_values + payload_value
        frame = pack(total_fmt, *total_values)
        return frame

    def decode_response(self,responsedata):
        singlemessagelength = self.parse_message(self.response,responsedata)
        return singlemessagelength

    def parse_message(self,dict,message):
        '''
        This is for decoding the message 0x8d(csap write) and 0x8e(csap read)
        Args:
            dict: the data structure dictionary
            message: the message needs to be decoded
        Returns:
            The length of the message
        '''
        length = 0
        payload_fmt = '<'
        if len(dict) == 1:
            dict['response_length']= (ord(message[1]),'uint8')
        elif len(dict) >= 2:
            if dict.has_key('response_msap_attribute_value'):
                a=  binascii.hexlify(message)
                b = bytearray(a)
                attribute_id = unpack('<H',message[2:4])[0]
                attribute_unpack_format, msap_value_packet_format = self.msap_attribute_format(attribute_id)
                dict['response_msap_attribute_value'] = (None, attribute_unpack_format)
            elif dict.has_key('response_csap_attribute_value'):
                attribute_id = unpack('<H',message[2:4])[0]
                attribute_unpack_format, csap_value_packet_format = self.csap_attribute_format(attribute_id)
                dict['response_csap_attribute_value'] = (None, attribute_unpack_format)

        for value in dict.itervalues():
            if not value[1].startswith('string'):
                packformat = self._desc_to_format(value[1])
                length += self._desc_to_len(value[1])
            else:
                string_length = int(value[1].split()[1])
                packformat = str(string_length)+'s'
                length += string_length
            payload_fmt = payload_fmt + packformat

        messagelength = length +1
        message = message[1:messagelength]

        unpacked = unpack(payload_fmt,message)
        i = 0
        for key, value in dict.iteritems():
            if len(unpacked) <= i:
                dict[key] = (None, value[1])
            elif value[1] == 'uint24':
                # Convert 24-bit values back
                dict[key] = (unpack('<I', unpacked[i] + '\x00')[0], value[1])
            else:
                dict[key] = (unpacked[i], value[1])
            i = i + 1

        return messagelength

class Ping(RemoteAPI):
    def __init__(self):
        super(Ping, self).__init__()
        self.message_type = MessageType.Ping
        self.command['length'] = (0, 'uint8')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')


class BeginWithoutKey(RemoteAPI):
    def __init__(self):
        super(BeginWithoutKey, self).__init__()
        self.message_type = MessageType.BeginWithoutKey
        self.command['length'] = (0, 'uint8')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')


class BeginWithKey(RemoteAPI):
    def __init__(self,
                 key=0
                 ):
        super(BeginWithKey, self).__init__()
        self.message_type = MessageType.BeginWithKey
        self.command['length'] = (0, 'uint8')
        self.command['key'] = (key, '16s')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')

class EndRequest(RemoteAPI):
    def __init__(self):
        super(EndRequest, self).__init__()
        self.message_type = MessageType.EndRequest
        self.command['length'] = (0, 'uint8')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')


class CancelRequest(RemoteAPI):
    def __init__(self):
        super(CancelRequest, self).__init__()
        self.message_type = MessageType.CancelRequest
        self.command['length'] = (0, 'uint8')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')


class Update(RemoteAPI):
    def __init__(self,
                 delay=0
                 ):
        super(Update, self).__init__()
        self.message_type = MessageType.Delay
        self.command['length'] = (0, 'uint8')
        self.command['delay'] = (delay,'uint16')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_value'] = (None, 'uint16')


class MsapWrite(RemoteAPI):
    def __init__(self,
                 attrib_id=9,
                 value=131074000
                 ):
        super(MsapWrite, self).__init__()
        self.message_type = MessageType.MsapWrite
        self.command['length'] = (0, 'uint8')
        self.command['attribute_id'] = (attrib_id, 'uint16')
        self.command['msap_attribute_value'] = (value, 'string')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_attribute_id'] = (None, 'uint16')
        self.response['response_msap_attribute_value'] = (None, 'string')


class MsapRead(RemoteAPI):
    def __init__(self,
                 attrib_id=9
                 ):
        super(MsapRead, self).__init__()
        self.message_type = MessageType.MsapRead
        self.command['length'] = (0, 'uint8')
        self.command['attribute_id'] = (attrib_id, 'uint16')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_attribute_id'] = (None, 'uint16')
        self.response['response_msap_attribute_value'] = (None, 'string')

class CsapWrite(RemoteAPI):
    def __init__(self,
                 attrib_id = 1,
                 value = 1
                 ):
        super(CsapWrite, self).__init__()
        self.message_type = MessageType.CsapWrite
        self.command['length'] = (0, 'uint8')
        self.command['attribute_id'] = (attrib_id, 'uint16')
        self.command['csap_attribute_value'] = (value, 'string')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_attribute_id'] = (None, 'uint16')
        self.response['response_csap_attribute_value'] = (None, 'string')

class CsapWriteKey(RemoteAPI):
    def __init__(self,
                 attrib_id = 1,
                 value = 1
                 ):
        super(CsapWriteKey, self).__init__()
        self.message_type = MessageType.CsapWrite
        self.command['length'] = (0, 'uint8')
        self.command['attribute_id'] = (attrib_id, 'uint16')
        self.command['csap_attribute_value'] = (value, 'string')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_attribute_id'] = (None, 'uint16')

class CsapRead(RemoteAPI):
    def __init__(self,
                 attrib_id=1
                 ):
        super(CsapRead, self).__init__()
        self.message_type = MessageType.CsapRead
        self.command['length'] = (0, 'uint8')
        self.command['attribute_id'] = (attrib_id, 'uint16')

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_attribute_id'] = (None, 'uint16')
        self.response['response_csap_attribute_value'] = (None, 'string')


class Error(RemoteAPI):
    def __init__(self):
        super(Error, self).__init__()

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_invalid_command'] = (None, 'uint8')
        self.response['response_attributeID'] = (None, 'uint16')

class Error_Without_Attribute(RemoteAPI):
    def __init__(self):
        super(Error_Without_Attribute, self).__init__()

        self.response_message_type = None
        self.response['response_length'] = (None, 'uint8')
        self.response['response_invalid_command'] = (None, 'uint8')

class RemoteAPICommandMessage(RemoteAPI):
    '''
     The remote api which is inherited from the basic remote api class
    '''
    def ping_command(self):
        '''
        To generate the Ping request
        '''
        msg = Ping()
        command = msg.encode(msg.message_type)
        return command

    def begin_withoutkey_command(self):
        '''
        To generate the begin without key command
        '''
        msg = BeginWithoutKey()
        command = msg.encode(msg.message_type)
        return command

    def begin_withkey_command(self,key):
        '''
        Args:
            key:
        Returns:
           the begin with key command
        '''
        msg = BeginWithKey(key)
        command = msg.encode(msg.message_type)
        return command

    def msap_read(self,attribute):
        '''
        Args:
            attribute:
        Returns:
           the msap read command
        '''

        msg = MsapRead(attribute)
        command = msg.encode(msg.message_type)
        return command

    def msap_write(self, attrib_id, value):
        '''
        Args:
            attrib_id:
            value:
        Returns:
            The command to write the MSAP attribute
        '''
        msg = MsapWrite(attrib_id, value)
        command = msg.encode(msg.message_type)
        return command

    def csap_read(self,attribute):
        '''
        Args:
            attribute:
        Returns:
            The command to read the CSAP attribute
        '''
        msg = CsapRead(attribute)
        command = msg.encode(msg.message_type)
        return command

    def csap_write(self, attrib_id, value):
        '''
        Args:
            attrib_id:
            value:
        Returns:
            The command to write the CSAP attribute
        '''
        msg = CsapWrite(attrib_id, value)
        command = msg.encode(msg.message_type)
        return command

    def csap_write_key(self, attrib_id, value):
        '''
        Args:
            attrib_id:
            value:
        Returns:
            The command to write the CSAP attribute
        '''
        msg = CsapWriteKey(attrib_id, value)
        command = msg.encode(msg.message_type)
        return command

    def end(self):
        '''
        Returns:
            The command to end the whole command
        '''
        msg = EndRequest()
        command = msg.encode(msg.message_type)
        return command

    def cancel(self):
        msg = CancelRequest()
        command = msg.encode(msg.message_type)
        return command

    def update(self, delay):
        '''
        Args:
            delay:
        Returns:
            The command to set the delay for the previous command
        '''
        msg = Update(delay)
        command = msg.encode(msg.message_type)
        return command

    def decode_match_id_with_class(self, response,messagetype_got,messagetype_expect, classname, message):
        '''
        This function is made to avoid many elif branch in the decoding function
        Args:
            response: the list for storing the decoded data
            messagetype_got: the message type which is got from the received data
            messagetype_expect: the first element in the "match"
            classname: the second element in the "match"
            message: the message which is going to be decoded

        Returns:
            1. whether the message is found from the list
            2. the message length which is decoded

        '''
        found_message = False
        thismessagelength = 0
        if messagetype_got == messagetype_expect:
            if messagetype_got == MessageType.CsapWrite|0x80 and ord(message[1:2]) == 2:   # write the key
                res = CsapWriteKey()
                thismessagelength = res.decode_response(message)
                res.response_message_type = messagetype_got
                response.append(res)
                found_message = True
            else:
                res = classname()
                thismessagelength = res.decode_response(message)
                res.response_message_type = messagetype_got
                response.append(res)
                found_message = True
                #return thismessagelength
        return (found_message,thismessagelength)


    def decode_error_2structure(self, response,messagetype_got, message):
        '''
        This function is used to decode the error message, the error message does not always have fixed length

        Args:
            response: the list for storing the decoded data
            messagetype_got: the message type
            message: the message which is going to be decoded

        Returns:
            1. whether the message is found from the list
            2. the message length which is decoded

        '''
        found_message = False
        thismessagelength = 0
        if ord(message[1:2]) == 1:
            res = Error_Without_Attribute()
            thismessagelength = res.decode_response(message)
            res.response_message_type = messagetype_got
            response.append(res)
            found_message = True
        elif ord(message[1:2]) == 3:
            res = Error()
            thismessagelength = res.decode_response(message)
            res.response_message_type = messagetype_got
            response.append(res)
            found_message = True
        else:
            raise RemoteResponseDecodeException("Parse Error: Invalid ERROR RESPONSE: "+binascii.hexlify(message))
        return (found_message,thismessagelength)

    def handle_response(self, responsemessage):
        '''
        The general decoding function
        Args:
            responsemessage:  the message which needs to be decoded

        Returns: the decoded message, storing in a list

        '''
        # The message length
        undecodemessagelength = len(responsemessage)

        # The result will be in a list
        thismessagelength =0
        responses = []
        #messagetypes = [MessageType.Ping, MessageType.BeginWithKey,M]
        #match = map(lambda x: (x | 0x80, x), messagetypes)

        match = [(MessageType.Ping|0x80, Ping),
            (MessageType.BeginWithoutKey|0x80, BeginWithoutKey),
            (MessageType.BeginWithKey|0x80, BeginWithKey),
            (MessageType.EndRequest|0x80, EndRequest),
            (MessageType.CancelRequest|0x80, CancelRequest),
            (MessageType.Delay|0x80, Update),
            (MessageType.MsapWrite|0x80, MsapWrite),
            (MessageType.MsapRead|0x80, MsapRead),
            (MessageType.CsapWrite|0x80, CsapWrite),
            (MessageType.CsapRead|0x80, CsapRead),
            #(MessageType.AccessDenied|0x80, Error_Without_Attribute), # 2 structures
            (MessageType.WriteOnly|0x80, Error),
            (MessageType.InvalidBroadcast|0x80, Error),
            #(MessageType.InvalidBegin|0x80, Error_Without_Attribute), # 2 structures
            (MessageType.NoSpace|0x80, Error),
            #(MessageType.InvalidValue|0x80, Error),
            #(MessageType.InvalidLength|0x80, Error)
            #(MessageType.UnknownRequest|0x80, Error)
             ]

        while undecodemessagelength > 0:
            # The message type
            messagetype =responsemessage[0]
            messagetype = ord(messagetype)

            found_message = False
            for item in match:
                #thismessagelength = self.decode_match_id_with_class(responses,messagetype,item[0], item[1],responsemessage)
                found_message,thismessagelength = self.decode_match_id_with_class(responses,messagetype,item[0], item[1],responsemessage)
                if found_message == True:
                    break
            if messagetype not in map(lambda x: x[0], match):
                if messagetype == MessageType.AccessDenied|0x80 or messagetype == MessageType.InvalidBegin|0x80 \
                    or messagetype == MessageType.UnknownRequest|0x80 or messagetype == MessageType.InvalidValue|0x80 \
                        or messagetype == MessageType.InvalidLength|0x80:
                    found_message,thismessagelength = self.decode_error_2structure(responses,messagetype,responsemessage)
                else:
                    raise RemoteResponseDecodeException("Parse Error: Invalid payload: "+binascii.hexlify(responsemessage))
            responsemessage =responsemessage[thismessagelength:]
            undecodemessagelength -= thismessagelength

        return responses


class RemoteResponseDecodeException(Exception):
    '''
     For decode exception
    '''
    def __init__(self, message):
        super(RemoteResponseDecodeException, self).__init__(message)

