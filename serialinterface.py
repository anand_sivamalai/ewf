# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''This is the serial port interface class that is inherited from the
CommInterface class.
'''

import sys
import binascii
import debug
import comminterface
from configuration import config

from threading import Thread, Lock
from struct import pack

# Add the test API library to the path.
# This is because we have the pyserial as a subdirectory..
import inspect, os
sys.path.append(os.path.dirname(inspect.getfile(inspect.currentframe())))

import serial.tools.list_ports
import time
from framecodec_serial import FrameCodecSerial

from itertools import product, dropwhile
from serial.serialutil import SerialException

from meshapicommand import MeshApiCommand
from Queue import Queue, Empty

# Used ports and a related lock
used_ports = set()
used_ports_lock = Lock()

def find_devices(interface=None, configname='serial'):
    '''
    A function to find all available serial devices.

    - Returns a list of serial device instances.
    '''

    debug.info("Finding serial devices..")
    devices = []

    if interface is None:
        interface = SerialInterface

    # Find all available serial ports.

    # Are the forced ports?
    if config.has_option(configname, 'forced_ports'):
        # Create the forced ports to the list
        unfiltered = map(lambda x: (x['port'], 'Forced', 'Forced', x),
                         config.get(configname, 'forced_ports'))
        # Check if we also scan with forced ports.
        if config.has_option(configname, 'scan_with_forced_ports'):
            if config.get(configname, scan_with_forced_ports):
                # Add rest of the ports to the list
                unfiltered += list(serial.tools.list_ports.comports())

        # Create a set of devs so we drop the duplicates.
        devs = set(map(lambda x: x[0], unfiltered))

        # Create a list that doesn't have the duplicates.
        all_ports = []
        for dev in devs:
            for port in unfiltered:
                if port[0] == dev:
                    all_ports.append(port)
                    break
    else:
        # If there were no forces ports we can just use the list_ports.
        all_ports = list(serial.tools.list_ports.comports())

    # Try to create an instance from them.
    for port in all_ports:
        # Only use unused ports
        if not is_used_port(port[0]):
            try:
                devices.append(interface(port, configname))
            except Exception as ex:
                debug.error('Opening port ' + str(port) +
                            ' failed. Reason: "' + str(ex) +
                            '". Skipping port.')

    # Only use serial devices that could be opened.
    devices = filter(lambda x: x.get_connected(), devices)

    # Add ports to the used ports set
    for port_name in map(lambda x: x.ser_port.port, devices):
        add_used_port(port_name)

    debug.info('Found '+str(len(devices))+' serial devices.')

    # Return list of devices.
    return devices

def get_used_ports():
    '''
    Get the list of ports that are already used.
    '''

    global used_ports
    global used_ports_lock

    used_ports_lock.acquire()
    return_list = list(used_ports)
    used_ports_lock.release()

    return return_list

def add_used_port(port):
    '''
    Add a port to the list of ports already used.

    Parameters:

    - port: Port name

    '''

    global used_ports
    global used_ports_lock

    used_ports_lock.acquire()
    used_ports.add(port)
    used_ports_lock.release()

def remove_used_port(port):
    '''
    Remove a port from the list of ports already used.

    Parameters:

    - port: Port name

    '''

    global used_ports
    global used_ports_lock

    used_ports_lock.acquire()
    used_ports.remove(port)
    used_ports_lock.release()

def is_used_port(port):
    '''
    Check if a port is in the list of ports already used.

    Parameters:

    - port: Port name

    '''

    global used_ports
    global used_ports_lock

    used_ports_lock.acquire()
    return_value = port in used_ports
    used_ports_lock.release()

    return return_value

class SerialInterface(comminterface.CommInterface):
    '''
    A class for serial port interface. Inherited from the CommInterface class.
    '''

    # Max retries before giving up serial writes.
    max_retries = 10

    def __init__(self, port, configname = 'serial'):
        '''
        Init takes the serial tools port as parameter.
        '''

        # Serial tools dev string.
        self.dev = ""
        # Serial tools desc string.
        self.desc = ""
        # Serial tools hw string.
        self.hw_string = ""
        # Connection status.
        self.connected = False
        # Serial port instance.
        self.ser_port = None

        # Framecodec instance
        self.framecodec = None

        # Configuration name.
        self.configname = configname

        # Store port information. Do this before calling creating interface
        self.dev = port[0]
        self.desc = port[1]
        self.hw_string = port[2]

        super(SerialInterface, self).__init__()

        self.debug_info("Trying to initialize a serial interface: "+self.dev)

        # Test the PID against know PIDs list.
        pid_found = False
        for pid in config.get(self.configname, 'usb_pids'):
            if self.hw_string.count('USB VID:PID='+pid) > 0 or self.hw_string == 'Forced':
                pid_found = True
                break
            pids = pid.split(':')
            if self.hw_string.count('VID_{}+PID_{}'.format(pids[0], pids[1])) > 0:
                pid_found = True
                break

        if not pid_found:
            self.debug_info("Not supported PID {}".format(pid))
            return

        self.debug_info('Found serial port:{}'.format(port))

        connection_works = False

        # These are common settings.
        self.ser_port = serial.Serial()
        self.ser_port.port = self.dev
        self.ser_port.timeout = 0.1
        self.ser_port.xonxoff = False
        self.ser_port.rtscts = False
        self.ser_port.dsrdtr = False
        self.ser_port.write_timeout = 1.0

        # Get write chunk size
        try:
            self.chunk_size = config.get(self.configname, 'chunk_size')
        except AttributeError:
            self.chunk_size = 32

        # Check if the port was forced.
        if self.hw_string == 'Forced':
            # It was forced. Get the settings.
            self.debug_info("Port was forced. Using forced settings.")
            settings = port[3]

            try:
                self.ser_port.bytesize = self._databits_to_bytesize(settings['databits'])
            except KeyError:
                self.ser_port.bytesize = self._databits_to_bytesize(8)

            try:
                self.ser_port.parity = self._parity_to_parity(settings['parity'])
            except KeyError:
                self.ser_port.parity = self._parity_to_parity('none')

            try:
                self.ser_port.stopbits = self._stopbits_to_stopbits(settings['stopbits'])
            except KeyError:
                self.ser_port.stopbits = self._stopbits_to_stopbits(1)

            try:
                self.baudrate = settings['baudrate']
            except KeyError:
                self.baudrate = 115200

            self.ser_port.baudrate = self.baudrate

            # Try to open the port.
            self.ser_port.open()

        else:
            # Get the attribute lists or use hardcoded values if not available.

            try:
                baudrates_list = config.get(self.configname, 'baudrates')
            except AttributeError:
                baudrates_list = [115200, 125000]

            try:
                parities_list = config.get(self.configname, 'parities')
            except AttributeError:
                parities_list = ['none']

            try:
                databits_list = config.get(self.configname, 'databits')
            except AttributeError:
                databits_list = [8]

            try:
                stopbits_list = config.get(self.configname, 'stopbits')
            except AttributeError:
                stopbits_list = [1]

            # Check through all serial port parameters which combination works.
            for baudrate, parity, databits, stopbits in product(baudrates_list,
                                                                parities_list,
                                                                databits_list,
                                                                stopbits_list):

                self.debug_info("Trying to communicate: baudrate="+
                                str(baudrate)+", parity="+str(parity)+
                                ", databits="+str(databits)+", stopbits="+str(stopbits))

                self.ser_port.bytesize = self._databits_to_bytesize(databits)
                self.ser_port.parity = self._parity_to_parity(parity)
                self.ser_port.stopbits = self._stopbits_to_stopbits(stopbits)

                # Try to open the port.
                retry = 0
                while (True):
                    try:
                        if not self.ser_port.isOpen():
                            self.ser_port.open()
                            # FTDI workaround, wait for 2 seconds before first sending
                            time.sleep(2)
                    except Exception:
                        # Newly connected devices take a while to open the serial port. So we wait.
                        if retry < self.max_retries:
                            self.debug_info("Port busy. Wait 5s and retry.")
                            time.sleep(5)
                            retry += 1
                            continue
                        else:
                            self.debug_info("Maximum retries reached. Port {} could not be opened".format(self.dev))
                            return
                    else:
                        # Can open the port. Let's test the connection.

                        # This is a stack state query..
                        frame = pack('<BBBBBBBBBBBBB',0xc0,0xc0,0xc0,0xc0,0xc0,0x0c,0x00,0x02,0x01,0x00,0x76,0xc7,0xc0)

                        # Write test packet
                        self.debug_info("Writing test packet: "+binascii.hexlify(frame))
                        self.baudrate = baudrate
                        self.ser_port.baudrate = baudrate
                        self.ser_port.reset_input_buffer()
                        self.ser_port.reset_output_buffer()
                        write_try_count = 0
                        while True:
                            try:
                                self.ser_port.write(frame)
                                break
                            except Exception as e:
                                self.debug_error("Serial write error: "+str(e))
                                time.sleep(0.1)
                                if write_try_count == 10:
                                    return
                                write_try_count += 1

                        # Read back data.
                        self.debug_info("Reading back data")
                        ba = bytearray()
                        while(1):
                            try:
                                byte = self.ser_port.read(1)
                            except:
                                self.debug_error("Failed to read data back. Connection possibly already in use.")
                                return

                            if byte == '':
                                break
                            ba = ba+bytearray(byte)

                        self.debug_info("Got: "+binascii.hexlify(ba))

                        connection_works = False

                        # Test that the read back data is a SLIP frame.

                        # Remove beginning zeroes..
                        ba = bytearray(dropwhile(lambda x: x==0, ba))

                        # Remove trailing zeroes..
                        ba = bytearray(dropwhile(lambda x: x == 0, ba[::-1]))[::-1]

                        if len(ba) > 2:
                            if ba[0] == 0xc0 and ba[-1] == 0xc0:
                                connection_works = True
                                self.debug_info("Connection works!")
                                # We can close the connection now. The interface
                                # will reopen it.
                                self.ser_port.close()
                                break

                        # Message was not responded, try again
                        retry += 1
                        if retry < self.max_retries:
                            break
                        self.debug_info('Port did not reply, try again')
                        time.sleep(1)

                if connection_works:
                    break

            if not connection_works:
                # Connection didn't work after all the tries..
                return

        # Open the port.
        self._tx_thread = None
        self._rx_thread = None
        self.open()

        # Start this thread.
        try:
            self.start()
        except:
            self.debug_error("Thread starting failed!")
            self.close()

        #self.set_command_base()

    def get_connected(self):
        '''
        Just to get connection status.

        - Returns True if connection is open, False if not.
        '''

        return self.connected

    def is_local(self):
        '''
        To check if connection is local (not over TCP/IP or similar).

        - Return True if connection is local, False if not.
        '''

        return True

    def __repr__(self):
        return "Serial Port "+self.dev

    def _databits_to_bytesize(self, bits):
        '''
        Converts numeric databits value to enums used by serial port interface.
        '''

        conv_table = {
            5:serial.FIVEBITS,
            6:serial.SIXBITS,
            7:serial.SEVENBITS,
            8:serial.EIGHTBITS
            }
        value = None
        try:
            value = conv_table[bits]
        except:
            self.debug_error("Invalid bit setting!")

        return value

    def _parity_to_parity(self, parity):
        '''
        Converts the parity string value to enums used by serial port interface.
        '''
        conv_table = {
            "none":serial.PARITY_NONE,
            "even":serial.PARITY_EVEN,
            "odd":serial.PARITY_ODD,
            "mark":serial.PARITY_MARK,
            "space":serial.PARITY_SPACE
            }
        try:
            value = conv_table[parity]
        except:
            self.debug_error("Invalid parity setting!")

        return value

    def _stopbits_to_stopbits(self, stopbits):
        '''
        Converts the numeric stopbits value to enums used by serial port interface.
        '''
        conv_table = {
            1:serial.STOPBITS_ONE,
            1.5:serial.STOPBITS_ONE_POINT_FIVE,
            2:serial.STOPBITS_TWO
            }
        try:
            value = conv_table[stopbits]
        except:
            self.debug_error("Invalid stopbits setting!")

        return value

    def run(self):
        '''
        The run method. Only starts the TX and RX threads in serial interface.
        '''

        # Test that all the subthreads and the parent (i.e. main program) is running.
        # If not then kill the interface.
        while self.parent_thread.isAlive():
            time.sleep(0.1)

        self.kill()

    class TXThread(Thread):
        '''
        TX Thread class.

        Started by the SerialInterface open method.
        '''

        def __init__(self, parent):
            '''
            Just the init function.
            '''

            self.parent = parent
            super(parent.TXThread, self).__init__(name='SerialTXThread '+str(self.parent))

        def run(self):
            '''
            Run the thread.
            '''
            self.parent.debug_info(str(self.parent) + " TX thread started")

            # Run until parent is disconnected.
            while self.parent.connected:
                try:
                    # Get TX item.
                    # NOTE: No blocking timeout because it causes 30-50ms delay
                    #    in responses!
                    item = self.parent.tx_queue.get(False)
                except Empty:
                    # Sleep a bit and try again.
                    time.sleep(0.001)
                    continue

                if item is None:
                    self.parent.debug_error("Serial TX queue contained a "
                                            "NoneType object!")
                    self.parent.kill()

                # Encode frame.
                frame = self.parent.framecodec.encode(item.item)
                try:
                    # Split frame into pieces and write to serial port.
                    while frame and self.parent.connected:
                        chunk = frame[:self.parent.chunk_size]
                        frame = frame[self.parent.chunk_size:]
                        self._send_frame(chunk)

                except SerialException as e:
                    # Something went wrong in sending the frame..
                    self.parent.debug_error("Serial exception in send frame: " +
                                            str(e))
                    self.parent.kill()

                # Special case, release after sending the item
                if not item.is_waiting_responses():
                    self.parent._end_tx(item)

            self.parent.debug_info("Serial port TX thread killed.")

        def _send_frame(self, frame):
            '''
            Sends a frame of data to the serial port.

            - frame, the frame in bytearray
            '''

            self.parent.debug_info("Serial write:{}".format(binascii.hexlify(frame)))
            if self.parent.connected:
                write_try_count = 0
                while write_try_count < self.parent.max_retries:
                    try:
                        self.parent.ser_port.write(frame)
                        return

                    except Exception as e:
                        self.parent.debug_error("Serial write error. Try number: "+str(write_try_count)+
                                         ": "+str(e))
                        time.sleep(0.1)
                        write_try_count += 1

                self.parent.debug_error("Serial write error. Max count reached. Killing interface: "
                                        + str(e))
                self.parent.kill()

    class RXThread(Thread):
        '''
        RX Thread class.

        Started by the SerialInterface open method.
        '''

        def __init__(self, parent):
            '''
            Just the init.
            '''
            self.parent = parent
            super(parent.RXThread, self).__init__(name='SerialRXThread ' +
                                                  str(self.parent))

        def run(self):
            '''
            Run the thread.
            '''
            self.parent.debug_info(str(self.parent) + " RX thread started")

            # Run until the parent is disconnected.
            while self.parent.connected and self.parent.alive:
                response = None

                # Read until we get a full response or thread ended
                while response is None and self.parent.connected \
                      and self.parent.alive:
                    try:
                        response = self._read_response()

                        # Handle the debug prints coming from framecodec.
                        self.parent._handle_debug_print_queue()

                        if response is not None:
                            # Call the interface handle response function.
                            try:
                                self.parent._handle_incoming_packet(response)
                            except Exception as e:
                                self.parent.debug_error("RX thread error: " +
                                                        str(e))
                                self.parent.kill()

                    except SerialException as e:
                        # Serial exception happened for some reason.
                        self.parent.debug_error("Serial exception in read "
                                                "response")
                        self.parent.kill()

            # RX thread killed.
            self.parent.debug_info("Serial port RX thread killed.")

        def _read_bytes(self):
            '''
            Reads data from serial port.

            - Returns the data in a bytearray. If no data was available then an empty bytearray is returned.
            '''

            try:
                # Read rest of the bytes with shorter timeout, for faster reaction.
                num_bytes = self.parent.ser_port.in_waiting
                if num_bytes == 0:
                    return bytearray()
                data = bytearray(self.parent.ser_port.read(num_bytes))
                self.parent.debug_info("Serial read:{}".format(binascii.hexlify(data)))

            except (serial.SerialException, IOError, TypeError) as e:
                # Error, close connection.
                return bytearray()

            return data

        def _read_response(self):
            '''
            Reads a response from serial port and decodes it. Waits until a full frame is collected.

            - Returns the decoded frame.
            '''

            # Try to decode an empty bytearray which causes the frame codec to use it's internal buffer.
            retval = self.parent.framecodec.decode(bytearray())

            if retval is not None:
                # There was a full frame in frame codec buffer. Return it.
                return retval

            # Read data until we have a full frame.
            # Note this is spinlock to avoid waiting which would result buffer overrun
            # We let Python threading/GIL to give fairness to other threads
            while retval is None and self.parent.connected:
                if self.parent.ser_port.in_waiting > 0:
                    data = self._read_bytes()
                    retval = self.parent.framecodec.decode(data)
                else:
                    # Insert slight delay here to give processing power to other threads
                    time.sleep(0.001)

            # Return the frame.
            return retval

    def open(self):
        '''
        A method to open the interface.
        '''

        self.debug_info("Opening the "+str(self))

        # Serial open is tried to be reopened.
        # It has possibly died so close it first.
        if self.connected:
            self.debug_info("Serial port already open")
            self.debug_info("Try to close it first before continuing..")
            self.close()

        # Flush the port.
        self.ser_port.baudrate = self.baudrate

        # Try to open the port.
        try:
            self.debug_info("Try to open the port")
            self.ser_port.open()
        except serial.SerialException as ex:
            # Port was already open try close and reopen.
            if str(ex) == "Port is already open.":
                self.debug_info("Port is already open")
                self.debug_info("Try to close it..")
                self.ser_port.close()
                try:
                    self.debug_info("Serial port open retry..")
                    self.ser_port.open()
                except serial.SerialException as ex:
                    # Closing didn't help. Just raise the exception.
                    self.debug_error("Still couldn't open the serial port.. "
                                     "Reason: " + str(ex))
                    raise ex
            else:
                raise ex

        self.ser_port.reset_input_buffer()
        self.ser_port.reset_output_buffer()

        # Start a new framecodec.
        self.framecodec = FrameCodecSerial()

        # Set connection as connected.
        self.connected = True

        # Call parent class.
        super(SerialInterface, self).open()

        # Start rx and tx threads
        self._tx_thread = self.TXThread(self)
        self._tx_thread.start()

        self._rx_thread = self.RXThread(self)
        self._rx_thread.start()

        # Start indication poll.
        self.start_indication_poll()

    def kill(self):
        '''
        Kill the interface.

        The interface cannot be revived after a kill.
        '''

        if not self.alive:
            return

        super(SerialInterface, self).kill()
        try:
            self.ser_port.close()
        except:
            self.debug_info("Closing the serial port failed!")

        if self.ser_port is not None:
            if is_used_port(self.ser_port.port):
                remove_used_port(self.ser_port.port)

    def close(self):
        '''
        A method to close the interface.
        '''
        self.debug_info("Closing the "+str(self))

        # Close only if connected.
        if self.connected:
            super(SerialInterface, self).close()

            if self.ind_thread is not None:
                self.ind_thread.stop()

            self.connected = False

            # The RX and TX threads might be dead already because of an error.
            if self._tx_thread is not None:
                self._tx_thread.join()
                self._tx_thread = None

            if self._rx_thread is not None:
                self._rx_thread.join()
                self._rx_thread = None

            self.debug_info("Child threads dead.")
        else:
            self.debug_info("Serial port already closed")

    def send_request(self, request, timeout=5):
        '''
        Send a request.
        '''

        self.debug_info("Send request:"+str(request))
        if not (self.connected and self.alive):
            self.debug_info("Serial port was closed. Send request failed.")
        retval = super(SerialInterface, self).send_request(request,
                                                           timeout=timeout)
        return retval
