# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
Safe OTAP script to do OTAP according Wirepas instructions.

Created on 2.2.2017

'''

from wirepas.meshapi import *
from wirepas.configuration import config
from wirepas.otapimage import *

import argparse
import time
from Queue import Empty
import binascii
import sys

def load_csv_nodelist(filename):
    f = open(filename)

    nodeset = set()
    
    for line in f:
        if line == '\n':
            continue
        nodeset.add(int(line))

    return nodeset

def nodeset_sorted_str(nodeset):
    sorted = list(nodeset)
    sorted.sort()
    s = ', '.join(map(str, sorted))
    return s

def print_status_information(status_dict):
    print "Scratchpad length: "+str(status_dict['scratchpad_length'][0])
    print "Scratchpad CRC: "+hex(status_dict['crc'][0])
    print "Scratchpad sequence number: "+str(status_dict['otap_seq'][0])
    print "Scratchpad type: "+str(status_dict['scratchpad_type'][0])
    print "Scratchpad status: "+str(status_dict['scratchpad_status'][0])
    print "Processed scratchpad length: "+str(status_dict['processed_length'][0])
    print "Processed CRC: "+hex(status_dict['processed_crc'][0])
    print "Processed sequence number: "+str(status_dict['processed_seq'][0])
    print "FW memory area ID: "+str(status_dict['fw_mem_area_id'][0])
    print "FW major version: "+str(status_dict['fw_major_version'][0])
    print "FW minor version: "+str(status_dict['fw_minor_version'][0])
    print "FW maintenance version: "+str(status_dict['fw_maintenance_version'][0])
    print "FW development version: "+str(status_dict['fw_development_version'][0])

def progress_cb(percentage):
    if percentage == 0:
        return
    
    if percentage % 10 == 0:
        print str(percentage)+'%'
    else:
        print ".",
        sys.stdout.flush()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--csvlist', default=None, help="A CSV list of the nodes")
    parser.add_argument('-n', '--nodelist', default=None,
                        help="A comma separated node list from command line")
    parser.add_argument('-i', '--inifile', default=None, help=".ini file for the Python API")
    parser.add_argument('--scantime', default=None, help="An optional scan time for nodes in minutes")
    parser.add_argument('--voltage', default="2.5",
                        help="Voltage level for voltage check in volts. Default 2.5V.")
    parser.add_argument('-s', '--scratchpad', default=None, help="The scratchpad file")
    parser.add_argument('-q', '--sequence', default=None,
                        help="Override the OTAP sequence with this value.")
    parser.add_argument('--statusinterval', default="60",
                        help="Status request interval in seconds. Default 60s.")
    parser.add_argument('-r', '--rebootdelay', default="600",
                        help='The reboot delay in seconds. Default 600s.')
    parser.add_argument('-u', '--updateinterval', default="120",
                        help='The delay between update request. Default 120s.')
    parser.add_argument('-a', '--application', action="store_true",
                        help="Application mode. Don't check the processed sequence numbers")
    parser.add_argument('--nosinks', action="store_true")
    
    args = parser.parse_args()

    
    nodeset = set()

    if args.csvlist != None:
        nodeset.update(load_csv_nodelist(args.csvlist))

    if args.nodelist != None:
        nodeset.update(set(map(int, args.nodelist.split(','))))
        
    if len(nodeset) != 0:
        print "User inputed nodes: "+nodeset_sorted_str(nodeset)
        
    if args.inifile != None:
        print "Loading .ini-file: "+str(args.inifile)
        config.load_file(args.inifile)

    print "Searching for devices.."
    network = MeshApiNetwork()
    network.find_devices()

    devices = network.get_devices()
    if len(devices) == 0:
        print "No devices found"
        exit(1)
        
    print "Found devices: "+str(devices)

    sinks = network.get_sinks()
    
    if len(sinks) == 0:
        print "At least one sink required"
        exit(1)

    sink_addresses = map(lambda x: x.get_address(), sinks)

    print "Sinks: "+", ".join(map(str, sink_addresses))

    print "Confirming that the sink stacks are started.."
    for sink in sinks:
        sink.stack_start(autostart=True)
    
    if args.scantime != None:
        scantime = int(args.scantime)
        print "Scanning for more nodes for "+str(scantime)+" minutes.."
        start_time = time.time()

        for sink in sinks:
            sink.start_data_rx()
            
        while time.time() - start_time < 60*scantime:
            for sink in sinks:

                try:
                    data = sink.get_data_rx(timeout = 1)
                except Empty:
                    continue
                
                source = data['source_address']
                if not ((source in nodeset) or (source in sink_addresses)):
                    print "New node found: "+str(source)
                    nodeset.add(source)
                    
        for sink in sinks:
            sink.stop_data_rx()

                    
    print "Total nodes found: "+nodeset_sorted_str(nodeset)

    if len(nodeset) == 0:
        print "No nodes in the network. Exiting.."
        exit(1)

    print "Check for voltages. Limit is: "+str(args.voltage)+"V"
    voltages = {}
    for node in nodeset:
        voltages[node] = None

    for sink in sinks:
        sink.start_node_diag_rx()

    old_missing_len = 0
    
    while True:
        missing = map(str, filter(lambda x: voltages[x] == None, nodeset))
        if len(missing) == 0:
            break

        missing.sort()
        if old_missing_len != len(missing):
            print "Still missing: "+", ".join(missing)
            old_missing_len = len(missing)
        
        for sink in sinks:
            try:
                ndiag = sink.get_node_diag_rx(timeout=1)
            except Empty:
                continue

            source = ndiag['source_address']

            if source in nodeset:
                voltage = 2.0 + ndiag['voltage']/100.0
                if voltages[source] == None:
                    voltages[source] = voltage
                    if voltage >= float(args.voltage):
                        ok_string = "OK"
                    else:
                        ok_string = "FAIL"

                    print "\tNode: "+str(source)+": "+str(voltage)+"V .. "+ok_string

                    if ok_string == "FAIL":
                        print "Failed the voltage test."
                        exit(1)

    for sink in sinks:
        sink.stop_node_diag_rx()

    scratchpad_sequences = set()
    processed_sequences = set()
        
    print "Get scratchpad status from sinks: "
    for sink in sinks:
        print "Sink: "+str(sink.get_address())
        image_status = sink.get_otap_image_status()
        print_status_information(image_status)
        scratchpad_sequences.add(image_status['otap_seq'][0])
        processed_sequences.add(image_status['processed_seq'][0])
        print "---------------------------------"

    print "Query scratchpad status from the network.."
    for sink in sinks:
        sink.wait_remote_status_indication_rx()
        sink.otap_remote_status()

    otap_seqs = {}
    processed = {}
    
    for node in nodeset:
        otap_seqs[node] = None
        processed[node] = None

    old_missing_len = 0

    start_time = time.time()

    statusinterval = float(args.statusinterval)
    
    while True:
        missing = map(str, filter(lambda x: otap_seqs[x] == None, nodeset))
        if len(missing) == 0:
            break

        if (time.time() - start_time) > statusinterval:
            for sink in sinks:
                print "Still missing: "+", ".join(map(str, missing))
                print "Sending a remote status query.."
                sink.otap_remote_status()
            start_time = time.time()
        
        missing.sort()
        if old_missing_len != len(missing):
            print "Still missing: "+", ".join(missing)
            old_missing_len = len(missing)
        
        for sink in sinks:
            try:
                otap_status = sink.get_remote_status_indication_rx(timeout=1)
            except Empty:
                continue

            source = otap_status['source_address']

            if source in nodeset:

                if otap_seqs[source] == None:
                    otap_seqs[source] = otap_status['otap_seq']
                    processed[source] = otap_status['processed_seq']

                    scratchpad_sequences.add(otap_status['otap_seq'])
                    processed_sequences.add(otap_status['processed_seq'])

                    print "\tNode: "+str(source)+": OTAP seq: "+str(otap_status['otap_seq'])+\
                        " Processed seq: "+ str(otap_status['processed_seq'])

    if 255 in scratchpad_sequences:
        print "Scratchpad sequence 255 found in network. Unable to update."
        exit(1)

    unotapped = set()

    for key in otap_seqs:
        if otap_seqs[key] == 0:
            unotapped.add(key)

    if 0 in scratchpad_sequences:
        scratchpad_sequences.remove(0)

    print "Image sequences in the network: "+", ".join(map(str, scratchpad_sequences))
    print "Processed image sequences in the network: "+", ".join(map(str, processed_sequences))
    print "Nodes with the OTAP turned off (sequence 0): "+", ".join(map(str, unotapped))

    if len(scratchpad_sequences) > 1:
        print "More than one OTAP sequences. Network in unstable state."
        exit(1)

    if len(scratchpad_sequences) == 0:
        print "All devices have OTAP turned off!"
        exit(1)

    if args.sequence:
        otap_sequence = int(args.sequence)
    else:
        otap_sequence = (max(scratchpad_sequences) % 254) + 1
        
    print "The used OTAP sequence will be "+str(otap_sequence)

    otapped = nodeset-unotapped

    print "The nodes to be updated are: "+", ".join(map(str, otapped))
    
    print "---------------------------------------------"
    print "Network in a stable state. Starting OTAP."
    print "Loading the OTAP file.."

    if args.scratchpad == None:
        print "No scratchpad image defined!"
        exit(1)
    
    image = OTAPImage()
    image.load_file(args.scratchpad)
    print "File tag: "+binascii.hexlify(image.file_tag)
    print "Image length: "+str(image.length)
    print "CRC: "+hex(image.crc)
    print "Sequence number: "+str(image.sequence_number)
    print "Pad: "+str(image.pad)
    print "Type: "+str(image.image_type)
    print "Image start: "+binascii.hexlify(image.image[:32])
    
    print "Uploading the image to sinks"    
    for sink in sinks:
        print "Sink "+str(sink.get_address())
        print "Start listening for OTAP status messages"
    
        print "Stop stack.."
        sink.stack_stop()

        print "Upload scratchpad image.."
        result = sink.load_otap_image(image, otap_sequence = otap_sequence,
                                        progress_callback = progress_cb, progress_step=1)
        if result == MsapScratchpadBlockResult.SuccessAllData:
            print "Image loaded succesfully!"
        else:
            print "ERROR: Image loading failed!"
            exit(1)

        print "Start stack.."
        sink.stack_start(autostart=True)

    print "Wait for the OTAP scratchpad to spread.."

    otap_seqs = {}

    for node in otapped:
        otap_seqs[node] = None

    start_time = time.time()
    while True:
        missing = map(str, filter(lambda x: otap_seqs[x] != otap_sequence, otapped))
        if len(missing) == 0:
            break

        if time.time() - start_time > statusinterval:
            for sink in sinks:
                print "Still missing: "+", ".join(map(str, missing))
                print "Sending a remote status query.."
                sink.otap_remote_status()
            start_time = time.time()
            
        for sink in sinks:
            try:
                otap_status = sink.get_remote_status_indication_rx(timeout=1)                
            except Empty:
                continue

            source = otap_status['source_address']
            
            if source in otapped:
                otap_seqs[source] = otap_status['otap_seq']
                print "\tNode: "+str(source)+": OTAP seq: "+str(otap_status['otap_seq'])

    print "All nodes have the new scratchpad."

    update_interval = float(args.updateinterval)
    reboot_delay = float(args.rebootdelay)

    if reboot_delay < 60:
        print "Reboot delay can't be lower than 60s"
        exit(1)
    
    print "Sending update commands every "+str(update_interval)+" seconds for "+str(reboot_delay)+\
        " seconds.."

    start_time = time.time()
    update_time = reboot_delay
    count = 0

    while (time.time() - start_time < reboot_delay) and (update_time >= 60):
        print "Sending an update request with delay "+str(update_time)+" seconds"
        for sink in sinks:
            print "Sink: "+str(sink.get_address())
            sink.otap_remote_update_request(ADDRESS_BROADCAST, otap_sequence, reboot_delay=update_time)

        count += 1

        while (time.time() - start_time) < count*update_interval:
            for sink in sinks:
                try:
                    otap_status = sink.get_remote_status_indication_rx(timeout=1)
                    print "\tNode: "+str(otap_status['source_address'])+": OTAP seq: "+\
                        str(otap_status['otap_seq'])+\
                        " Processed seq: "+ str(otap_status['processed_seq'])
                except Empty:
                    continue

        update_time -= update_interval

    print "Wait for the update to take effect.."
    while (time.time() - start_time < reboot_delay+60):
        pass
        
    if not args.application:
        print "Checking the processed sequence numbers"
        for node in otapped:
            processed[node] = None

        status_start_time = time.time()

        for sink in sinks:
            sink.otap_remote_status()
                
        while True:
            missing = map(str, filter(lambda x: processed[x] != otap_sequence, otapped))

            if len(missing) == 0:
                break
                
            if time.time() - status_start_time > statusinterval:
                print "Still missing: "+", ".join(map(str, missing))
                print "Resending status query.."
                for sink in sinks:
                    sink.otap_remote_status()
                    status_start_time = time.time()

            for sink in sinks:
                try:
                    otap_status = sink.get_remote_status_indication_rx(timeout=1)
                except Empty:
                    continue

                source = otap_status['source_address']

                print "\tNode: "+str(source)+": OTAP seq: "+str(otap_status['otap_seq'])+\
                    " Processed seq: "+ str(otap_status['processed_seq'])

                if source in otapped:
                    processed[source] = otap_status['processed_seq']

                    if otap_status['processed_seq'] != otap_sequence:
                        print "\tWrong processed sequence. Resending the update command.."
                        for sink_update in sinks:
                            sink_update.otap_remote_update_request(source, otap_sequence, \
                                                                   reboot_delay=60)

    else:
        print "Application mode. Skipping the processed sequence numbers test"

    if args.nosinks:
        print "Skipping the sink updates."
    else:
        print "Update the sinks also.."
        for sink in sinks:
            print "Sink: "+str(sink.get_address())
            print "Stopping stack.."
            sink.stack_stop()
            print "Set image as bootable.."
            sink.set_otap_image_bootable()
            print "Boot the device.."
            sink.stack_stop()
            print "Starting the stack.."
            sink.stack_start(autostart=True)

            while(sink.get_stack_state() == MsapStackStatus.StackStopped):
                print "Waiting for stack to start.."
                time.sleep(1)        
    print "Done"
