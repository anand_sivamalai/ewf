
# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This is an example of enable beacon and send an ibeacon from sink.
Note: Always better to use eddystone or ibeacon, since there are scan applications for phone

It works as following:
. compose the data package(mode + interval + beacon payload)
. send the data package (endpoint 255->242) to the target node so the node will send the beacon

To customize the beacon, the interval, uuid, version, power can be changed

'''

import random
from wirepas.beacon.beacondata_command import *
from wirepas.meshapi_beacon_functions import *
from wirepas.meshapi import *

beacon = BeaconCommandMessage()

# set the mode as 1 to enable the sending
moderequest1 = beacon.generate_mode_command(value=ModeValue.TX, value_format='uint8')

# Set the beacon send interval, unit is ms, the range is 100-60000
tx_interval = beacon.generate_frame_tx_interval(value=100, value_format='uint16')

# random address
address = random.randint(1, 0xffffffffffff)
print "Info**the address in beacon received by phone is {}".format(hex(address))

# If you set the uuid in decimal, use the following two lines code
uuid_in_hex_decimal = '666666666666666631323334616161ae'
uuid_string = binascii.unhexlify(uuid_in_hex_decimal)

# If you set the uuid in string, use the following two lines code
uuid_string = "Iwanttosetuuiddd"
uuid_in_hex_decimal = binascii.hexlify(uuid_string)

#print "uuid in string is {}".format(uuid_string)
print "uuid in hex decimal format is {}".format(uuid_in_hex_decimal)

beacon_generated = beacon.create_ibeacon(index=0,
            device_address=address,
            uuid=uuid_in_hex_decimal, # if not set it will come from address
            major=None,               # if not set it will come from address
            minor=None,               # if not set it will come from address
            measured_power=4-41)      # TRP - attenuation at 1m

beacon_request = beacon.generate_payload(beacon_generated, value_format='string')

# Print the command, can copy this command and use it in wirepas terminal
one_command = binascii.hexlify( moderequest1+tx_interval+beacon_request)
print "Info**the command is going to sent to target device is {}".format(one_command)

# Find the sinks
network = MeshApiNetwork()
network.find_devices()
sinks = network.get_sinks()

# Be sure there is at least one sink
if len(sinks) == 0:
    print "Warning** Needs at least one sink!"
    exit(0)
else:
    for sink in sinks:
        retval = sink.get_stack_state()
        if retval != 0:
            try:
                print "Info** Sink {} is off, will start the stack".\
                    format(sink.get_address())
                sink.stack_start(autostart=True)
            except Exception as ex:
                print 'Warning** Sink {} stack is off and cannot be started,ex:{}'.\
                    format(sink.get_address(), ex)

        print "Info**Send the command to the target device(here used sink) now"
        sink.beacon_request_tx(data= moderequest1+tx_interval+beacon_request,
                               dest_address=sink.get_address(),
                               src_endpoint=BEACON_ENDPOINT_REQUEST_SRC,
                               dst_endpoint=BEACON_ENDPOINT_REQUEST_DST,
                               timeout=60)
