# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
The tool to generate the remote comment

Created 23.11.2016

'''
import argparse
import ast
import sys
from wirepas.remote_api.remote_api import *
from wirepas.beacon.beacondata_command import *

csap_attribute = \
    { 1:CsapAttribId.NodeAddress,
      2:CsapAttribId.NetworkAddress,
      3:CsapAttribId.NetworkChannel,
      4:CsapAttribId.NodeRole,
      13:CsapAttribId.CipherKey,
      14:CsapAttribId.AuthenticationKey,
      20:CsapAttribId.OfflineScan,
      21:CsapAttribId.ChannelMap,
      22:CsapAttribId.FeatureLockBits,
      23:CsapAttribId.FeatureLockKey
     }

msap_attribute = \
    {
      9:MsapAttribId.AccessCycleRange
     }

def lock(delay,feature,key):
    '''
    This is a function to compose all the command together for lock one feature
    Args:
        delay:  the delay for the lock taking effect
        feature: the lock feature
        key: the key used for locking the feature
    Returns:

    '''
    instance = RemoteAPICommandMessage()
    start = instance.begin_withoutkey_command()
    lock_feature = instance.csap_write(CsapAttribId.FeatureLockBits,int(0xffffffff-feature))
    key = instance.csap_write(CsapAttribId.FeatureLockKey,key)
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start+lock_feature+key+end+update)

def unlock(delay,key):
    '''
    This is a function to compose all the command together for unlocking all the feature
    Args:
        delay:  the delay for the unlock taking effect
        key: the key used for unlocking the feature
    Returns:

    '''
    instance =RemoteAPICommandMessage()
    start_withkey = instance.begin_withkey_command(key)
    key = instance.csap_write(CsapAttribId.FeatureLockKey,convert_keys_to_binary('ffffffffffffffffffffffffffffffff'))
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start_withkey+key+end+update)

def set_msap(delay,attributeid, value):
    '''
    This function composes all the commands needed for setting the MSAP arribute value
    Args:
        delay:
        attributeid: the attribute ID
        value: the value for this attribute

    Returns:

    '''
    instance = RemoteAPICommandMessage()
    start = instance.begin_withoutkey_command()
    attribute = instance.msap_write(attributeid,value)
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start+attribute+end+update)

def set_csap(delay,attributeid, value):
    '''
    This function composes all the commands needed for setting the CSAP arribute value
    Args:
        delay:
        attributeid: the attribute ID
        value: the value for this attribute

    Returns:

    '''
    instance = RemoteAPICommandMessage()
    start = instance.begin_withoutkey_command()
    attribute = instance.csap_write(attributeid,value)
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start+attribute+end+update)

def read_msap(delay,attributeid):
    '''
    This function composes all the commands needed for reading the msap
    Args:
        delay:
        attributeid: the attribute ID

    Returns:

    '''
    instance = RemoteAPICommandMessage()
    start = instance.begin_withoutkey_command()
    attribute = instance.msap_read(attributeid)
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start+attribute+end+update)

def read_csap(delay,attributeid):
    '''
    This function composes all the commands needed for reading the csap
    Args:
        delay:
        attributeid: the attribute ID

    Returns:

    '''
    instance = RemoteAPICommandMessage()
    start = instance.begin_withoutkey_command()
    attribute = instance.csap_read(attributeid)
    end = instance.end()
    update = instance.update(delay)
    print binascii.hexlify(start+attribute+end+update)

def response_parse(packet):
    '''
    This function decodes the response got from the remote device
    Args:
        packet: the response data got from the remote device in terminal

    Returns:

    '''
    instance = RemoteAPICommandMessage()
    result = instance.handle_response(packet)
    match = {255:"Unknown Request:",
             254:"Invalid Length:",
             253:"Invalid Value:",
             252:"No Space:",
             251:"Invalid Begin:",
             250:"Invalid Broadcast:",
             249:"Write Only:",
             248:"Access Denied"}
    for item in result:
        if item.response_message_type in range(248,256):
            print match[item.response_message_type]+', '.join(map(lambda x: x+':'+str(item.response[x][0]), item.response.keys()))
        else:
            print item,', '.join(map(lambda x: x+':'+str(item.response[x][0]), item.response.keys()))

def convert_keys_to_binary(key):
    '''
    This is to convert the key format
    '''
    # In case the keys are empty strings set them to NoneTypes
    if key == "":
        key = None

    # Convert from a hex string
    if key != None:
        if key[:2] == '0x':
           key = key[2:]
        key = binascii.unhexlify(key)
    return key

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--cmd',
                        required=True,
                        help='lock, unlock, readcsap, writecsap, readmsap, writemsap, parse')
    parser.add_argument('-d', '--delay',
                        default=10,
                        help='The delay when the command will take effect')
    parser.add_argument('-f', '--feature',
                        default= int(0xe66cb3fd),  # This is to enable all feature
                        help="the feature you want to lock, the format is such as 0x00000001 \
                             the define about all bit lock is in FeatureLockBit in testapi_comm_layer/meshapicsap")
    parser.add_argument('-k', '--key',
                        #default="ffffffffffffffff",
                        help="the key to lock or unlock the feature. example:-k 0102030405060708090a0b0c0d0e0f")
    parser.add_argument('-a', '--attribute',
                        help="The attribute which is read or written \
                              1:CsapAttribId.NodeAddress, \
                              2:CsapAttribId.NetworkAddress, \
                              3:CsapAttribId.NetworkChannel, \
                              4:CsapAttribId.NodeRole, \
                              13:CsapAttribId.CipherKey, \
                              14:CsapAttribId.AuthenticationKey, \
                              20:CsapAttribId.OfflineScan, \
                              21:CsapAttribId.ChannelMap, \
                              22:CsapAttribId.FeatureLockBits, \
                              23:CsapAttribId.FeatureLockKey, \
                              9:MsapAttribId.AccessCycleRange ")
    parser.add_argument('-v',
                        '--value',
                        help='the attribute value to be set')
    parser.add_argument('-p',
                        '--packet',
                        help='the response which is needed to be parsed')
    args = parser.parse_args()

    result = None
    if args.cmd == "lock":
        if args.feature is None or args.key is None:
            print "please input the needed arguments : feature and key. " \
                  "example -c lock -f 0x40000000 -k 01020304050607080102030405060708"
            sys.exit(1)
        else:
            lock(delay=int(args.delay), feature=ast.literal_eval(args.feature), key=convert_keys_to_binary(args.key))
    elif args.cmd == "unlock":
        if args.feature is None or args.key is None:
            print "please input the needed arguments : key. " \
                  "example -c unlock -k 01020304050607080102030405060708 "
            sys.exit(1)
        else:
            unlock(delay=int(args.delay), key=convert_keys_to_binary(args.key))
    elif args.cmd == "readcsap":
        if args.attribute is None:
            print "please input the needed arguments : attribute id"
            sys.exit(1)
        elif int(args.attribute) not in csap_attribute.itervalues():
            print "The remote reading to this attribute is not supported"
            sys.exit(1)
        else:
            read_csap(delay=int(args.delay),attributeid=int(args.attribute))
    elif args.cmd == "readmsap":
        if args.attribute is None:
            print "please input the needed arguments: attribute id"
            sys.exit(1)
        elif int(args.attribute) not in msap_attribute.itervalues():
            print "The remote reading to this attribute is not supported"
            sys.exit(1)
        else:
            read_msap(delay=int(args.delay),attributeid=int(args.attribute))
    elif args.cmd == "writecsap":
        if args.attribute is None or args.value is None:
            print "please input the needed arguments : csap attribute id and the attribute value"
            sys.exit(1)
        elif int(args.attribute) not in csap_attribute.itervalues():
            print "the remote writing to this attribute is not supported"
            sys.exit(1)
        elif int(args.attribute) in (13,14):
            set_csap(delay=int(args.delay),attributeid=int(args.attribute), value=convert_keys_to_binary(args.value))
        else:
            set_csap(delay=int(args.delay),attributeid=int(args.attribute), value=int(args.value))
    elif args.cmd == "writemsap":
        if args.attribute is None or args.value is None:
            print "please input the needed arguments: msap attribute id and the attribute value "
            sys.exit(1)
        elif int(args.attribute) not in msap_attribute.itervalues():
            print "the remote writing to this attribute is not supported"
            sys.exit(1)
        else:
            set_msap(delay=int(args.delay),attributeid=int(args.attribute), value=int(args.value))
    elif args.cmd == "parse":
        if args.packet is None:
            print "please input the packet content"
            sys.exit(1)
        else:
            response_parse(packet=convert_keys_to_binary(args.packet))

