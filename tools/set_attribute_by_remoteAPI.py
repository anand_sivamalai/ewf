# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''Uses all connected sinks to send Remote API commands.

usage: set_attribute_by_remoteAPI.py [-h] [--nodelist NODELIST] [--read READ]
                                     [--write WRITE] [--delay DELAY]
                                     [--cancel CANCEL] [--waiting WAITING]
                                     [--lock LOCK]

optional arguments:
  -h, --help           show this help message and exit
  --nodelist NODELIST  node list,example: 1,2,3, if None, will broadcast
  --read READ          attribute list, example: 1,2,3
  --write WRITE        Comma separated list for attribute and values. Value is
                       <attribute>=<value>, for example 1=4,4=2.
  --delay DELAY        The delay when the command will work in remote node, in
                       seconds
  --cancel CANCEL      cancel the command in waiting status
  --waiting WAITING    the time wait for the response, in seconds
  --lock LOCK          To lock the feature, use "key="
'''

import argparse
import datetime
import time
import re
from Queue import Empty

from wirepas.remote_api.remote_api import *
from wirepas.meshapi import *

# Dictionary for supported CSAP attributes.
csap_attribute = \
    { 1:CsapAttribId.NodeAddress,        # read and write, no broadcast write
      2:CsapAttribId.NetworkAddress,     # read and write
      3:CsapAttribId.NetworkChannel,     # read and write
      4:CsapAttribId.NodeRole,           # read and write
      13:CsapAttribId.CipherKey,         # write only
      14:CsapAttribId.AuthenticationKey, # write only
      20:CsapAttribId.OfflineScan,       # read and write
      21:CsapAttribId.ChannelMap,        # read and write
      22:CsapAttribId.FeatureLockBits,   # read and write
      23:CsapAttribId.FeatureLockKey     # write only
     }

# Dictionary for supported MSAP attributes.
msap_attribute = \
    {
      9:MsapAttribId.AccessCycleRange #read and write
     }

def response_parse(packet, errornode):
    '''Parse a response and check for errors and print errors.

    Args:
       packet (str): Packet APDU payload to be parsed.
       errornode (int): Node address where the packet came from.
    '''

    instance = RemoteAPICommandMessage()
    result = instance.handle_response(packet)
    match = {255:"Unknown Request:",
             254:"Invalid Length:",
             253:"Invalid Value:",
             252:"No Space:",
             251:"Invalid Begin:",
             250:"Invalid Broadcast:",
             249:"Write Only:",
             248:"Access Denied"}
    for item in result:
        if item.response_message_type in range(248,256):
            print "------ERROR response from node ", str(errornode), ':' + \
                match[item.response_message_type] + \
                 ', '.join(map(lambda x: x + ':' + str(item.response[x][0]),
                               item.response.keys()))
        else:
            print "---------", item,', '.join(map(lambda x: x + ':' + \
                                                  str(item.response[x][0]),
                                                  item.response.keys()))

def convert_keys_to_binary(key):
    '''Convert hex string format keys to binary string.

    Args:
        key (str): Hex format string to be converted.

    Returns (str): Binary string containing the key.
    '''
    # In case the keys are empty strings set them to NoneTypes
    if key == "":
        key = None

    # Convert from a hex string
    if key != None:
        if key[:2] == '0x':
           key = key[2:]
        key = binascii.unhexlify(key)
    return key

def _generate_csapwrite(attribute, attribute_value, instance):
    '''Generate a CSAP Write command.

    Args:
      attribute (int): Attribute ID.
      attribute_value (object): Attribute value. String or integer.
      instance (RemoteApiCommandMessage): A RemoteAPICommandMessage instance
          generating the packets.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    if int(attribute) in (13, 14):
        set_csap_attribute = instance.csap_write(attrib_id=int(attribute),
                                value = convert_keys_to_binary(attribute_value))
    else:
        set_csap_attribute = instance.csap_write(attrib_id=int(attribute),
                                                 value=int(attribute_value))
    return set_csap_attribute

def _generate_msapwrite(attribute, attribute_value, instance):
    '''Generate a MSAP Write command.

    Args:
      attribute (int): Attribute ID.
      attribute_value (object): Attribute value. String or integer.
      instance (RemoteApiCommandMessage): A RemoteAPICommandMessage instance
          generating the packets.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    set_msap_attribute = instance.msap_write(attrib_id=int(attribute),
                                             value=int(attribute_value))
    return set_msap_attribute

def _generate_msapread(attribute, instance):
    '''Generate a MSAP Read command.

    Args:
      attribute (int): Attribute ID.
      instance (RemoteApiCommandMessage): A RemoteAPICommandMessage instance
          generating the packets.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    read_msap_attribute = instance.msap_read(attribute=int(attribute))
    return read_msap_attribute

def _generate_csapread(attribute, instance):
    '''Generate a CSAP Read command.

    Args:
      attribute (int): Attribute ID.
      instance (RemoteApiCommandMessage): A RemoteAPICommandMessage instance
          generating the packets.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    read_csap_attribute = instance.csap_read(attribute=int(attribute))
    return read_csap_attribute

# The input is attribute ID=value,attribute ID=value,attribute ID=value
def _create_attribute_write(write_attribute, instance):
    '''Create an attribute write command from a list of attribute IDs.

    Args:
       read_attribute (str): List of attribute IDs in comma separated string.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    write_command = ''
    # Separate all the input
    attribute_inputs = write_attribute.split(',')

    # Generate the command according to the attribute ID
    for attribute_input in attribute_inputs:
        try:
            attribute_id = re.findall(r'\d+', attribute_input.split('=')[0])
            attribute_value = re.findall(r'[0-9a-fA-F]+',
                                         attribute_input.split('=')[1])
            if int(attribute_id[0]) in msap_attribute:
                write_command += _generate_msapwrite(attribute_id[0],
                                                     attribute_value[0],
                                                     instance)
            if int(attribute_id[0]) in csap_attribute:
                write_command += _generate_csapwrite(attribute_id[0],
                                                     attribute_value[0],
                                                     instance)

        except Exception as ex:
            print '{} cannot be parsed,ex:{}'.format(attribute_input, ex)
            raise ex
    return write_command

# To check the sink's role
def _find_sink_role(device):
    role = device.get_role()
    return role

def _create_attribute_read(read_attribute, instance):
    '''Create an attribute read command from a list of attribute IDs.

    Args:
       read_attribute (list): List of attribute IDs.

    Returns (RemoteAPI): RemoteAPI class instance.
    '''

    read_command = ''
    # Separate all the input

    for attribute_id in read_attribute:
        if attribute_id in csap_attribute:
            read_command += _generate_csapread(attribute_id, instance)
        if attribute_id in msap_attribute:
            read_command += _generate_msapread(attribute_id, instance)

    return read_command

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--nodelist', default=None,
                        help="node list,example: 1,2,3,"
                        " if None, will broadcast")
    parser.add_argument('--read',default=None,
                        help="attribute list, example: 1,2,3")
    parser.add_argument('--write', default=None,
                        help='Comma separated list for attribute and values. '\
                        'Value is <attribute>=<value>, for example 1=4,4=2. ')
    parser.add_argument('--delay', default=120,
                        help='The delay when the command will work in '
                        'remote node, in seconds')
    parser.add_argument('--cancel', action="store_true",
                        help='cancel the command in waiting status')
    parser.add_argument('--waiting',default=120,
                        help='the time wait for the response, in seconds ')
    parser.add_argument('--lock', default=None,
                        help='To lock the feature, use "key="')
    args = parser.parse_args()

    nodeset = set()
    response_nodeset = set()
    read_attribute = set()
    write = False
    if args.delay:
        delay = int(args.delay)
    if args.waiting:
        waiting = int(args.waiting)

    instance = RemoteAPICommandMessage()
    command = instance.begin_withoutkey_command()

    # Ensure there is some command
    if (args.read is None) and (args.write is None) and not args.cancel:
        print "Warning** Input at least one valid command!"
        exit(0)

    # Get the node list
    if args.nodelist != None:
        if len(args.nodelist) == 0:
            print "Info** No node address is given, " + \
                "the command will be broadcasted"
        else:
            nodeset.update(set(map(int, args.nodelist.split(','))))

    # attribute read
    if args.read and args.write:
        read_attribute.update(set(map(int, args.read.split(','))))
        command += _create_attribute_read(read_attribute, instance)
    elif args.read and not args.write:
        command = ''
        read_attribute.update(set(map(int, args.read.split(','))))
        command += _create_attribute_read(read_attribute, instance)

    # attribute write, with delay
    if args.write and args.delay:
        write = True
        command += _create_attribute_write(args.write, instance)
        command += instance.end()
        command += instance.update(delay=delay)

    # Cancel
    if args.cancel:
        command = ''
        command += instance.cancel()

    print 'Info** ' + binascii.hexlify(command) + " is going to be sent"

    network = MeshApiNetwork()
    network.find_devices()

    # Get all the sinks
    sinks = network.get_sinks()
    # Be sure there is at least one sink
    if len(sinks) == 0:
        print "Warning** Needs at least one sink!"
        exit(0)
    else:
        for sink in sinks:
            retval = sink.get_stack_state()
            if retval != 0:
                try:
                    print "Info** Sink {} is off, will start the stack".\
                        format(sink.get_address())
                    sink.stack_start(autostart=True)
                except Exception as ex:
                    print 'Warning** Sink {} stack is off and '\
                        'cannot be started,ex:{}'.format(sink.get_address(), ex)

            # Be sure the command is not too long
            mtu = sink.get_mtu()['mtu']
            if len(command)>mtu:
                print "Warning** The command is too long for sink: {}".\
                    format(sink)
                exit(0)
            else:
                # The sink is waiting for the response
                sink.start_data_rx()

                # Get sink address
                sink_address = sink.get_address()

                # The sink is sending the remote api to the nodes
                if nodeset:
                    print 'Info** The command is going to be sent to nodes: ', \
                        " ".join(str(x) for x in (nodeset))

                    print "Info** The sending will take {} seconds if sink is LL or {} seconds if sink is LE"\
                        .format((len(nodeset)*1),(len(nodeset)*4))
                    for node in nodeset:
                        sink.data_tx(data=command, dest_address=node,
                                     src_endpoint=255, dst_endpoint=240)

                        # Send the data with the max access cycle interval
                        sink_role = _find_sink_role(sink)
                        if sink_role == CsapNodeRole.Sink:
                            time.sleep(4)
                        if sink_role == CsapNodeRole.Sink:
                            time.sleep(1)

                    wait_response_time = datetime.datetime.now()
                    print "Info** you chose to wait for {} seconds after the sending".format(waiting)
                    # Get the correct response, get the wrong response or did
                    # not get response at all.
                    while (datetime.datetime.now() -
                           wait_response_time).total_seconds() < waiting:
                        # get the response from all the nodes?
                        if nodeset - response_nodeset == set():
                            print "Info** Get the response from all the nodes"
                            break

                        # Try to get the response
                        try:
                            retvalresponse = sink.get_data_rx(timeout=1)
                        except Empty:
                            continue

                        node = retvalresponse['source_address']
                        if node in response_nodeset or \
                           retvalresponse['source_endpoint'] != 240:
                            continue
                        else:
                            response_nodeset.add(node)

                            print "Info** get response from " + str(node) + \
                                ". Responses from "+ \
                                str(len(response_nodeset))+" node: " + \
                                   " ".join(str(x) for x in
                                            (response_nodeset)) + \
                                   ', still missing ' + \
                                  str(len(nodeset-response_nodeset)) + \
                                  " nodes: " + \
                                  " ".join(str(x) for x in (nodeset -
                                  response_nodeset)) + \
                                  ". Please wait for about "+ \
                                  str(waiting - \
                                      (datetime.datetime.now() - wait_response_time).\
                                      total_seconds()) + "seconds."

                            # Print the response once it gets the error response
                            response_decode = response_parse(packet=\
                                                retvalresponse['apdu'],
                                                errornode = node)

                    print "Info** Timeout and response is missing from " + \
                        str(len(nodeset-response_nodeset)) + \
                          " nodes: " + \
                          " ".join(str(x) for x in (nodeset-response_nodeset))
                else:
                    wait_response_time = datetime.datetime.now()
                    print 'Info** The command is going to be broadcasted.'
                    sink.data_tx(data=command, dest_address=ADDRESS_BROADCAST, \
                                 src_endpoint=255,dst_endpoint=240)

                    while (datetime.datetime.now() - wait_response_time).\
                          total_seconds() < waiting :

                        # Try to get the response
                        try:
                            retvalresponse = sink.get_data_rx(timeout=1)
                        except Empty:
                            continue

                        node = retvalresponse['source_address']
                        if node in response_nodeset or \
                           retvalresponse['source_endpoint'] != 240:
                            continue
                        else:
                            response_nodeset.add(node)

                            print "Info** get response from " + str(node) + \
                                ". Responses from "+ \
                                str(len(response_nodeset)) + " node: " + \
                                   " ".join(str(x) for x in (response_nodeset))\
                                   + ". Please wait for about "+ \
                                  str(waiting-(datetime.datetime.now() -
                                               wait_response_time).total_seconds()) \
                                  + "seconds."

                            # Print the response once it gets the error response
                            response_decode = response_parse(packet=\
                                                retvalresponse['apdu'],
                                                errornode=node)

                    print "Info** Timeout and check the above information to "\
                        "see whether the response has been received from "\
                        "all nodes "
