# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

import argparse
import time


from wirepas.meshapi import *
from wirepas.configuration import config

# Force ignoring of CRC errors
config.framecodec_skip_errors = True

class DebugLogger(object):

    def __init__(self, mincount=None):
        self.network = MeshApiNetwork()
        
        while True:
            self.network.find_devices()
            if mincount is None:
                break
            if len(self.network.get_devices()) >= int(mincount):
                break
            else:
                print 'Found {} devices'.format(len(self.network.get_devices()))

        # Create file instances on every packet
        for device in self.network.get_devices():
            nodeid = device.get_address()
            print 'Opening debug capture on node {}'.format(nodeid)
            f = open('debug_{}.txt'.format(nodeid),'w')

        self.network.stack_start(autostart=True)

    def loop(self):
        while True:
            time.sleep(2)

    def _debug_callback_gen(self, handle):
        '''
        Used to create a callback with a queue.put so we can create queue waits for callbacks.
        '''
        def f(debug):
            _now = datetime.datetime.now()
            rows = debug.split('\n')
            for row in rows:
                if len(row) > 0:
                    handle.write('time:{}:{}\n'.format(_now, row))
                    handle.flush()
        return f

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--minimum',
                        help='Minimum amount of detected devices',
                        default=None)
    args = parser.parse_args()

    instance = DebugLogger(mincount=args.minimum)
    instance.loop()
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
