# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
An example on how to update with OTAP if gateway.py is running with the MySQL
plugin.

Created on: 12.8.2016
'''
import argparse
import datetime
import MySQLdb
import os.path
import pickle
import sys
import time
import warnings
warnings.filterwarnings('ignore', category = MySQLdb.Warning)

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

class OtapUpdater(object):
    '''
    This class handles updating of the firmware in the network.

    It requires gateway.py to be running in all the sinks in order to operate properly
    '''
    # Remote status & remote update resent interval
    remote_status_interval = 300

    # Maximum time to wait for remote status
    remote_status_max_time = 3600
    # Time to take the remote update into use
    remote_update_into_use = 1200
    # Time for network formation
    network_formation_delay = 3600

    def __init__(self,host_ip, port, username, password, db, interval, verbose, reboottime, otapfile, network):
        '''
        Initialize a new instance of OtapUpdater class

        @param host_ip:     IP address of the MySql database
        @param port:        TCP/IP Port number of the MySql database
        @param username:    Username for the database
        @param password:    Password for the database
        @param db:          Database table
        @param interval:    How often the devices are sending the data. Used to evaluate whether devices are online
        @param verbose:     If True: verbose output is used
        @param reboottime:  Time in seconds what to set for update command
        @param otapfile:    Path to the otap file
        @param network:     Network OTAP
        '''
        self.otapfile = otapfile
        if not os.path.isfile(self.otapfile):
            print 'File {} does not exist. Aborting'.format(self.otapfile)
            sys.exit(1)

        self.database = MySQLdb.connect(host=host_ip,
                                   user=username,
                                   passwd=password,
                                   db=db,
                                   port=int(port))
        self.cursor = self.database.cursor()
        self.sinks = []
        self.interval = float(interval)
        print 'Using packet interval of {} seconds'.format(self.interval)
        self.devices_to_otap = []
        self.voltage_limit = 2.5
        self.verbose = verbose
        self.network_otap = network
        OtapUpdater.remote_update_into_use = float(reboottime)
        print 'Using reboot delay of {} seconds'.format(OtapUpdater.remote_update_into_use)

        # Various validity checks
        assert(interval > 0)
        assert(reboottime > (1.5 * OtapUpdater.remote_status_interval))

        self.phase = 1

    def _start_phase(self, description):
        '''
        Start the programming phase

        @param description: Textual description of the phase

        Returns:

        '''
        print 'Phase {}:{}'.format(self.phase, description)
        self.phase = self.phase + 1

    def _find_sinks(self):
        '''
        Find all sinks in the environment
        '''
        # Get all sinks addresses
        query = 'SELECT known_nodes.node_address '\
                'FROM known_nodes '\
                'WHERE known_nodes.node_role & 4 '\
                'GROUP BY known_nodes.node_address;'

        self.cursor.execute(query)
        sinks = self.cursor.fetchall()
        self.sinks = []
        offlinesinks = []
        for sink in sinks:
            if sink[0] in self.devices_to_otap:
                self.sinks.append(sink[0])
            else:
                offlinesinks.append(sink[0])

        if len(offlinesinks) > 0:
            print 'Following sinks are offline:{}'.format(offlinesinks)
            proceed = query_yes_no('Are you absolutely sure you want to proceed?', default='yes')
            self.database.ping(True)
            if not proceed:
                sys.exit(0)


        # Ensure that sinks are online
        self._start_stacks()

    def wait_for_devices_online(self):
        '''
        Wait until devices are online
        '''
        # Wait until devices are online
        starttime = time.time()
        endtime = (3 * self.interval) + starttime

        self._start_phase('Ensure devices to be active')

        # Just read from known nodes which nodes are not online
        query = 'SELECT known_nodes.node_address FROM known_nodes ' \
                'WHERE known_nodes.last_time<from_unixtime({}) '\
                'ORDER BY known_nodes.node_address;'.format(starttime - self.interval)
        while time.time() < endtime:
            time.sleep(10)
            self.cursor.execute(query)
            offlinenodes = self.cursor.fetchall()
            if len(offlinenodes) > 0:
                offlinenodes = [int(x[0]) for x in offlinenodes]
                print 'Waiting for following devices:{}'.format(offlinenodes)
            else:
                print 'All devices online'
                break

        if len(offlinenodes) > 0:
            print 'Could not detect all devices online.'
            proceed = query_yes_no('Are you absolutely sure you want to proceed?', default='no')
            self.database.ping(True)
            if not proceed:
                sys.exit(0)

        query = 'SELECT known_nodes.node_address FROM known_nodes ' \
                'WHERE known_nodes.last_time>from_unixtime({}) '\
                'ORDER BY known_nodes.node_address;'.format(starttime - self.interval)
        self.cursor.execute(query)

        self.devices_to_otap = self.cursor.fetchall()
        self.devices_to_otap = [int(x[0]) for x in self.devices_to_otap]

        self._find_sinks()
        print 'Proceeding otap with following devices:{}'.format(self.devices_to_otap)

    def verify_battery(self):
        '''
        Verify that batteries are in tolerable level
        '''
        # Verify that batteries are in tolerable level
        self._start_phase('Verify adequate battery level')
        
        # Find the latest voltage results from the last 24 hours
        query = 'select source_address, voltage, maxid from ' \
                'diagnostic_node, ' \
                '(select source_address, max(id) as maxid from diagnostic_node as dn, '\
                '	(select * from received_packets where logged_time > date_sub(now(), interval 1 day)) as rp '\
                ' where dn.received_packet = rp.id group by source_address) as miq '\
                ' where received_packet=maxid;'

        self.cursor.execute(query)
        node_voltages = self.cursor.fetchall()
        self.database.commit()
        too_low_voltages = []

        for node_voltage in node_voltages:
            # Obsolete device
            if not int(node_voltage[0]) in self.devices_to_otap:
                continue

            if float(node_voltage[1]) < self.voltage_limit:
                too_low_voltages.append(int(node_voltage[0]))

        if len(too_low_voltages) > 0:
            print 'Following devices have too low voltages:{}'.format(too_low_voltages)
            proceed = query_yes_no('Are you absolutely sure you want to continue anyway?', default='no')
            self.database.ping(True)
            if not proceed:
                sys.exit(0)
        else:
            print 'Battery voltages ok. Proceeding.'

    def otap(self):
        '''
        Do OTAP procedure in safe manner.

        '''

        self._start_phase('Evaluate current scratchpad sequence')
        currentscratchpad = self._get_current_scratchpad(greedy=False)
        new_scratchpad = currentscratchpad + 1
        if new_scratchpad >= 254:
            new_scratchpad = 1
        self._start_phase('Stopping sink')
        self._stop_stacks()
        self._start_phase('Download scratchpads with sequence number {}'.format(new_scratchpad))
        self._download_scratchpads(self.otapfile, new_scratchpad)
        if self.network_otap:
            self._start_phase('Restarting sink')
            self._start_stacks()
            self._start_phase('Evaluating new scratchpad distribution')
            # Wait here for the image to spread to avoid totally useless requests
            since_when = self._get_newest_remote_status_id()
            time.sleep(OtapUpdater.remote_status_interval)
            new_scratchpad = self._get_current_scratchpad(since_when)
            self._start_phase('Send update request')
            self._send_otap_remote_update_request(new_scratchpad)
        self._start_phase('Take image into use in sinks')
        self._stop_stacks()
        self._set_image_bootable()
        self._stop_stacks()
        self._start_stacks()
        self._start_phase('Verify network reformation')
        self._verify_otapped_devices_online(new_scratchpad)

    def _verify_otapped_devices_online(self, scratchpad):
        '''
        Verify that all the devices participating to the OTAP are online

        @param scratchpad: new scratchpad id
        '''
        since_when = self._get_newest_remote_status_id()

        starttime = time.time()
        last_remote_status = starttime

        while True:
            time.sleep(10)

            # Send remote status interval if not received all results
            if (time.time()-last_remote_status) > OtapUpdater.remote_status_interval:
                self._send_scratchpad_query()
                last_remote_status = time.time()

            remote_statuses = self._get_scratchpads(since_when, processed_seq=True)
            booted = self._get_boot_msgs(starttime, scratchpad)

            allfound = True
            for device in self.devices_to_otap:
                # Check out boot messages
                if device in booted:
                    continue

                # Check out remote statuses
                if device in remote_statuses:
                    if remote_statuses[device] == scratchpad:
                        continue
                    else:
                        # Not yet got correct sequence
                        allfound = False
                        break

                # Skip sinks just in case the boot msg is lost
                if device in self.sinks:
                    continue

                # Node not found in either list
                allfound = False
                break

            if allfound:
                print 'OTAP successful'
                return

            if (time.time()-starttime) > OtapUpdater.network_formation_delay:
                print 'Not all the devices returned to network'
                sys.exit(1)

    def _get_boot_msgs(self, since_when, expected_seq):
        """
        Get boot messages from specific time

        @param  since_when:     time.time() format time since when values are ok
        @param  expected_seq:   Scratchpad sequence desired

        @return List of nodes that have updated
        """
        query = 'SELECT received_packets.source_address '\
                'FROM diagnostic_boot '\
                'INNER JOIN received_packets '\
                'WHERE diagnostic_boot.received_packet=received_packets.id '\
                'AND received_packets.launch_time>from_unixtime({}) '\
                'AND diagnostic_boot.scratchpad_seq={} '\
                'GROUP BY received_packets.source_address;'.format(since_when, expected_seq)

        self.cursor.execute(query)
        nodes = self.cursor.fetchall()
        self.database.commit()

        nodes = [x[0] for x in nodes]

        return nodes


    def _set_image_bootable(self):
        '''
        Sets the image in the sinks to bootable, thus taking those into use in sinks
        '''
        sink_commands = []
        for sink in self.sinks:
            sink_commands.append('({},"image_bootable")'.format(sink))

        query = 'INSERT INTO sink_command(address, command) VALUES {};'.format(','.join(sink_commands))
        self.cursor.execute(query)
        self.database.commit()
        self._wait_sinks_ready()

    def _send_otap_remote_update_request(self, new_scratchpad):
        '''
        Send the otap remote update request

        @param new_scratchpad:  ID for new scratchpad
        '''

        start_time = time.time()
        reboot_time = start_time + OtapUpdater.remote_update_into_use
        since_when = self._get_newest_remote_status_id()

        while time.time() < reboot_time:

            for sink in self.sinks:
                # New otap has update time as param
                pickled_object = pickle.dumps([OtapUpdater.remote_update_into_use, new_scratchpad])
                query = 'INSERT INTO sink_command(address, command, param) VALUES ({}, "otap_remote_update_request", %s);' \
                    .format(sink)
                self.cursor.execute(query, (pickled_object,))
                self.database.commit()

            reboot_time = time.time() + OtapUpdater.remote_update_into_use

            self._wait_sinks_ready()

            # Wait to query the results
            time.sleep(OtapUpdater.remote_status_interval)

            # Verify scratchpads that are marked as take into use (seconds_until_update>0)
            allfound, scratchpads = self._get_verify_scratchpads(since_when,
                                                                 new_scratchpad,
                                                                 require_update=True)
            if allfound:
                print 'All devices received the update request. Wait for devices to reboot'
                break

            print 'Not all the devices have yet received the update request. Resending the request'
            # Sending the query, just in case
            self._send_scratchpad_query()

        if time.time() < reboot_time:
            reboot_clock = datetime.datetime.now()+datetime.timedelta(seconds=reboot_time-time.time())
            print 'Boot shall happen in {}'.format(reboot_clock)
            time.sleep(reboot_time - time.time())

    def _download_scratchpads(self, otap_image, new_scratchpad):
        """
        Download scratchpad to the sinks

        @param otap_image: Path to the scratchpad file (.otap)
        @param new_scratchpad:  Id for the new scratchpad
        """
        handle = open(otap_image, 'rb')
        scratchpad = handle.read()
        handle.close()

        pickled_object = pickle.dumps([new_scratchpad, scratchpad])

        for sink in self.sinks:
            query = 'insert into sink_command (address, command, param) values ({},"load_otap_image",%s);' \
                .format(sink)
            self.cursor.execute(query, (pickled_object,))
            self.database.commit()

        self._wait_sinks_ready()

    def _get_current_scratchpad(self, since_when=None, greedy=True):
        """
        Evaluate currently used scratchpad in the network

        @param since_when:  If <>None, ID since when the scratchpad values are valid
        @param greedy:      True: Require all devices to have same scratchpad,
                            False: Just get the first scratchpad that <> 0

        @return:    Scratchpad id
        """
        start_time = time.time()
        if since_when is None:
            since_when = self._get_newest_remote_status_id()

        while True:
            self._send_scratchpad_query()
            scratchpads, isall = self._wait_for_all_scratchpads(since_when, greedy=greedy)
            currentscratchpad = None
            if len(scratchpads) > 0:
                currentscratchpad = max(scratchpads.values())

            if isall:
                print 'Current scratchpad is {}'.format(currentscratchpad)
                return currentscratchpad

            if (time.time()-start_time) > OtapUpdater.remote_status_max_time:
                print 'Unable to get status for current software version for all devices'
                proceed = query_yes_no('Are you absolutely sure you want to continue anyway?', default='no')
                self.database.ping(True)
                if not proceed:
                    sys.exit(0)
                else:
                    return currentscratchpad
            print 'Not got result for all nodes, resend the query'

    def _wait_for_all_scratchpads(self, since_when, processed_seq=False, greedy=True):
        """
        Wait for all the devices to get the same scratchpad.

        @param since_when: Id when first query for scratchpad has been sent
        @param processed_seq: True: Query processed_seq, False: Query otap_seq
        @param greedy:      True: Require all devices to have same scratchpad,
                            False: Just get the first scratchpad that <> 0

        @return: tuple with two values:
                    1) dictionary of scratchpads
                    2) true/false whether all devices have same
        """
        starttime = time.time()
        while True:
            time.sleep(10)
            allfound, scratchpads = self._get_verify_scratchpads(since_when,
                                                                 processed_seq=processed_seq,
                                                                 greedy=greedy)

            if allfound:
                return scratchpads, True

            if (time.time() - starttime) > OtapUpdater.remote_status_interval:
                return scratchpads, False

    def _get_verify_scratchpads(self,
                                since_when,
                                currentscratchpad=None,
                                processed_seq=False,
                                require_update=False,
                                greedy=True):
        '''
        Reads the scratchpads and verifies that every device has sent the response and that the scratchpad sequences
        are the same

        @param since_when:          Id since when reports are valid
        @param currentscratchpad:   If defined, what scratchpad sequence should be
        @param processed_seq:       True: Query processed_seq, False: Query otap_seq
        @param require_update:      True: Require seconds_until_update field >0 (image taken into use),
                                    false: don't care
        @param greedy:              True: Require all devices to have same scratchpad,
                                    False: Just get the first scratchpad that <> 0
        @return:  Tuple of two values:
                    True/false whether all devices have sent it and values are consistent
                    Dictionary of devices and scratchpad sequences
        '''
        scratchpads = self._get_scratchpads(since_when, processed_seq, require_update)

        if greedy:
            # Check that all have been received
            allfound = True
            for device in self.devices_to_otap:
                # If device is either sink or response returned, continue it
                if device in self.sinks:
                    continue
                if device in scratchpads:
                    continue
                else:
                    allfound = False
                    break

            # Do additional check that each device agrees on same value
            if allfound:
                for scratchpad in scratchpads.values():
                    # If scratchpad value is ok, it is ok
                    if scratchpad == 0:
                        continue
                    # If currentscratchpad not defined
                    if currentscratchpad is None:
                        currentscratchpad = scratchpad
                        continue

                    # If value differs from some other scratchpad value,
                    if scratchpad != currentscratchpad:
                        allfound = False
                        break
        else:
            allfound = False
            # If any scratchpad is returned (could be all 0), it is ok
            if len(scratchpads) > 0:
                allfound = True

        return allfound, scratchpads

    def _get_newest_remote_status_id(self):
        """
        Get the newest remote status id in the database. This is because the gateways and backend does
        not necessary have the clocks in sync, therefore it is not good idea to use clock as reference

        Returns: Id what is the newest item in the database
        """
        query = 'SELECT MAX(remote_status.id) FROM remote_status;'
        self.cursor.execute(query)
        max_val = self.cursor.fetchall()
        self.database.commit()

        max_val = max_val[0][0]

        if max_val is None:
            max_val = 0

        return max_val

    def _get_scratchpads(self, since_when, processed_seq=False, require_update=False):
        """
        Get scratchpad sequences in the devices

        @param since_when: ID since when the scratchpad ids are checked
        @param processed_seq: True: Query processed_seq, False: Query otap_seq
        @param require_update:      True: Require seconds_until_update field >0 (image taken into use),
                                    false: don't care
        @return: Dictionary of nodes and scratchpad sequences
        """

        if processed_seq:
            seq_column = 'processed_seq'
        else:
            seq_column = 'otap_seq'

        # Should we chack that scratchpad is marked to be taken into use?
        if require_update:
            additional_check = 'AND remote_status.seconds_until_update>0'
        else:
            additional_check = ''

        query = 'SELECT t.address, remote_status.{} '\
                'FROM '\
                '(SELECT remote_status.address,MAX(remote_status.id) maxid '\
                'FROM remote_status '\
                'WHERE remote_status.id>{} {} '\
                'GROUP BY remote_status.address) AS t '\
                'INNER JOIN remote_status '\
                'WHERE remote_status.id=t.maxid;'.format(seq_column, since_when, additional_check)

        self.cursor.execute(query)
        scratchpads = self.cursor.fetchall()
        self.database.commit()

        retval = {}
        for scratchpad in scratchpads:
            retval[scratchpad[0]] = scratchpad[1]

        if self.verbose:
            print 'Acquired scratchpads:{}'.format(retval)

        return retval

    def _send_scratchpad_query(self):
        '''
        Sends scratchpad query
        '''
        sink_commands = []
        for sink in self.sinks:
            sink_commands.append('({},"otap_remote_status")'.format(sink))

        query = 'INSERT INTO sink_command(address, command) VALUES {};'.format(','.join(sink_commands))
        self.cursor.execute(query)
        self.database.commit()

        self._wait_sinks_ready()

    def _stop_stacks(self):
        '''
        Stops the stacks in sinks
        '''

        sink_commands = []
        for sink in self.sinks:
            sink_commands.append('({},"stop_stack")'.format(sink))

        query = 'INSERT INTO sink_command(address, command) VALUES {};'.format(','.join(sink_commands))
        self.cursor.execute(query)
        self.database.commit()

        self._wait_sinks_ready()

    def _start_stacks(self):
        """
        Start the stacks in sinks
        """
        sink_commands = []
        for sink in self.sinks:
            sink_commands.append('({},"start_stack")'.format(sink))

        query = 'INSERT INTO sink_command(address, command) VALUES {};'.format(','.join(sink_commands))
        self.cursor.execute(query)
        self.database.commit()

        self._wait_sinks_ready()

    def _wait_sinks_ready(self):
        """
        Wait for sinks to be ready
        """
        while True:
            all_done = True
            for sink in self.sinks:
                query = 'SELECT COUNT(*) '\
                        'FROM sink_command '\
                        'WHERE sink_command.address={} '\
                        'AND sink_command.result IS NULL;'.format(sink)
                self.cursor.execute(query)
                result = self.cursor.fetchall()
                self.database.commit()

                if result[0][0] > 0:
                    all_done = False
                    break

            if all_done:
                return

            time.sleep(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # host_ip, port, username, password, db
    parser.add_argument('-i', '--ip',
                        default='localhost',
                        help='Address of the MySQL Server')
    parser.add_argument('-p', '--port',
                        default='3306',
                        help='Port number of the MySQL Server')
    parser.add_argument('-u', '--username',
                        default="pinocommtest",
                        help="User name to the MySQL Server")
    parser.add_argument('-w', '--password',
                        default="pinocommpassword",
                        help="Password to the MySQL Server")
    parser.add_argument('-d', '--database',
                        default="pinocommtest",
                        help="Database name in MySQL Server")
    parser.add_argument('-t',
                        '--interval',
                        default=60,
                        help='How often (in seconds) each node sends data')
    parser.add_argument('-f',
                        '--file',
                        required=True,
                        help='Otap filename (.otap) to be loaded')

    parser.add_argument('-v',
                        '--verbose',
                        help='Use verbose output',
                        action='store_true')
    parser.add_argument('-r', '--reboottime',
                        default=1200,
                        help='Time to reboot (USE WITH CARE!)')
    parser.add_argument('-n', '--network',
                        help='Use network OTAP to update incompatible firmware',
                        action='store_true'
                        )

    args = parser.parse_args()

    instance = OtapUpdater(args.ip,
                           args.port,
                           args.username,
                           args.password,
                           args.database,
                           args.interval,
                           args.verbose,
                           args.reboottime,
                           args.file,
                           args.network)
    instance.wait_for_devices_online()
    instance.verify_battery()
    instance.otap()



