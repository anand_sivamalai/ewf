
# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This is an example of enable beacon and send an eddystone beacon from sink.
Note: Always better to use eddystone or ibeacon, since there are scan applications for phone

It works as following:
. compose the data package(mode + interval + beacon payload)
. send the data package (endpoint 255->242) to the target node so the node will send the beacon

To customize the beacon, the interval and beacon URL are the elements usual changed by user

'''

import random
from wirepas.beacon.beacondata_command import *
from wirepas.meshapi_beacon_functions import *
from wirepas.meshapi import *

beacon = BeaconCommandMessage()

# set the mode as 1 to enable the sending
moderequest1 = beacon.generate_mode_command(value=1, value_format='uint8')

# Set the beacon send interval, unit is ms, the range is 100-60000
tx_interval = beacon.generate_frame_tx_interval(value=100, value_format='uint16')

# random address
address = random.randint(1, 0xffffffffffff)
print "Info**the address in beacon received by phone is {}".format(hex(address)[2:-1])

# beacon payload, with index 0
# Used two functions to compose the payload with the correct message type
beacongenerated_index0 = beacon.create_eddystone_url_beacon(index=0,measured_power=4,
                                                            device_address=address,
                                                            url="https://index_0.biz")
beaconrequest_index0 = beacon.generate_payload(beacongenerated_index0,value_format='string')

# Print the command, can copy this command and use it in wirepas terminal
one_command = binascii.hexlify( moderequest1+tx_interval+beaconrequest_index0)
print "Info**the command is going to sent to target device is {}".format(one_command)

# Find the sinks
network = MeshApiNetwork()
network.find_devices()
sinks = network.get_sinks()
# Be sure there is at least one sink
if len(sinks) == 0:
    print "Warning** Needs at least one sink!"
    exit(0)
else:
    for sink in sinks:
        retval = sink.get_stack_state()
        if retval != 0:
            try:
                print "Info** Sink {} is off, will start the stack".format(sink.get_address())
                sink.stack_start(autostart=True)
            except Exception as ex:
                print 'Warning** Sink {} stack is off and cannot be started,ex:{}'.format(sink.get_address(), ex)

        print "Info**Send the command to the target device now"
        sink.beacon_request_tx(data= moderequest1+tx_interval+beaconrequest_index0,
                               dest_address=sink.get_address(),
                               src_endpoint=BEACON_ENDPOINT_REQUEST_SRC,
                               dst_endpoint=BEACON_ENDPOINT_REQUEST_DST,
                               timeout = 60)


