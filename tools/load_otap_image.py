# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
A Python script to program local devices using OTAP primitives
provided by the Dual-MCU API.

'''

from wirepas.configuration import config

import sys
import binascii
import time
import argparse

from wirepas.otapimage import *

def progress_cb(percentage):
    if percentage % 10 == 0:
        print str(percentage)+'%'
    else:
        print ".",
        sys.stdout.flush()

def print_status_information(status_dict):
    print "Scratchpad length: "+str(status_dict['scratchpad_length'][0])
    print "Scratchpad CRC: "+hex(status_dict['crc'][0])
    print "Scratchpad sequence number: "+str(status_dict['otap_seq'][0])
    print "Scratchpad type: "+str(status_dict['scratchpad_type'][0])
    print "Scratchpad status: "+str(status_dict['scratchpad_status'][0])
    print "Processed scratchpad length: "+str(status_dict['processed_length'][0])
    print "Processed CRC: "+hex(status_dict['processed_crc'][0])
    print "Processed sequence number: "+str(status_dict['processed_seq'][0])
    print "FW memory area ID: "+str(status_dict['fw_mem_area_id'][0])
    print "FW major version: "+str(status_dict['fw_major_version'][0])
    print "FW minor version: "+str(status_dict['fw_minor_version'][0])
    print "FW maintenance version: "+str(status_dict['fw_maintenance_version'][0])
    print "FW development version: "+str(status_dict['fw_development_version'][0])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--configfile',
                        default = None,
                        help = 'Load a custom configuration .ini file')
    parser.add_argument('--otapfile',
                        default = None,
                        help = 'OTAP file')
    parser.add_argument('--sequence',
                        default = None,
                        help = 'OTAP sequence number')

    args = parser.parse_args()

    if args.otapfile is None:
        print "No OTAP file defined!"
        exit(1)

    filename = args.otapfile

    print "Loading file: "+filename
    image = OTAPImage()
    image.load_file(filename)
    print "File tag: "+binascii.hexlify(image.file_tag)
    print "Image length: "+str(image.length)
    print "CRC: "+hex(image.crc)
    print "Sequence number: "+str(image.sequence_number)
    print "Pad: "+str(image.pad)
    print "Type: "+str(image.image_type)
    print "Image start: "+binascii.hexlify(image.image[:32])

    if args.sequence is not None:
        sequence_number = int(args.sequence)
        print "Overriding file sequence number with command line: "+\
            str(sequence_number)
    else:
        sequence_number = image.sequence_number

    if args.configfile is not None:
        config.load_file(args.configfile)

    # Need to load the config before importing meshapi
    from wirepas.meshapi import *
    from wirepas.meshapimsap import *

    print "Searching for devices.."
    network = MeshApiNetwork()
    network.find_devices()

    print "Found devices: "+str(network.get_devices())

    for device in network.get_devices():
        print "********************************************************"
        print "Programming device: "+str(device)

        print "*** Device status before OTAP:"
        print_status_information(device.get_otap_image_status())

        print "Stopping stack.."
        device.stack_stop()

        print "*** Programming.."
        result = device.load_otap_image(image, otap_sequence = sequence_number,
                                        progress_callback = progress_cb,
                                        progress_step=1)

        if result == MsapScratchpadBlockResult.SuccessAllData:
            print "Image loaded succesfully!"
        else:
            print "ERROR: Image loading failed!"
            raise RuntimeError("Local OTAP failed")

        print "*** Device status after image load:"
        status = device.get_otap_image_status()
        print_status_information(status)

        error_string = ''

        # The header length is 32 bytes which needs to be added
        if status['scratchpad_length'][0] != image.length+32:
            error_string += 'ERROR: Invalid scratchpad length!\n'

        if status['crc'][0] != image.crc:
            print status['crc']
            print image.crc
            error_string += 'ERROR: Invalid CRC!\n'

        if status['otap_seq'][0] != sequence_number:
            error_string += 'ERROR: Invalid sequence number!\n'

        if status['scratchpad_type'][0] != 1:
            error_string += 'ERROR: Scratchpad type is not "present"!\n'

        if status['scratchpad_status'][0] != 255:
            error_string += 'ERROR: Scratchpad status is not "new"!\n'

        if error_string != '':
            print error_string
            raise RuntimeError("Local OTAP failed")

        print "Setting image bootable.."
        device.set_otap_image_bootable()

        # After set bootable it takes a moment for the device to recover
        time.sleep(5)

        print "Booting device.."
        device.stack_stop()

        print "Waiting for the device to boot.. (some errors may appear)"

        print "*** Device status after boot:"
        status = device.get_otap_image_status()
        print_status_information(status)

        error_string = ''

        # The header length is 32 bytes which needs to be added
        if status['processed_length'][0] != image.length+32:
            error_string += 'ERROR: Invalid scratchpad length!\n'

        if status['processed_crc'][0] != image.crc:
            print status['crc']
            print image.crc
            error_string += 'ERROR: Invalid CRC!\n'

        if status['processed_seq'][0] != sequence_number:
            error_string += 'ERROR: Invalid sequence number!\n'

        if status['scratchpad_type'][0] != 2:
            error_string += 'ERROR: Scratchpad type is not "processed"!\n'

        if status['scratchpad_status'][0] != 0:
            error_string += 'ERROR: Scratchpad status is not "success"!\n'

        if error_string != '':
            print error_string
            raise RuntimeError("Local OTAP failed")

        print "Booting device.."
        device.stack_stop()

        print "Device programmed succesfully!"

    network.kill()
