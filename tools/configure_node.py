# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''A generic node configuration script.
'''

from wirepas.meshapi import *
from wirepas.meshapicsap import *
from wirepas.nodeinformation import *
from wirepas.configuration import config

import sys
import getopt
import json
import binascii

def print_help_screen(node_info):
    '''Prints a help screen for command line.

    Args:
        node_info (NodeInformation): A NodeInformation instance to get the
            parameter list.
    '''

    print 'Usage: python configure_node.py [OPTIONS]'
    print 'Options:'
    print '\t-h\t\t\tHelp'
    print '\t-c <.json file>\t\tLoad a node configuration file'
    print '\t-j <JSON options>\tOptions in JSON format'
    print '\t-i <.ini file>\t\tAn optional configuration file for Python API'
    print '\t-x \t\t\tThe app config is a hex string'
    print 'Available parameters:'
    print '\n'.join(map(lambda x: '\t'+x, node_info.get_all_parameters().keys()
                        +node_info.get_parametrization_options()))
    print 'Example: python configure_node.py -c example_node_conf.json -j ' + \
        '"{\\"node_address\\": 23}"'
    exit(1)

if __name__ == "__main__":

    # Create a node information instance
    node_info = NodeInformation()

    # Unpack the command line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hxc:j:i:')
    except:
        print_help_screen(node_info)

    configuration_filename = None
    json_options = None

    # App config is not by default a hex string.
    app_config_hex = False

    # Go through the command line options.
    for opt, arg in opts:
        if opt == '-h':
            print_help_screen(node_info)
        if opt == '-c':
            configuration_filename = arg
        if opt == '-j':
            json_options = arg
        if opt == '-i':
            config.load_file(arg)
        if opt == '-x':
            app_config_hex = True

    # If configuration file is used load it
    if configuration_filename != None:
        print 'Loading device configuration from file "' + \
            configuration_filename + '"...'
        node_info.load_json_file(configuration_filename)

    # If JSON options are available then unpack them
    # Note: they will override all settings from the file
    if json_options != None:
        print 'Inserting JSON options from command line..'
        try:
            json_data = json.loads(json_options)
        except:
            print 'Could not parse JSON options'
            print 'JSON format: {"value name": value, ...}'
            exit(1)
        node_info.insert_json_data(json.loads(json_options))

    # Check what required configurations are missing and get them from user
    missing = node_info.get_missing()

    # Loop until all the missing are got
    # (if role is sink it needs more information)
    while missing != []:
        print "Still missing: "+', '.join(missing)
        for item in missing:
            value = raw_input('Please enter "'+item+'": ')
            setattr(node_info, item, value)
        missing = node_info.get_missing()

    print '*** Node configuration to write: '

    # Print out the configuration list
    print '\n'.join(map(lambda x: '\t'+x[0]+': '+str(x[1]),
                        node_info.get_all_parameters().items()))

    # Find the devices
    print 'Searching for devices..'
    network = MeshApiNetwork()
    network.find_devices()
    devices = network.get_devices()
    print 'Found devices: '+str(devices)
    if len(devices) == 0:
        print 'No devices found!'
        network.kill()
        exit(1)
    elif len(devices) > 1:
        # If there are more than one node then check that they all
        # can be configured.
        if not node_info.autoprogram_all_devices:
            print 'More than one devices found. Program all? (y/n)'
            if sys.stdin.read(1) != 'y':
                print 'Please remove additional devices and retry'
                network.kill()
                exit(1)

    # Stop the stack for all devices
    network.stack_stop()

    # Reset if configured so
    if node_info.factory_reset:
        print 'Doing a factory reset on all devices..'
        for device in devices:
            device.factory_reset()

    print 'Setting parameters..'

    # Set required attributes
    network.set_network_address(int(node_info.network_address))
    network.set_network_channel(int(node_info.network_channel))
    for device in devices:
        device.set_role(int(node_info.role))
        device.set_address(int(node_info.node_address))

    # Convert hex values to binary strings if possible
    node_info.convert_values_to_binary(app_config_hex)

    # If node is sink then set also app configuration data
    if int(node_info.role) & 0xf == CsapNodeRole.Sink:
        for device in devices:
            device.set_app_config_data(int(node_info.app_config_sequence),
                                       int(node_info.diagnostic_interval),
                                       str(node_info.app_config_data))

    # If node has both cipher and authentication keys then set them both
    if node_info.cipher_key is not None and \
       node_info.authentication_key is not None:
        for device in devices:
            device.set_cipher_key(node_info.cipher_key)
            device.set_authentication_key(node_info.authentication_key)

    # If there is a channel map then set it.
    if node_info.channel_map is not None and node_info.channel_map != '':
        device.set_channel_map(int(node_info.channel_map))

    # If there are both access cycle ranges then set them.
    if node_info.access_cycle_range_min is not None and \
       node_info.access_cycle_range_max is not None and \
       node_info.access_cycle_range_min != '' and \
       node_info.access_cycle_range_max != '':

        device.set_access_cycle_range(int(node_info.access_cycle_range_min),
                                      int(node_info.access_cycle_range_max))
        
    elif node_info.access_cycle_range_min or \
    node_info.access_cycle_range_max:
        print "Only one access cycle range value found! Please define both."
        exit(1)

    # If configured so then start stacks
    if node_info.start_stack:
        print 'Starting stacks..'
        network.stack_start(autostart=True)

    # Kill the interfaces
    print 'Disconnecting devices..'
    network.kill()

    print 'Done.'
