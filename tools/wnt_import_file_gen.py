# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#


import argparse
import MySQLdb

class NmsImportFileGen(object):
    '''
    This module creates import file for NDT client
    '''

    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude
        # Nodes shall be array with dictionaries with following keys: 'network_id','node_id'
        self.nodes = []

    def open_nodes_from_mysql(self, host_ip, port, username, password, db):
        database = MySQLdb.connect(host=host_ip,
                                   user=username,
                                   passwd=password,
                                   db=db,
                                   port=int(port))
        database.autocommit(on=True)

        cursor = database.cursor()
        query = 'SELECT DISTINCT known_nodes.network_address,known_nodes.node_address '\
                'FROM known_nodes;'
        cursor.execute(query)

        values = cursor.fetchall()

        for value in values:
            node = {}
            node['network_id'] = int(value[0])
            node['node_id'] = int(value[1])

            self.nodes.append(node)

    def gen_file(self, path):
        '''
        Generate import file

        Args:
            path: Path for import file

        '''

        f = open(path, 'w')
        # Add prefix
        f.write('<?xml version="1.0"?>\n'\
                '<config version="1.0">\n'\
                '<nodes>\n')
        # Add values for each file
        for node in self.nodes:
            nodestr = '<node networkid="{}" nodeid="{}" latitude="{}" longitude="{}" altitude="100" approved="true" />\n'.\
                format(node['network_id'], node['node_id'], self.latitude, self.longitude)
            f.write(nodestr)

        # Add postfix
        f.write('</nodes>\n'\
                '</config>')

        f.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # MySql definition
    parser.add_argument('--mysql',
                        action='store_true',
                        help='Get nodes from MySql database')
    # host_ip, port, username, password, db
    parser.add_argument('-i', '--mysqlip',
                        default='localhost',
                        help='Address of the MySQL Server')
    parser.add_argument('-p', '--mysqlport',
                        default='3306',
                        help='Port number of the MySQL Server')
    parser.add_argument('-u', '--mysqlusername',
                        default="pinocommtest",
                        help="User name to the MySQL Server")
    parser.add_argument('-w', '--mysqlpassword',
                        default="pinocommpassword",
                        help="Password to the MySQL Server")
    parser.add_argument('-d', '--mysqldatabase',
                        default="pinocommtest",
                        help="Database name in MySQL Server")

    parser.add_argument('--latitude',
                        default='43.6387',
                        help='Latitude where nodes shall be put to')
    parser.add_argument('--longitude',
                        default='116.24135',
                        help='Longitude where nodes shall be put to')

    # Add file
    parser.add_argument('importfile',
                        help='Generated import file')

    args = parser.parse_args()

    instance = NmsImportFileGen(args.latitude, args.longitude)
    if args.mysql:
        instance.open_nodes_from_mysql(args.mysqlip,
                                       args.mysqlport,
                                       args.mysqlusername,
                                       args.mysqlpassword,
                                       args.mysqldatabase)

    instance.gen_file(args.importfile)
