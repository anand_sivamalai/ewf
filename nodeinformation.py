# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''A class for storing and loading node configuration.
'''

import json
from meshapicsap import *
import binascii

class NodeInformation():
    '''
    Node information class for node configuration storage and loading
    '''

    # Required parameteres for all nodes
    network_address = None
    network_channel = None
    role = None
    node_address = None

    # List of the required parameter names
    required = ['network_address', 'network_channel', 'role', 'node_address']

    # Required extra parameters for a sink
    app_config_sequence = None
    diagnostic_interval = None
    app_config_data = None

    # List of the required parameter names for a sink
    required_by_sink = ['app_config_sequence', 'diagnostic_interval',
                        'app_config_data']

    # Optional parameters
    cipher_key = None
    authentication_key = None
    channel_map = None
    access_cycle_range_min = None
    access_cycle_range_max = None

    # Optional parameter names list
    optional_information = ['cipher_key', 'authentication_key', 'channel_map',
                            'access_cycle_range_min', 'access_cycle_range_max']

    # Parametrization settings
    factory_reset = True             # If true then do factory reset before
                                     # parametrization

    start_stack = True               # If true then start stack after
                                     # parametrization

    autoprogram_all_devices = False  # If true then configurate all the devices
                                     # even if there are more than one
    skip_optional = False            # If true then skip optional parameters

    parametrization_options = ['factory_reset', 'start_stack',
                               'autoprogram_all_devices', 'skip_optional']

    def get_missing(self):
        '''Get a list of missing parameter names
        '''

        # Check the required list
        missing = []
        for item in self.required:
            if getattr(self, item) == None:
                missing.append(item)

        # If node is sink add also the parameters required by the sink
        if self.role != None:
            if int(self.role) & 0xf == CsapNodeRole.Sink:
                for item in self.required_by_sink:
                    if getattr(self, item) == None:
                        missing.append(item)

        # If skip_optional is false then add optional parameters also
        if not self.skip_optional:
            for item in self.optional_information:
                if getattr(self, item) == None:
                    missing.append(item)

        return missing

    def get_all_parameters(self):
        '''Get a list of all possible parameters for a node
        '''

        all_items = {}

        for item in self.required + self.required_by_sink + self.optional_information:
            all_items[item] = getattr(self, item)

        return all_items

    def get_parametrization_options(self):
        '''
        Get a list of parametrization options
        '''
        return self.parametrization_options

    def load_json_file(self, filename):
        '''
        Load configuration from a JSON file

        Args:
            filename (str): A file name for the JSON configuration file
        '''

        with open(filename, 'r') as file:
            json_data = json.load(file)
        self.insert_json_data(json_data)

    def insert_json_data(self, json_data):
        '''Insert configuration options from a JSON data string

        Args:
            json_data (str): A JSON string containing a dictionary of
                option values
        '''

        for key in json_data:
            if hasattr(self, key):
                setattr(self, key, json_data[key])
            else:
                print 'Skipping an unknown datafield in JSON '+\
                    'configuration file: "'+key+'"'

    def convert_values_to_binary(self, app_config_hex=False):
        '''Convert cipher and authentication keys from hex strings to binary

        Args:
           app_config_hex (bool): True if the stored app config is in hex string
               format. False if app config is a literal string.
        '''

        # In case the keys are empty strings set them to NoneTypes
        if self.cipher_key == "":
            self.cipher_key = None
        if self.authentication_key == "":
            self.authentication_key = None

        # Convert from a hex string
        if self.cipher_key != None:
            if self.cipher_key[:2] == '0x':
                self.cipher_key = self.cipher_key[2:]
            self.cipher_key = binascii.unhexlify(self.cipher_key)

        if self.authentication_key != None:
            if self.authentication_key[:2] == '0x':
                self.authentication_key = self.authentication_key[2:]
            self.authentication_key = \
                binascii.unhexlify(self.authentication_key)

        # If the app config data is in hex format then convert it to string.
        if app_config_hex:
            try:
                self.app_config_data = binascii.unhexlify(self.app_config_data)
            except TypeError:
                print "ERROR: App config set to be a hex string but " + \
                    "can not parse!"
                exit(1)
