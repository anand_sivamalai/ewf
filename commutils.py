# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

Communication interface helper functions.

Created on 17.09.2015

'''

from struct import pack, unpack
from random import randint

# Try to import all interfaces. Just pass if they're not available.
try:
    import serialinterface
except ImportError:
    pass

try:
    import spi_interface
except ImportError:
    pass

try:
    import serial_tcp_ip_interface
except ImportError:
    pass

try:
    import ipv6br_udp_interface
except ImportError:
    pass

try:
    import serial_streaming_interface
except ImportError:
    pass

import binascii

from configuration import config

import debug

def uint8_to_int(ba):
    '''
    A helper function to convert the first byte of a bytearray to int.

    Parameters:
    - ba, bytearray to be converted

    Returns: the byte as integer
    '''

    if len(ba) != 1:
        raise RuntimeError("Invalid length string: "+binascii.hexlify(ba))

    return unpack('<B',ba[0])[0]

def uint16_to_int(ba):
    '''
    A helper function to convert the first 2 bytes of a bytearray to int.

    Parameters:
    - ba, bytearray to be converted

    Returns: the 16-bit word as integer
    '''

    if len(ba) != 2:
        raise RuntimeError("Invalid length string: "+binascii.hexlify(ba))

    return unpack('<H',ba[0:2])[0]

def uint32_to_int(ba):
    '''
    A helper function to convert the first 4 bytes of a bytearray to int.

    Parameters:
    - ba, bytearray to be converted

    Returns: the 32-bit word as integer
    '''

    if len(ba) != 4:
        raise RuntimeError("Invalid length string: "+binascii.hexlify(ba))

    return unpack('<I',ba[0:4])[0]

def uint24_to_int(ba):
    '''
    A helper function to convert the first 3 bytes of a bytearray to int.

    Parameters:
    - ba, bytearray to be converted

    Returns: the 24-bit word as integer
    '''

    if len(ba) != 3:
        raise RuntimeError("Invalid length string: "+binascii.hexlify(ba))

    bytes = unpack('<BBB', ba[0:3])
    return bytes[0] + (bytes[1]<<8) + (bytes[2]<<16)

def int_to_uint8(value):
    '''
    A helper function to convert int to uint8 string.

    Parameters:
    - value, int to be converted

    Returns: Packed string.
    '''

    return pack('<B', value)

def int_to_uint16(value):
    '''
    A helper function to convert int to uint16 string.

    Parameters:
    - value, int to be converted

    Returns: Packed string.
    '''

    return pack('<H', value)

def int_to_uint32(value):
    '''
    A helper function to convert int to uint32 string.

    Parameters:
    - value, int to be converted

    Returns: Packed string.
    '''

    return pack('<I', value)

def int_to_uint24(value):
    '''
    A helper function to convert int to uint24 string.

    Parameters:
    - value, int to be converted

    Returns: Packed string.
    '''

    if value < 0 or value >= 2**24:
        raise RuntimeError("Invalid value: " + str(value))

    return pack('<BBB', value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff)

def random_network_address():
    '''
    A function to generate a random network address.

    Returns: A random network address.
    '''

    while True:
        address = randint(1, 0xfffffe)
        if ((address >> 16) & 0xff) == 0x55:
            continue
        if ((address >> 8) & 0xff) == 0x55:
            continue
        if ((address >> 0) & 0xff) == 0x55:
            continue
        if ((address >> 16) & 0xff) == 0xaa:
            continue
        if ((address >> 8) & 0xff) == 0xaa:
            continue
        if ((address >> 0) & 0xff) == 0xaa:
            continue
        break

    return address
