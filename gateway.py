# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
Wirepas Python Gateway

Implements a Python gateway that can be extended by plugins.

Commandline parameters
----------------------

- -h, --help: Display help.
- --configfile CONFIGFILE: Load a custom configuration .ini file.

Plug-ins
--------

The datapacket and backend plug-ins are automatically scanned.

The datapacket plug-ins need to be in modules named datapacket_*.py and
inherited from the *wirepas.meshapidsap.DsapDataRX* class or any class
inherited from that.

The backend plug-ins need to be in modules named backend_*.py and inherited
from the *wirepas.backendgateway.BackendGateway* class or any class
inherited from that.

The plug-ins will be scanned from different places definied by three conditions:

1) The *gw_plugins* directory under the wirepas module directory.
2) The directories in the *plugin_dirs* list from the gateway .ini-file(s).
3) If the *wirepas_plugins* Python module is installed then from its
    installation directory.

Classes
-------
'''

import argparse
import sys
import time
import signal
import ConfigParser
import ast
import os
import importlib
import imp
import inspect
import Queue
import datetime

from configuration import config
import gatewaybackend

from meshapicsap import CsapNodeRole

def break_handler(signum, frame):
    '''
    A SIGINT handler so we can shutdown the script
    '''
    raise RuntimeError('Exit Program')

class CommAdapter(object):
    '''A class for the gateway instance that will be run.

    Args:
        configfile (str): The name of the configuration .ini-file for gateway
            specific configuration.
    '''
    def __init__(self, configfile = None):
        ''''''
        # Config parser instance
        self.configparser = ConfigParser.ConfigParser()

        if configfile:
            self.load_configuration(configfile)
        else:
            self.load_configuration()

        # Try to get the logfile name from the config file.
        # Skip log file if no configuration.
        try:
            logfile_name = ast.literal_eval(self.configparser.get('general',
                                                                  'logfile'))
        except Exception as ex:
            logfile_name = None

        # Try to open the log file.
        if logfile_name is not None:
            try:
                self.logfile = open(logfile_name, "a+")
            except Exception as e:
                print "Log file opening failed: " + str(e)
                exit(1)

            self.msg("Gateway logging to file: " + logfile_name)
        else:
            self.logfile = None

        global config

        if self.get_python_api_config_file():
            config.load_file(self.get_python_api_config_file())

        # NOTE: The meshapi is imported after the Python API config file!
        import meshapi

        # Load the plugins
        self.load_datapacket_plugins()
        self.load_backend_plugins()

        # Load the backend definitions
        self.backend_definitions = self.get_backend_definitions()

        # Force settings
        config.framecodec_skip_errors = True

        # Create the command queue.
        self.command_queue = Queue.Queue()

        self.known_sinks = []

        self.backend_instances = {}
        self.datapacket_instances = {}
        self.gateway_threads = {}

        self.network = meshapi.MeshApiNetwork()

    def setup_backends(self, sinks):
        '''
        Creates the backend instances for a list of sinks

        Attributes:

        - sinks: The list of sinks that have been scanned.
        '''

        # Remove the sinks that have disappeared
        removed_sinks = filter(lambda x: x not in sinks, self.known_sinks)

        # Add unconnected sinks to list
        for sink in self.known_sinks:
            if not sink.is_connection_alive():
                removed_sinks.append(sink)

        self.known_sinks = filter(lambda x: x not in removed_sinks,
                                  self.known_sinks)

        for sink in removed_sinks:
            for backend_instance in self.backend_instances[sink]:
                backend_instance.kill()

        # Get the new sinks..
        new_sinks = filter(lambda x: x not in self.known_sinks, sinks)

        # If no new sinks then we just can return.
        if len(new_sinks) == 0:
            return

        self.msg("Found new sinks in connections: " +
                 ', '.join(map(lambda x: str(x.comminterface),new_sinks)))

        self.msg("Starting the stack for the new sinks..")
        print("Starting the stack for new sink!")
        for sink in new_sinks:
            sink.stack_start(autostart=True)

        print("Sink Started")

        # Append the new sinks to the known sinks
        self.known_sinks += new_sinks

        self._setup_new_sinks(new_sinks)

    def _setup_new_sinks(self, new_sinks):
        '''Helper function to set up new sinks

        Args:
            new_sinks (list): New sinks to be handled
        '''
        backends = {}
        debug_print_callback_list = []

        # Find the non-legacy backend types..
        for backend_id in self.backend_definitions:
            if not self.backend_definitions[backend_id]['type'].endswith("-v0"):
                backends[backend_id] = self.backend_definitions[backend_id]

        # Construct a dictionary of the available backend classes
        backend_classes = {}
        for backend_class in self.backend_classes:
            backend_classes[backend_class.get_name()] = backend_class

        

        # Also create the gateway threads if available
        for backend_id in backends:
            type = backends[backend_id]['type']
            backend_class = backend_classes[type]
            if not backend_id in self.gateway_threads:
                gw_thread = backend_class.get_gateway_thread(backends[backend_id])
                if gw_thread != None:
                    self.gateway_threads[backend_id] = gw_thread

   

        # Construct a dictionary of the available datapacket classes
        datapacket_classes = {}
        for datapacket_class in self.datapacket_classes:
            datapacket_classes[datapacket_class.get_name()] = datapacket_class

        # Add all the backends for all newly found sinks.
        for sink in new_sinks:
            backend_instances = []
            datapacket_instances = []

            # Create the backend instances for this sink
            for backend_id in backends:
                type = backends[backend_id]['type']
                backend_class = backend_classes[type]

                if backend_id in self.gateway_threads:
                    gw_thread = self.gateway_threads[backend_id]
                else:
                    gw_thread = None

                backend_instance = backend_class(backends[backend_id],
                                                 sink, gw_thread)

                # Create debug print callbacks in case the backend supports it
                if hasattr(backend_instance, 'get_debug_print_callback'):
                    debug_print_callback_list.append(backend_instance.\
                                                     get_debug_print_callback())

                backend_instances.append(backend_instance)
                datapacket_defs = backends[backend_id]['datapackets']

                # Create the datapacket instances for this
                # sink/backend combination
                for datapacket_type in datapacket_defs:
                    dest_endpoint = backends[backend_id]\
                                    ['datapackets'][datapacket_type]
                    datapacket_class = datapacket_classes[datapacket_type]
                    datapacket_instance = \
                            datapacket_class(dest_endpoint=dest_endpoint)
                    datapacket_instances.append(datapacket_instance)
                    callback = backend_instance.indication_callback_gen()
                    sink.register_indication_callback(datapacket_instance,
                                                      callback)
                # Set-up the backend queues and functions to work with
                # this gateway.
                self._setup_backend_functions(backend_instance)

            # In case we have debug print supports then register them
            if debug_print_callback_list:
                sink.comminterface.\
                    register_debug_handler(self.\
                                           create_debug_print_callback(
                                               debug_print_callback_list))

            self.datapacket_instances[sink] = datapacket_instances
            self.backend_instances[sink] = backend_instances

    def _setup_backend_functions(self, backend_instance):
        '''Set-up the backend queues and functions to work with this gateway.

        Args:
            backend_instance (GatewayBackend): The backend instance.
        '''

        # Set the gateway command queue if the functionality is
        # available.
        if hasattr(backend_instance, 'set_gw_command_queue'):
            backend_instance.set_gw_command_queue(self.command_queue)

        # Set the logger function if the gateway has the ability.
        if hasattr(backend_instance, 'set_logger'):
            backend_instance.set_logger(lambda text:
                                        self.logger(backend_instance,
                                                    time.time(),
                                                    text))

    def create_debug_print_callback(self, callback_list):
        '''
        Create a callback to call all the listed backend debug print callbacks
        for one sink.

        Args:
            callback_list (list): A list of callback functions of type
                cb(debug_text) where debug_test is a string.
        '''

        def debug_cb(text):
            for cb in callback_list:
                cb(text)
        return debug_cb

    def load_configuration(self, filename = None):
        '''Load the gateway configuration from defaults file and an optional
        extra configuration file.

        Args:
            filename (str): An optional .ini file's filename.
        '''

        default_config_file = os.path.join(os.path.dirname(__file__),
                                           'gw_defaults.ini')

        if not os.path.isfile(default_config_file):
            self.msg('Default config file "' + default_config_file +
                '" does not exist! Aborting..')
            exit(1)

        self.configparser.read(default_config_file)

        if filename != None:
            print 'Loading custom gateway config file {}'.format(filename)
            if not os.path.isfile(filename):
                print 'Custom config file "' + filename + \
                    '" does not exist! Aborting..'
                exit(1)
            self.configparser.read(filename)

    def logger(self, plugin, timestamp, text):
        '''Log into a file.

        Args:
            plugin (object): GatewayBackend object or None.
            timestamp (float): Timestamp in time since epoch.
            text (str): The logged message.
        '''

        if self.logfile is not None:
            date_str = datetime.datetime.fromtimestamp(timestamp).\
                       strftime('%d/%m/%Y %H:%M:%S')

            if plugin is not None:
                self.logfile.write(str(date_str) + '\t' +
                                   str(plugin) + '\t' +
                                   str(text) + '\n')
            else:
                self.logfile.write(str(date_str) + '\t' +
                                   'Main Thread '+ '\t' +
                                   str(text) + '\n')
            self.logfile.flush()

    def msg(self, text):
        '''Print a message to stdout and logfile (if available).

        Args:
            text (str): The message.
        '''

        print text
        self.logger(None, time.time(), text)

    def get_backend_definitions(self):
        '''
        Get the backend definitions from the loaded .ini files.
        '''

        backends_list = ast.literal_eval(self.configparser.get('general',
                                                               'backends'))
        if not backends_list:
            self.msg("ERROR: No backend connections defined. " +
                     "Please add connections to the 'backends' list in " +
                     "the configuration file.")
            exit(1)

        backends_dict = {}

        for name in backends_list:
            backend = {}
            section = 'backend:'+name
            option_names = self.configparser.options(section)

            for option_name in option_names:
                backend[option_name] = ast.literal_eval(self.\
                                                        configparser.\
                                                        get(section,
                                                            option_name))

            backends_dict[name] = backend

        return backends_dict

    def get_python_api_config_file(self):
        '''Get the Python API configuration file name from
        the gateway configuration.
        '''

        if self.configparser.has_option('general', 'python_api_config_file'):
            conf_value = self.configparser.get('general',
                                               'python_api_config_file')
            return ast.literal_eval(conf_value)
        else:
            return None

    def load_backend_plugins(self):
        '''
        Load all the available backend plugins.
        '''

        self.msg("Searching for backend plugins..")

        backendmodules = self._find_plugins('backend')
        self.backend_classes = []

        for backendmodule in backendmodules:
            self.backend_classes += \
                filter(lambda x: issubclass(x, backendmodule.GatewayBackend) \
                       and x != backendmodule.GatewayBackend, \
                       map(lambda x: x[1], inspect.getmembers(backendmodule,
                                                              inspect.isclass)))

        self.msg("Found backend plugins: "+str(map(lambda x: x.get_name(),
                                                   self.backend_classes)))

    def load_datapacket_plugins(self):
        '''
        Load all the available datapacket plugins.
        '''

        self.msg("Searching for datapacket plugins..")

        datapacketmodules = self._find_plugins('datapacket')

        self.datapacket_classes = []

        for datapacketmodule in datapacketmodules:
            self.datapacket_classes += \
                filter(lambda x: issubclass(x, datapacketmodule.DsapDataRx)
                       and x != datapacketmodule.DsapDataRx,
                       map(lambda x: x[1], inspect.getmembers(datapacketmodule,
                                                              inspect.isclass)))

        self.msg("Found datapacket plugins: " +
                 str(map(lambda x: x.get_name(), self.datapacket_classes)))

    def _find_plugins(self, type):

        directory_list = [os.path.join(os.path.dirname(__file__), 'gw_plugins')]
        directory_list += ast.literal_eval(self.configparser.get('general',
                                                                 'plugin_dirs'))

        # Test if wirepas_plugins was installed and include it..
        try:
            import wirepas_plugins
            directory_list.append(os.path.dirname(wirepas_plugins.__file__))
        except:
            self.msg("No wirepas_plugins module installed. " +
                     "Continuing without it..")

        filelist = []

        for directory in directory_list:
            try:
                self.msg("Scanning " + type +
                         " plugins in directory: " + directory)
                filenames = os.listdir(directory)
                filelist += map(lambda x: (x, directory+'/'+x),
                                os.listdir(directory))

            except OSError:
                self.msg("Couldn't open the directory. Skipping.")

        pluginlist = filter(lambda x: x[0].startswith(type+"_") and
                            x[0].endswith(".py"), filelist)
        pluginlist = map(lambda x: (x[0].split('.py')[0], x[1]), pluginlist)

        pluginmodules = []

        for pluginfile in pluginlist:
            modulename = pluginfile[0]
            filename = pluginfile[1]
            try:
                pluginmodules.append(imp.load_source(modulename, filename))
            except Exception as e:
                self.msg("Could not load module '" + modulename +
                         "'. Reason: " + str(e))

        return pluginmodules

    def kill_gateway_threads(self):
        '''
        Kill all gateway threads and empty the dictionary.
        '''

        self.msg('Killing the gateway threads')
        for id in self.gateway_threads:
            self.gateway_threads[id].kill()
        self.gateway_threads = {}

    def kill_backend_connections(self):
        '''
        Kill all backend connections nicely.
        '''

        self.msg('Killing the backend connections')
        for sink in self.backend_instances:
            for backend_instance in self.backend_instances[sink]:
                backend_instance.kill()

    def kill_all_interfaces(self):
        '''
        Kills all backend connection, gateway threads and CommInterfaces.
        '''

        # Kill everything.
        self.kill_backend_connections()
        self.kill_gateway_threads()
        self.network.kill()

    def handle_command(self, command_item):
        '''
        Handles a gateway command coming from a backend plug-in.

        Supported command keywords:

        - 'restart': Closes all interfaces and reloads them in next device scan.
        - 'kill': Kills the gateway.

        Args:
            command_item (dict): Dictionary with two items:
                                 'backend': Contains the backend instance
                                            where this command is coming from
                                 'command': Contains the command keyword.

        '''

        command = command_item['command']
        backend = command_item['backend']

        self.msg("GW command '%s' from backend %s" % (command, backend))

        if command == 'restart':
            self.msg('Killing all interfaces for a restart..')
            self.kill_all_interfaces()
        elif command == 'kill':
            self.msg('Shutdown command from plug-in..')
            raise RuntimeError('Exit Program')
        else:
            self.msg("Unknown command: %s" % (command))

    def exec_loop(self):
        '''
        The main loop for the gateway. Run until a break signal.
        '''
        signal.signal(signal.SIGINT, break_handler)

        try:
            while True:

                try:
                    self.network.find_devices()
                except Exception, ex:
                    self.msg('Find devices failed:{}'.format(ex))

                sinks = []

                for device in self.network.get_devices():
                    try:
                        #networkch = device.get_network_channel()
                        #print("network channel = " + str(networkch))
                        
                        #device.stack_stop()
                        #device.set_network_channel(2)
                       
                        device.stack_start(autostart=True)
                        if (device.get_role() & CsapNodeRole.BASEROLEMASK) \
                           == CsapNodeRole.Sink:
                            sinks.append(device)
                          
                            #self.msg('Found a SINK!\r\n')
                    except Exception as e:
                        self.msg("Error when querying role for device: " +
                            str(device) +  " " +str(e))
                        device.kill()
                

 
                # Set up the plug-ins.
                self.setup_backends(sinks)

                


                # Sleep for ten seconds before trying to find new devices.
                time.sleep(10)

                while True:
                    try:
                        self.handle_command(self.command_queue.get(timeout=0))
                    except Queue.Empty:
                        break

        except Exception as ex:
            self.msg(str(ex))

            self.kill_all_interfaces()
            self.msg('Exiting')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--configfile',
                        default=None,
                        help='Load a custom configuration .ini file')

    args = parser.parse_args()
    instance = CommAdapter(args.configfile)
    instance.exec_loop()
