# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from exceptions import NotImplementedError

# List of known definitions, to prevent multiple instances from same definition/device
known_definitions_devices = set()

class DiagnosticsDefinition(object):
    '''
    Abstract base class for diagnostics definition.
    '''
    def __init__(self, measurements=[]):
        self.measurements = measurements

    def GetHandler(self, device):
        '''
        This method must be overridden in derived classes

        Args:
            device: to which device we return the handler

        Returns:
            DiagnosticsHandler instance
        '''
        raise NotImplementedError("GetHandler not implemented!")

    def _device_exists_already(self, device):
        '''
        Check if this device/definition already exists

        Args:
            device: Device to be associated

        Returns:
            true: already exists
            false: not exists

        '''

        global known_definitions_devices
        return (self, device) in known_definitions_devices

    def _add_device(self, device):
        '''
        Add device/definition to list
        Args:
            device: Device to be associated
        '''

        global known_definitions_devices
        known_definitions_devices.add((self, device))



