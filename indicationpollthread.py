# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

Indication Poll Thread class for CommInterface

Created on 20.12.2016

'''
from comminterface_common import *

from meshapimsap import MsapIndicationPoll

from threading import Thread
from Queue import Queue,Empty
import time
from configuration import config

class IndicationPollThread(Thread):
    '''
    A thread class to poll indication from a device. Each of these needs a dedicated communication 
    interface. The interface starts the thread.
    '''


    def __init__(self, interface, interval=None):
        '''
        Instantiates and starts an indication poll thread which polls the
        indications

        Arguments:
            - interface: Interface used
            - interval: Poll interval in seconds. None if read from config files
        '''
        super(IndicationPollThread, self).__init__(name='IndPollThread '+str(interface))
        self.interface = interface
        self.active = False
        # Succeeded indication polls
        self.ok_count = 0
        # Failed indication polls
        self.fail_count = 0

        if interval is None:
            self.interval = config.serial_indication_poll_interval
        else:
            self.interval = interval

    def run(self):
        '''
        The run method.
        '''
        
        self.interface.debug_info("Indication poll thread started for interface "+str(self.interface))

        # Run while the thread is set as active.
        while self.active:
            # Create an indication poll and send it to the parent interface.
            poll = MsapIndicationPoll()
            try:
                # It is VERY important to keep data flown as much as possible. Therefore, there is None
                # timeout here. We don't know in advance how much data there is so it is not possible
                # to determine the timeout when all the data has been written
                result = self.interface.send_request(poll, timeout=None)
                
            except Exception as e:
                # Check if the interface is actually closed or killed.
                if (not self.interface.connected) or (not self.interface.alive):
                    break
                
                self.fail_count += 1
                result = None

            if result != None:
                self.ok_count += 1

            # Sleep for a while continue polling. Even after some problem we pause here for a while
            time.sleep(self.interval)

        self.interface.debug_info("Closing Indication poll thread for interface "+str(self.interface))

        # Make sure this thread is marked as inactive.
        # An error might have caused closing.
        self.active = False
        
    def stop(self):
        '''
        Stop the indication poll thread.
        '''
        # The thread main loop checks this flag.
        if self.active:
            self.active = False

            # Wait until stopped.
            self.join()

    def start(self):
        '''
        Star the indication poll thread.
        '''

        # Set the loop active.
        if not self.active:
            self.active = True
            super(IndicationPollThread, self).start()

    def announce_more_indications(self, is_more):
        '''
        Tell that all pending indications have been received
        '''
        self.pending_indications_done.put(is_more)
