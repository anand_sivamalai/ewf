# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
MeshApiDeviceNetwork class for collecting MeshApiDevices together

Created on 20.12.2016

'''

from commutils import *
from meshapicsap import *
from meshapimsap import *
from meshapidsap import *
import datetime
import random
from struct import *
from Queue import Queue
from configuration import config
import time
from beacon.beacondata_command import *
from meshapidevice import MeshApiDevice
from meshapi_common import *

class MeshApiNetwork(object):
    '''
    A class for collecting MeshApiDevices into a network instance.

    Args:
        devices_param (list): An optional list of MeshApiDevices to create the
            network from.
    '''

    # A list of devices in this network
    devices = []

    def __init__(self, devices_param=[]):
        '''
        Initializes a MeshApiNetwork instance.

        Attributes:

        - devices_param: A list of MeshApiDevice instances.

        '''

        self.devices = devices_param[:]

    '''
    Basic functions
    '''

    def get_devices(self):
        '''
        Get devices.

        Returns: A list of devices
        '''

        return list(self.devices)

    def connect_all(self):
        '''
        Connects all devices in the network via their communication interface.
        '''

        for device in self.devices:
            try:
                device.open_connection()
            except:
                # Connection opening failed. Remove from devices.

                # Try to kill it first.
                try:
                    device.kill()
                except:
                    pass

                self.remove_device(device)

    def kill(self):
        '''
        Kills all devices in the network. Kills all associated threads.
        '''

        for device in self.devices:
            device.kill()

        self.devices = []

    def get_headnodes(self):
        '''
        Get the headnodes from the network.

        Returns: a list of headnodes as MeshApiDevice instances
        '''

        nodes = []
        for device in self.devices:
            try:
                if (device.get_role() & CsapNodeRole.BASEROLEMASK) == \
                   CsapNodeRole.Headnode:
                    nodes.append(device)
            except MeshApiDeviceException as e:
                # The role may not be set. Then just skip it.
                if e.confirmation_result == \
                   CsapAttributeResult.FailureInvalidAttributeValue:
                    continue
                else:
                    raise e

        return nodes

    def get_sinks(self):
        '''
        Get the sinks from the network.

        Returns: a list of sinks as MeshApiDevice instances
        '''
        nodes = []
        for device in self.devices:
            try:
                if (device.get_role() & CsapNodeRole.BASEROLEMASK) == \
                   CsapNodeRole.Sink:
                    nodes.append(device)
            except MeshApiDeviceException as e:
                # The role may not be set. Then just skip it.
                if e.confirmation_result == \
                   CsapAttributeResult.FailureInvalidAttributeValue:
                    continue
                else:
                    raise e
        return nodes

    def get_subnodes(self):
        '''
        Get the sinks from the network.

        Returns: a list of subnodes as MeshApiDevice instances
        '''

        nodes = []
        for device in self.devices:
            try:
                a = device.get_role()
                if (device.get_role() & CsapNodeRole.BASEROLEMASK) == \
                   CsapNodeRole.Subnode:
                    nodes.append(device)
            except MeshApiDeviceException as e:
                # The role may not be set. Then just skip it.
                if e.confirmation_result == \
                   CsapAttributeResult.FailureInvalidAttributeValue:
                    continue
                else:
                    raise e

        return nodes

    def get_nodes(self):
        '''
        Get all nodes (i.e. not sinks) in the network

        Returns: A list of nodes as MeshApiDevice instances
        '''
        return self.get_headnodes() + self.get_subnodes()

    def remove_device(self, device):
        '''
        Remove a device from this network.

        Attributes:

        - device: A MeshApiDevice instance of the device to be removed.

        '''

        self.devices.remove(device)

    def remove_killed_devices(self):
        '''
        Remove all the killed devices from this network.
        '''
        for device in self.devices:
            if device.killed:
                self.remove_device(device)

    def find_devices(self, diagnostics_handler_defs=[], remove_killed=True):
        '''
        Finds all available interfaces over different interfaces (as defined in
        the configuration), adds them to the network and connect all devices

        Args:
            diagnostics_handler_defs (list): Diagnostics handler definitions
                associated with sink devices

        '''
        debug.info('Finding interfaces..')

        if remove_killed:
            self.remove_killed_devices()

        interfaces = []

        # Find serial interfaces.
        if 'serial' in config.interfaces:
            interfaces.extend(serialinterface.find_devices())

        # Find SPI interfaces.
        if 'spi' in config.interfaces:
            interfaces.extend(spi_interface.find_devices())

        # Find the serial over TCP/IP interfaces.
        if 'serialtcpip' in config.interfaces:
            interfaces.extend(serial_tcp_ip_interface.find_devices())

        # Find the UDP devices.
        if 'udp' in config.interfaces:
            interfaces.extend(ipv6br_udp_interface.find_devices())

        # Find the streaming serial interfaces
        if 'serialstreaming' in config.interfaces:
            interfaces.extend(serial_streaming_interface.find_devices())

        debug.info('Found '+str(len(interfaces))+' interfaces in total.')

        for interface in interfaces:
            self.devices.append(MeshApiDevice(interface))

        # Parse through all diagnostics handler definitions
        for definition in diagnostics_handler_defs:
            for sink in self.get_sinks():
                handler = definition.GetHandler(sink)

    def get_local_devices(self):
        '''
        Get all local devices.

        Returns: A list of local devices as MeshApiDevice instances
        '''
        return filter(lambda x: x.is_local(), self.get_devices())

    def get_remote_devices(self):
        '''
        Get all remote devices.

        Return: A list of remote devices as MeshApiDevice instances
        '''
        return filter(lambda x: not x.is_local(), self.get_devices())

    '''
    Stack functions
    '''

    def set_network_address(self, address):
        '''
        Sets network address to all devices in the network.

        Attributes:

        - address, The network address

        '''

        for device in self.devices:
            device.set_network_address(address)

    def set_cipher_key(self, key):
        '''
        Sets cipher key to all devices in the network.

        Attributes:

        - key: the cipher key in a 16 byte bytearray or string

        '''

        for device in self.devices:
            device.set_cipher_key(key)

    def clear_cipher_key(self):
        '''
        Clear cipher keys in all devices in the network.
        '''
        for device in self.devices:
            device.clear_cipher_key()

    def set_authentication_key(self, key):
        '''
        Sets authentication key to all devices in the network.

        Attributes:

        - key: the authentication key in a 16 byte bytearray or string

        '''

        for device in self.devices:
            device.set_authentication_key(key)

    def clear_authentication_key(self):
        '''
        Clear authentication keys in all devices in the network
        '''
        for device in self.devices:
            device.clear_authentication_key()

    def set_network_channel(self, channel):
        '''
        Set the network channel in the network

        Arguments:

        - Channel: network channel: 1...

        '''
        for device in self.devices:
            device.set_network_channel(channel)

    def set_autostart_msap(self, autostart):
        '''
        Set the autostart in the network

        Arguments:

        - autostart: 0(Auto-start disabled) or 1(Auto-start enabled)

        '''
        for device in self.devices:
            device.set_autostart_msap(autostart)

    def stop_indication_poll(self):
        '''
        Stop indication poll on all devices in the network.
        '''
        for device in self.devices:
            device.stop_indication_poll()

    def stack_stop(self):
        '''
        Stops all the devices in the network
        '''

        for device in self.devices:
            try:
                device.stack_stop()
                device.wait_until_stopped()
            except MeshApiDeviceException as e:
                if e.confirmation_result == \
                   MsapStackStopResult.ErrorStackAlreadyStopped:
                    # Stack was already stopped. Let's continue..
                    continue
                else:
                    raise e

    def stack_start(self, autostart = False):
        '''
        Starts all the devices in the network
        '''
        for device in self.devices:
            try:
                device.stack_start(autostart = autostart)
            except MeshApiDeviceException as e:
                if e.confirmation_result == \
                   MsapStackStartResult.StackRemainsStopped:
                    # Stack is not started, Let's continue..
                    continue
                else:
                    raise e

            device.wait_until_started()

    '''
    Internal functions
    '''
    def __repr__(self):
        '''
        Return the string presentation of MeshApiNetwork.
        '''
        return "MeshApiNetwork"
