# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
Debugging tools for the communication interface.

Created on 17.09.2015

'''
from threading import Thread, current_thread
from Queue import Queue, Empty

import logging.handlers
from configuration import config
import os
import platform

def info(s):
    '''
    Information logging function.
    '''

    global log_queue
    
    if logger:
        log_queue.put(s)
        
def error(s):
    '''
    Error logging function.
    '''

    if logger:
        logger.info(s)

    print "ERROR: "+s

class LoggerThread(Thread):
    '''
    A thread to control all the log file access to prevent
    multiple simultaneous accesses.

    Only one instance should be created.
    '''

    def __init__(self, log_queue):
        ''' 
        Initialize the logger thread.
        '''
        
        super(LoggerThread, self).__init__(name='LoggerThread')

        self.parent_thread = current_thread()
        self.log_queue = log_queue
        
        self.start()

    def run(self):
        '''
        The run method just waiting for new log items and then printing
        and/or logging them to a file.
        '''
        global logger

        # Initialize the logger instance.
        if not self._init_logger():
            print 'Unable to open debugfile {} for writing. Commencing without'.\
                format(config.get('debug', 'logfile'))
            return

        # Start waiting for the queue.
        while self.parent_thread.isAlive():
            try:
                item = self.log_queue.get(timeout=0.1)
            except Empty:
                continue
            
            if logger:
                logger.info(item)

                # Check if we need to print these to the console.
                if config.get('debug','console'):
                    print "Info: "+item

    def _init_logger(self):
        '''
        Initialize the Python logger instance.
        '''

        global logger
        
        try:
            # Create a basic config logger.
            logging.basicConfig(level=logging.DEBUG,
                                datefmt='%d/%m/%Y %H:%M:%S',
                                format='%(asctime)s.%(msecs)03d %(message)s')

            # Check how many archived files the rotating log files should create
            try:
                backup_count = config.get('debug', 'backups')
            except AttributeError:
                backupt_count = 1

            # Create the rotating log file handler.
            handler = logging.handlers.RotatingFileHandler(filename=config.get('debug', 'logfile'),
                                                           mode='a',
                                                           maxBytes=config.get('debug', 'filesize'),
                                                           backupCount=backup_count)
            formatter = logging.Formatter('%(asctime)s.%(msecs)03d %(message)s',
                                          datefmt='%d/%m/%Y %H:%M:%S')
            handler.setFormatter(formatter)
            logger = logging.getLogger('commlayer')
            logger.addHandler(handler)

            # Don't pass the log events to parents to avoid duplicate lines.
            logger.propagate = False

            # Everything went fine.
            return True
            
        except:
            # Some issues
            logger = None
            return False
                    
# Setup the logger instance if debug logging is turned on.
if config.get('debug', 'logging'):
    if not 'logger' in locals():

        logger = None
        
        # Setup the queue for logging thread.
        log_queue = Queue()
            
        # Setup the logging thread.
        loggerthread = LoggerThread(log_queue)            
else:
    logger = None
