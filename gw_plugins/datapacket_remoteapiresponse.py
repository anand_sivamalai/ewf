# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from meshapidsap import DsapDataRx
import meshapi

class DatapacketRemoteApiResponse(meshapi.DsapRemoteApiConfirmation):
    def __init__(self, source_endpoint=240, dest_endpoint=255):
        super(DatapacketRemoteApiResponse, self).__init__()

    @classmethod
    def get_name(cls):
        return "remoteapi-v1"
