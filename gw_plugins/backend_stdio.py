# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from gatewaybackend import GatewayBackend
from Queue import Queue, Empty

class BackendStdio(GatewayBackend):
    name = 'stdio-v1'

    def __init__(self, configuration, device, gw_thread = None):
        '''
        BackendStdio backend plug-in.
        Prints out all the datapackets that are assigned to this plug-in in the
        configuration.

        Args:
            configuration (dict): A dictionary containing the configuration.
            device (MeshApiDevice): The device that is assigned to this plug-in.
            gw_thread (Thread): Gateway thread. Not implemented.
        '''

        super(BackendStdio, self).__init__(configuration, device, gw_thread)

        # This is how the device can be commanded:
        address = self.device.get_address()

        print "STDIO backend started for sink in address "+str(address)

        self.running = True
        self.start()

    def run(self):
        '''
        Loop until self.running == False and print out all the packets.
        '''

        while self.running:
            try:
                frame = self.rx_queue.get(timeout=1)
                print "Got frame: "+str(frame.indication)
            except Empty:
                pass

    def kill(self):
        '''
        Kill the run() method by setting self.running to False.
        '''

        self.running = False

    def __str__(self):
        '''
        Return the string representation of the object.
        '''

        return "BackendStdio for device "+str(self.device)
