# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

import wirepas.meshapidsap
from wirepas.meshapidsap import DsapDataRx
from wirepas.beacon.beacondata_command import BEACON_ENDPOINT_RESPONSE_SRC,\
    BEACON_ENDPOINT_RESPONSE_DST

class DatapacketBeaconResponse(wirepas.meshapidsap.DsapBeaconResponse):
    def __init__(self, source_endpoint=BEACON_ENDPOINT_RESPONSE_SRC,
                 dest_endpoint=BEACON_ENDPOINT_RESPONSE_DST):
        super(DatapacketBeaconResponse, self).__init__()

    @classmethod
    def get_name(cls):
        return "beacon-v1"
