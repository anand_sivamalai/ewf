# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

# Import GatewayBackend from gatewaybackend.
# Needs to be imported *from* the module for the plug-in scan to work.
from wirepas.gatewaybackend import GatewayBackend

# It is a good idea to import Empty from Queue in case the rx_queue.get()
# has a timeout.
from Queue import Empty

#import threading
from gw_plugins.bond_server import BondServerThread

# To get the support for the datapackets we use, we import the classes
# from the modules.
from datapacket_ewf import DatapacketTemplate

from gw_plugins.nodeDB import nodeDB

# The backend plug-in class itself.
class BackendTemplate(GatewayBackend):
    '''
    The backend plug-in class. An instance is created for *each* connected
    sink.

    Needs to inherit *GatewayBackend*.

    Required members:

    - name (str): A string containing a unique name for this plug-in type.

    Needs to implement the following methods:

    - __init__(self, configuration, device, gw_thread): For initializing the
      backend connection instance.
    - run(self): The method that will be running when the plug-in is connected.
    - kill(self): Used to kill the connection. This should command the
      run()-method to finish. Called by the gateway when it is closing the
      connection.

    Optional methods:

    - get_gateway_thread(cls, configuration): An optional @classmethod that
      returns a threading.Thread instance that is a common thread for all the
      GatewayBackend instances for this connection.

    Parameters:

    - configuration: A dictionary containing the configuration for the
      connection from the .ini file(s) for this particular
      backend connection. The configuration file is set by the --configfile
      switch when running gateway.py.
    - device: A meshapidevice.MeshApiDevice instance for the device that
      this particular GatewayBackend instance is associated with.
      Can be used to read information from the device or control
      the device if necessary.
    - gw_thread: If there is a common thread for all the sinks in this
      gateway then this parameter has the reference.
      See documentation for further information.

    '''

    # The backend class needs to have a unique name.
    # The naming convention is '[name]-v[version number]'
    name = 'ewf-v1'

    # The database where all data is stored
    BackendNodeDB = nodeDB()

    # The init function.
    def __init__(self, configuration, device, gw_thread = None):
        '''
        The Gateway plug-in init function.

        This function will initialize the backend connection and start the
        thread.

        *NOTE*: This function is called for *each* sink that is connected.
                See get_gateway_thread method in documentation in case a single
                connection thread is needed for all the sinks.
        '''

        
        # Call the parent classes super.
        # Will set the following values ready:
        # -  self.configuration = configuration
        # -  self.device = device
        # -  self.gw_thread = gw_thread
        # -  self.rx_queue = A new Queue instance for receiving data.
        super(BackendTemplate, self).__init__(configuration, device, gw_thread)

        # We can access the configuration values from a dictionary.
        # The configuration values have been evaluated as Python code so
        # they should appear as evaluated here also.
        # The configuration usually comes from the .ini file set in the
        # gateway.py by the --configfile switch.
        self.integer_value = self.configuration['conf_integer']
        self.list_value = self.configuration['conf_list']
        self.string_value = self.configuration['conf_string']

        
        # Here we can initialize the connection to the backend.
        # We use the configuration parameters for that.
        self._setup_connection(self.integer_value, self.list_value,
                               self.string_value)

        # From the self.device we can use all the functions available in
        # MeshApiDevice. For example get the sink address.
        self.address = self.device.get_address()

        # Print out in the console that the gateway plug-in is starting up:
        print "Template backend plug-in for the sink in address: " +\
        str(self.address)

        # In this template we use self.running boolean variable to signal if the
        # connection should be running or not. This is just one example on how
        # to do it.
        self.running = True

        # IMPORTANT: In the end of __init__ the self.start() needs to be called
        # to start the connection thread.
        self.start()

      
    @classmethod    
    def get_gateway_thread(cls, configuration):
        print("Gateway Thread created and running")
        BondServerGatewayThread = BondServerThread("localhost", 8000, cls.BackendNodeDB)
        BondServerGatewayThread.start()
        return BondServerGatewayThread
    

    def run(self):
        '''
        The method that will be running when the plug-in is connected. It is
        started in the __init__ by self.start().

        Ideally this method shaould run until the kill() method is called. The
        plug-in needs to implement a way for the kill()-method to
        signal run() that it needs exit. In this template it is done by using
        the boolean variable self.running.
        '''

        # The run() is in a loop until self.running is False.
        # __init__() initializes self.running as True
        # and kill() changes it to False effectively killing the run().

        while self.running:

            # self.rx_queue contains all the incoming datapackets. So, we try
            # to get one in each iteration of the loop.
            # In this implementation there needs to be some finite timeout in
            # case no datapackets are coming. Also we need to handle
            # the Empty exception in case the timeout happened.
            try:
                # Get the frame from rx_queue.
                # Timeout should be kept small to make the kill message
                # responsive.
                frame = self.rx_queue.get(timeout=0.1)

                # The datapacket type check can be put here.
                # We have imported the diagnostics packet classes so we can
                # check for them.
                # NOTE: Only the datapackets configured in the .ini-file(s)
                # will be put in the rx_queue!
                # NOTE: We need to check only for the datapacket types
                # that are supported by the backend! The rest can be just
                # discarded.
                
                if isinstance(frame, DatapacketTemplate):
                    self._put_template_datapacket(frame)

                # If we want to insert diagnostics we can put them here also.

            except Empty:
                # Queue.get timeout caused an Empty exception. Just go to next
                # iteration through checking self.running.
                continue

    def kill(self):
        '''
        The kill method to kill the backend.

        This method needs to let the run() method to finish.

        This method gets called by the gateway.py when it closes down
        the backend connection.
        '''

        # In this template the run() is checking on self.running if it is
        # False. So to kill the run() we just need to change it False.
        self.running = False

   
        # Wait for the thread to die.
        self.join()

    def _setup_connection(self, integer_value, list_value, string_value):
        '''
        This method would setup the connection to backend based on
        configuration values.
        '''
        

        # The connection setup code would go here.
        pass

    def _put_template_datapacket(self, frame):
        '''
        This method would implement inserting a datapacket to the backend.

        Args:

        - frame: has the datapacket frame
        '''

        self.BackendNodeDB.addNewData(str(frame.decoded))

        
