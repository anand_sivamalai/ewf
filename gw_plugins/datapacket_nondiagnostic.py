# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from meshapidsap import DsapDataRx

class DatapacketNonDiagnostic(DsapDataRx):
    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        
        [success, hold] = super(DatapacketNonDiagnostic, self).parse_indication(primitive_id,
                                                                       frame_id,
                                                                       payload_length,
                                                                       payload,
                                                                       compare)

        if self.indication['destination_endpoint'][0] == 255:
            return False, hold

        else:
            return success, hold
    
    @classmethod
    def get_name(cls):
        return "nondiagnostic-v1"
