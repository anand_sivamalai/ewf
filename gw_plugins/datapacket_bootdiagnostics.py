# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from meshapidsap import DsapDataRx
import meshapidsap

class DatapacketBootDiagnostics(meshapidsap.DsapBootDiagnostics):
    def __init__(self, source_endpoint=254, dest_endpoint=255):
        super(DatapacketBootDiagnostics, self).__init__()
        
    @classmethod
    def get_name(cls):
        return "bootdiagnostics-v1"
