# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from meshapidsap import DsapDataRx
import meshapidsap

class DatapacketNodeDiagnostics(meshapidsap.DsapNodeDiagnostics):
    def __init__(self, source_endpoint=252, dest_endpoint=255):
        super(DatapacketNodeDiagnostics, self).__init__()
        
    @classmethod
    def get_name(cls):
        return "nodediagnostics-v1"
