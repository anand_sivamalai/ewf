# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

# Import DsapDataRX from meshapidsap.
# Needs to be imported *from* the module for the plug-in scan to work.
from wirepas.meshapidsap import DsapDataRx

# The datapacket plug-in class itself.
class DatapacketTemplate(DsapDataRx):
    '''
    The datapacket plug-in class. A new instance is created for each received
    instance.

    Needs to inherit *DsapDataRx*.

    Needs to implement following methods:

    - get_name(cls): A *@glassmethod* returning the name of the plug-in.
    - parse_indication(self, primitive_id, frame_id, payload_length, payload,
      compare): A method for parsing the payload.

    Optional methods to implement:

    - __init__(self, dest_endpoint): Can be used to initialize the instance and
      set the self.payload for automatic payload parsing in some use cases.
    '''

    @classmethod
    def get_name(cls):
        '''
        This method returns the unique name for this datapacket plug-in.

        **NOTE:** This needs to be a *@classmethod!*

        The naming convention is '[name]-v[version number]'
        '''

        return "EWFdata-v1"

    def __init__(self, dest_endpoint):
        '''
        The __init__ function.

        This is not necessary to implement is shown here as an example.
        '''

        super(DatapacketTemplate, self).__init__(dest_endpoint = dest_endpoint)

        # The initialization of the datapacket would go here.
        pass

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        '''
        Parse indication method overloading the DsapDataRx.parse_indication.

        The decoding of the payload should go here.

        **NOTES**:

        - In datapacket plug-ins the "compare" argument should always be false!
        - This will be called for ALL incoming indications so it needs
          to check from the parent class if the indication could be decoded
          as a DSAP-DATA_RX indication!
        - None of the original arguments are required for payload parsing.
          They should just be passed on to the parent function!
        - This function should finish fast so the interface read thread
          does not get stuck here!

        Args (All of these can be ignored in the APDU decode part itself):

        - primitive_id (int): The Dual-MCU API primitive ID for this frame.
          Always 0x03 for datapackets.
        - frame_id (int): The Dual-MCU API frame ID. A cyclic 8-bit frame ID.
        - payload_length (int): Payload length in number of bytes the whole
          Dual-MCU API frame, not just the APDU!
        - payload (str): The frame payload for the whole Dual-MCU API frame, not
          just APDU!
        - compare (bool): True if compared to original values during decoding,
          False otherwise.

        '''

        # We first call the parent class' parse_indication that returns a tuple
        # with two members:
        # - The first part of the tuple tells if we could parse the indication.
        # - The second part is instructions to the interface if to hold on to
        #   the indication. This should be just passed on. Don't modify it!
        [success, hold] = super(DatapacketTemplate, self).parse_indication(
            primitive_id,
            frame_id,
            payload_length,
            payload,
            compare)

        
        # If successful we parse the APDU. It has been stored by the parent's
        # parse_indication to the self.indication['apdu'] tuple's first
        # item. The second item just tells us the datatype (string).
        if success:
            # Get the APDU from the indication
            apdu = self.indication['apdu'][0]
            

            # A good idea is to add some exception handling in case there is
            # some parsing problem.
            try:
                # We parse the APDU.
                if self._parse_apdu(apdu):
                    # Everything went well so just return (True, hold).
                    return True, hold
                else:
                    # Could not parse. Return (False, hold).
                    return False, hold
            except:
                # Something went wrong.
                # Basically all exceptions happening inside the datapacket
                # plug-in are decoding errors so just return (False, hold).
                # Letting an exception raise here would cause the sink
                # specific interface to crash.
                return False, hold

        else:
            # If the parsing was not successful we still need to pass on the
            # return values.
            return False, hold

    def _parse_apdu(self, apdu):
        '''
        This would be the function that parses the APDU and
        stores it to the class instance itself.

        The backend then will get the class instance and use the parsed data
        as it pleases.

        Args:
        - apdu: The string containing the APDU data.

        Returns: True if parsing was successfull, otherwise False.
        '''

        # Here would be the code that decodes the APDU and stores it in the
        # self.
       
        # In this example we just store the whole APDU as a list of integers to
        # self.decoded. The parsed values can be stored to any member though but
        # the backend plug-in(s) needs to know how to access them.
        # self.decoded = list(map(ord, apdu))
        # 
        # We will store the pdu as a string
        self.decoded = apdu

        # If the decoding was successful return True.
        # (We can decode any APDU as a byte array so just return True).
        return True
