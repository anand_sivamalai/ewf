# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

from meshapidsap import DsapDataRx

class DatapacketGeneric(DsapDataRx):
    @classmethod
    def get_name(cls):
        return "generic-v1"
