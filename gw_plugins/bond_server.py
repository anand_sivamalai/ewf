'''
Created on 20Jul.,2018

@author: anands
'''


import threading
import SocketServer
from SimpleHTTPServer import SimpleHTTPRequestHandler 
from nodeDB import nodeDB 


class MyHandler(SimpleHTTPRequestHandler):

    def __init__(self, nodeDBref):
        super( MyHandler, self ).__init__()
        self.nodeDBrefBond = nodeDBref
        
    def do_GET(self):
        self.wfile.write(self.nodeDBrefBond.readData())      
        return

class BondServerThread(object):
  
    def __init__(self, host, port, nodeDBref):
        self.handler = MyHandler(nodeDBref)
        self.server = SocketServer.TCPServer((host, port), self.handler)
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server_thread.daemon = True
    
    def start(self):
        self.server_thread.start()

    def stop(self):
        self.server.shutdown()
        self.server.server_close()
