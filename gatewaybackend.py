# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

import threading
import Queue

class GatewayBackend(threading.Thread):
    '''
    An abstract class where the gateway backend plugins are inherited from.

    Args:
        device (MeshApiDevice): The MeshApiDevice this plug-in instance is
            connected to.
        configuration (dict): A dictionary containing the plug-in configuration
            based on the gateway configuration .ini file.
        gw_thread (Thread): Gateway thread created by the get_gateway_thread.

    **NOTE:** The __init__ method needs to call the self.start() in the end.
    '''

    name = "backend-v1"

    def __init__(self, configuration, device, gw_thread = None):
        super(GatewayBackend, self).__init__(name='GatewayBackendThread')
        self.configuration = configuration
        self.device = device
        self.gw_thread = gw_thread

        # This is the queue where all the incoming frames come to and the
        # backend needs to consume.
        self.rx_queue = Queue.Queue()

    def run(self):
        '''
        This is the run loop and needs to be implemented by the inherited class.
        '''

        raise NotImplementedError

    def kill(self):
        '''
        This is the kill method and needs to be implemented by the inherited
        class.
        '''

        raise NotImplementedError

    def set_logger(self, log_function):
        '''
        Set debug logging function for this backend connection.

        Called by the gateway.py.

        Args:
            log_function (func): The logger function.
        '''

        self.logger = log_function
        self.logger('Logging started')

    def msg(self, text):
        '''Print a message to stdout and logfile (if available).

        Args:
            text (str): The message.
        '''

        print text
        if hasattr(self, 'logger'):
            self.logger(text)

    @classmethod
    def get_name(cls):
        '''
        This returns the name and version of the plugin in
        format: "pluginname-vx".
        '''

        return cls.name

    def indication_callback_gen(self):
        '''
        This function returns the callback that gets called when a registered
        indication comes. The default operation of the callback is to just put
        the indication to the rx_queue.
        '''

        def f(indication):
            self.rx_queue.put(indication)
        return f

    @classmethod
    def get_gateway_thread(self, configuration):
        '''
        This starts and returns a gateway specific thread if such is available.
        Otherwise None.

        Args:
            configuration: This is the same configuration as the BackendGateway
                gets.

        **NOTE:** Only one of these threads are created per gateway connection.
        NOT per each sink like the usual gateway thread.

        **NOTE:** The gateway thread needs to implement a kill() method.
        '''

        return None
