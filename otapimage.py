# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
An OTAP file loader and OTAP image class.

Created on 04.03.2016

'''

from struct import unpack
import crc16

# Minimum and maximum length of OTAP image, in bytes
MIN_OTAP_IMAGE_LENGTH   = (16 + 4)          # 16-byte header included
MAX_OTAP_IMAGE_LENGTH   = (16 + 131072)     # 16-byte header included

FILE_HEADER_LENGTH = (16 + 16)  # Scratchpad tag and header

SCRATCHPAD_V1_TAG = "SCR1\x9a\x930\x82\xd9\xeb\n\xfc1!\xe37"

class OTAPImage():
    '''
    An OTAP Image class.
    '''

    file_tag = None
    length = None
    crc = None
    sequence_number = None
    pad = None
    image_type = None
    image = None
    data = None
    
    def __init__(self):
        pass

    def load_file(self, filename):
        '''
        Load an OTAP image from a file.

        Attributes:
        - filename: OTAP image file's filename.
        '''

        # Load the whole file.
        file = open(filename, 'rb')
        self.data = bytearray(file.read(MAX_OTAP_IMAGE_LENGTH))

        # Unpack the header.
        (self.file_tag,
         self.length, self.crc,
         self.sequence_number,
         self.pad, self.image_type, self.status) = unpack('<16sLHBBLL', self.data[:FILE_HEADER_LENGTH])

        # Leave out the 16 byte header from the image.
        self.image = self.data[FILE_HEADER_LENGTH:]
        
        # Check validity.
        self.check_validity()
        
    def check_validity(self):
        '''
        Check the validity of the image.

        Raises a RuntimeError in case the file is invalid.
        '''
        
        if len(self.image) != self.length:
            raise RuntimeError("Invalid OTAP Image Length")
        if crc16.crc16xmodem(str(self.image), 0xffff) != self.crc:
            raise RuntimeError("Invalid OTAP Image CRC")
        if self.file_tag != SCRATCHPAD_V1_TAG:
            raise RuntimeError("Invalid OTAP File Tag")
