# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
BLE Beacon functions for MeshApiDevice

Created on 21.12.2016

'''

from meshapi_common import *
from meshapicsap import *
from meshapimsap import *
from meshapidsap import *
from beacon.beacondata_command import *
import random

def beacon_request_tx(self,
            data,
            pdu_id = None,
            dest_address=ADDRESS_BROADCAST,
            src_endpoint=BEACON_ENDPOINT_REQUEST_SRC,
            dst_endpoint=BEACON_ENDPOINT_REQUEST_DST,
            qos = 0,
            Req_tx_ind = False,
            timeout = 10):
    '''
    Send data packet which includes the beacon request

    Parameters:

    - data: Data payload
    - PDU ID: ID, valid value is 0-65534
    - dest_address: Address of the destination
    - src_endpoint: 255 for beacon
    - dst_endpoint: 242 for beacon
    - Req_tx_ind: Indicate whether the Indication will be sent
    - timeout: Timeout in seconds for sending

    Return:
    Dictionary with following keys:

    - 'result' : return value
    - 'capacity': Amount of free items
    - 'pduid' : Generated pdu id

    '''

    response = None
    starttime = datetime.datetime.now()
    if pdu_id == None:
        pdu_id = random.randint(0,65534)
    request = DsapDataTx(pdu_id,
                         src_endpoint,
                         dest_address,
                         dst_endpoint,
                         qos,
                         Req_tx_ind,
                         data)

    while (response == None):
        if (datetime.datetime.now() - starttime).total_seconds() > timeout:
            raise RuntimeError('Timeout expired')
        response = self.comminterface.send_request(request)

    retval = {}

    retval['result'] = response.confirmation['result'][0]
    retval['pduid'] = response.confirmation['pdu_id'][0]
    retval['capacity'] = response.confirmation['capacity'][0]

    return retval

def set_ibeacon(self, dest_address, device_address, uuid=None, major=None, minor=None, power=4, index=0):
    '''
    Send ibeacon command to a node at dest_address

    Args:

        dest_address:  Address of the node where the beacon command is sent
        device_address: the address shown in beacon
        uuid:  the UUID of the beacon, if not set, it will be converted from device_address
        major: the major version of the beacon
        minor: the minor version of the beacon
        power: Power in dBm: Valid values are -8, -4, 0 and 4
        index: The beacon URL index from 0 to 7

    Returns:
    Dictionary with following keys:

    - 'result' : return value
    - 'capacity': Amount of free items
    - 'pduid' : Generated pdu id

    '''

    beacon = BeaconCommandMessage()

    beacon_generated = beacon.create_ibeacon(index=index,                    # The payload index, range is 0-7
                                             device_address=device_address,  # The device address
                                             uuid=uuid,                      # The uuid
                                             major=major,                    # The major version
                                             minor=minor,                    # The minor version
                                             measured_power=power)           # The power

    beacon_request = beacon.generate_payload(beacon_generated, value_format='string')

    retval = self.beacon_request_tx(data=beacon_request, dest_address=dest_address)

    return retval


def set_beacon_url(self, dest_address, url, device_address, index = 0, power = 4):
    '''
    Send beacon url command to a node at dest_address

    Attributes:

    - dest_address: Address of the node where the beacon command is sent
    - url: A URL in a string
    - device_address: The device address for the beacon
    - index: The beacon URL index from 0 to 7
    - power: Power in dBm: Valid values are -8, -4, 0 and 4

    Return:
    Dictionary with following keys:

    - 'result' : return value
    - 'capacity': Amount of free items
    - 'pduid' : Generated pdu id

    '''

    beacon = BeaconCommandMessage()

    beacon_generated = beacon.create_eddystone_url_beacon(index=index, measured_power=power,
                                                          device_address=device_address, url=url)

    beacon_request = beacon.generate_payload(beacon_generated, value_format='string')

    retval = self.beacon_request_tx(data=beacon_request, dest_address=dest_address)

    return retval

def set_beacon_mode(self, dest_address, mode):
    '''
    Send beacon mode command to a node at dest_address

    Attributes:

    - dest_address: Address of the node where the beacon command is sent
    - mode: Beacon mode: 0 = off, 1 = TX

    Return:
    Dictionary with following keys:

    - 'result' : return value
    - 'capacity': Amount of free items
    - 'pduid' : Generated pdu id

    '''

    beacon = BeaconCommandMessage()
    beacon_request = beacon.generate_mode_command(value=mode, value_format="uint8")

    retval = self.beacon_request_tx(data=beacon_request, dest_address=dest_address)

    return retval
