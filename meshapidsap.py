# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
DSAP commands

Created on 14.10.2015

'''

from collections import OrderedDict
import datetime
import struct

from meshapicsap import CsapStackProfile, CsapHwMagic
from meshapicommand import MeshApiCommand, MeshApiException
from meshapiindication import MeshApiIndication
from beacon.beacondata_command import BEACON_ENDPOINT_RESPONSE_SRC, BEACON_ENDPOINT_RESPONSE_DST, \
    BeaconCommandMessage
from remote_api.remote_api import ENDPOINT_RESPONSE_DST, ENDPOINT_RESPONSE_SRC, MessageType

# Reserved addresses
ADDRESS_ANYSINK=0
ADDRESS_BROADCAST=0xFFFFFFFF
# Multicast mask
ADDRESS_MULTICAST_MASK=0x80000000

class DataSendResult:
    PDUAccepted = 0
    StackStopped = 1
    InvalidQos = 2
    InvalidTXOption = 3
    OutOfMemory = 4
    UnknownDestination = 5
    InvalidAPDULength = 6
    IndicantionCannotSend = 7
    PDUidInUse = 8
    InvalidEndpoint = 9
    AccessDenied = 10

class SDUSendResult:
    SDUAccepted = 0
    StackStopped = 1
    OutOfMemory = 4
    UnknownDestination = 5
    InvalidSDULength = 6
    IndicantionCannotSend = 7
    AccessDenied = 10

def convert_channel_to_frequency(channel, stack_profile, max_channel=None):
    '''
    Convert channel to frequency

    Args:
        channel (int): Radio channel (1..32)
        stack_profile (int): Stack profile used.
        max_channel (int or None): Optional. Used to differentiate between
            915MHz US and 915MHz Brazil.

    Returns: (float) Frequency in MHz
    '''

    # Formula is base + (separation * (channel - 1)) MHz
    channel_to_freq_map = {
        CsapStackProfile.profile_24 : (2402.0, 3.0),
        CsapStackProfile.profile_868 : (863.2, 0.3),
        CsapStackProfile.profile_915 : (902.4, 0.8),
        CsapStackProfile.profile_917: (922.1, 0.2),
        CsapStackProfile.profile_ti : (2402.0, 3.0),
        CsapStackProfile.profile_865_indian : (865.165, 0.165)
    }

    # If it's 915MHz profile and max 22 channels then it's a Brazil device.
    if (stack_profile == CsapStackProfile.profile_915) and (max_channel == 22):
        # There's a gap between 6th and 7th channel.
        if channel < 7:
            return 902.4 + (channel - 1) * 0.8
        else:
            return 915.4 + (channel - 7) * 0.8

    # In 868MHz profile the second to last and last channel have a bigger gap.
    if (stack_profile == CsapStackProfile.profile_868) and (channel == 19):
        return 868.9

    if stack_profile in channel_to_freq_map:
        return channel_to_freq_map[stack_profile][0] + \
            (channel_to_freq_map[stack_profile][1] * (channel - 1))
    else:
        return 0

class DsapDataTx(MeshApiCommand):
    '''
    transport APDUs from the application to the stack
    '''
    def __init__(self,
                 pdu_id=0,
                 src_endpoint=0,
                 dst_address=ADDRESS_ANYSINK,
                 dst_endpoint=0,
                 QoS=0,
                 Req_tx_ind=False,
                 payload=""
                 ):
        '''
        Initializes a new instance of DsapDataTx class

        Attributes:
        - pdu_id: Pdu identifier
        - src_endpoint: source endpoint
        - dst_address: Destination address
        - dst_endpoint: Destination endpoint
        - QoS: QoS identifier
        - Req_tx_ind: If true, request tx indication
        '''

        super(DsapDataTx, self).__init__()
        self.primitive_id = 0x01

        self.request['pdu_id'] = (pdu_id, 'uint16')
        self.request['source_endpoint'] = (src_endpoint, 'uint8')
        self.request['destination_address'] = (dst_address, 'uint32')
        self.request['destination_endpoint'] = (dst_endpoint, 'uint8')
        self.request['qos'] = (QoS, 'uint8')
        if Req_tx_ind:
            self.request['txoptions'] = (1, 'uint8')
        else:
            self.request['txoptions'] = (0, 'uint8')
        self.request['apdulen'] = (len(payload), 'length uint8')
        self.request['apdu'] = (payload, 'string')

        self.confirmation['pdu_id'] = (pdu_id, 'uint16')
        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['capacity'] = (0, 'uint8')

class DsapDataTxTT(MeshApiCommand):
    '''
    transport APDUs from the application to the stack
    '''
    def __init__(self,
                 pdu_id=0,
                 src_endpoint=0,
                 dst_address=ADDRESS_ANYSINK,
                 dst_endpoint=0,
                 QoS=0,
                 Req_tx_ind=False,
                 buf_delay = 0,
                 payload=""
                 ):
        '''
        Initializes a new instance of DsapDataTx class

        Attributes:
        - pdu_id: Pdu identifier
        - src_endpoint: source endpoint
        - dst_address: Destination address
        - dst_endpoint: Destination endpoint
        - QoS: QoS identifier
        - Req_tx_ind: If true, request tx indication
        - buf_delay: The time the PDU has been in the application buffers
        '''

        super(DsapDataTxTT, self).__init__()
        self.primitive_id = 0x1F

        self.request['pdu_id'] = (pdu_id, 'uint16')
        self.request['source_endpoint'] = (src_endpoint, 'uint8')
        self.request['destination_address'] = (dst_address, 'uint32')
        self.request['destination_endpoint'] = (dst_endpoint, 'uint8')
        self.request['qos'] = (QoS, 'uint8')
        if Req_tx_ind:
            self.request['txoptions'] = (1, 'uint8')
        else:
            self.request['txoptions'] = (0, 'uint8')
        self.request['buffering_delay'] = (buf_delay, 'uint32')
        self.request['apdulen'] = (len(payload), 'length uint8')
        self.request['apdu'] = (payload, 'string')

        self.confirmation['pdu_id'] = (pdu_id, 'uint16')
        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['capacity'] = (0, 'uint8')

class DsapIndicationTx(MeshApiIndication):
    '''
    transport of received Indication from the stack to the application layer
    '''
    def __init__(self):
        super(DsapIndicationTx, self).__init__()

        self.primitive_id = 0x02

        self.indication['indication_status'] = (None, 'uint8')
        self.indication['pdu_id'] = (None, 'uint16')
        self.indication['source_endpoint'] = (None, 'uint8')
        self.indication['destination_address'] = (None, 'uint32')
        self.indication['destination_endpoint'] = (None, 'uint8')
        self.indication['buffering_delay'] =(None, 'uint32')
        self.indication['result'] = (None, 'uint8')

class DsapDataRx(MeshApiIndication):
    '''
    This class is for the DSAP-DATA_RX indication parsing.

    This is also used as parent class for datapacket plugins for the gateway.

    Checks the source_endpoint and dest_endpoint to match with the parsed
    datapacket when parse_indication is run. Also automatically parses
    the payload if it is defined in the self.payload ordered dictionary.
    '''

    def __init__(self, source_endpoint=None,dest_endpoint=None):
        '''
        The __init__ function.

        Args:

        - source_endpoint: The source endpoint where the packet is supposed to
        be coming from.
        - dest_endpoint: The destination endpoint where the packet is supposed
        to be going to.

        '''

        super(DsapDataRx, self).__init__()
        self.source_endpoint = source_endpoint
        self.dest_endpoint = dest_endpoint

        self.primitive_id = 0x03

        self.indication['source_address'] = (None, 'uint32')
        self.indication['source_endpoint'] = (None, 'uint8')
        self.indication['destination_address'] = (None, 'uint32')
        self.indication['destination_endpoint'] = (None, 'uint8')
        self.indication['qos'] = (None, 'uint8')
        self.indication['travel_time'] = (None, 'uint32')
        self.indication['pdu_length'] = (None, 'length uint8')
        self.indication['apdu'] = (None, 'string')

        # This is a ordered dictionary for parsing the payload
        self.payload = OrderedDict()

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        '''
        Parse DataRx specific indication.

        Parses also the payload according to self.payload ordered
        dictionary if it has been defined.

        Args:

        - primitive_id: The Dual-MCU API primitive ID for this frame
        - frame_id: The frame ID
        - payload_length: Payload length for the whole frame
        - payload: The frame payload
        - compare: True if compared to original values during decoding,
          False otherwise

        Returns: A tuple containing two booleans. First value is True
        if DsapDataRx could be parsed, otherwise False.
        The second is True only if the indication should be handled only once by
        the CommInterface receiving it.

        '''
        try:
            [a, b] = super(DsapDataRx, self).parse_indication(primitive_id,
                             frame_id,
                             payload_length,
                             payload,
                             compare)
            # Parse DsapDataRx specific metadata
            if a:
                # See if endpoints match
                if (not self.source_endpoint is None)\
                   and (self.indication['source_endpoint'][0]
                        != self.source_endpoint):
                    return False, b
                if (not self.dest_endpoint is None) and \
                   (self.indication['destination_endpoint'][0]
                    != self.dest_endpoint):
                    return False, b

                # Set all the timestamp related values in the indication.
                self.indication['travel_time_s'] = \
                        (self.indication['travel_time'][0] / 128.0, 'metadata')

                self.indication['tx_time'] = (self.indication['rx_time'][0] - \
                            datetime.timedelta(seconds = \
                            self.indication['travel_time_s'][0]),
                            'metadata')

                self.indication['tx_unixtime'] = \
                            (self.indication['rx_unixtime'][0] -
                             self.indication['travel_time_s'][0],
                             'metadata')

                # Parse the payload using payload dictionary if available.
                a = self._parse_payload()

            # Pass on the return values.
            return a, b

        except MeshApiException, e:
            # Parsing caused an exception, so set as could not parse.
            return False, b

    def _parse_payload(self):
        '''
        Parse the payload based on self.payload ordered dictionary
        and store results in self.indication.

        Returns:
        - True: Parsed successfully (or not at all)
        - False: Parsing failed.
        '''
        if len(self.payload) == 0:
            return True
        try:
            if self._generic_parse(self.payload, self.indication['apdu'][0]):
                # Copy these to actual indication values
                self.indication.update(self.payload)
            return True
        except:
            return False

class LegacyAppConfDataRx(MeshApiIndication):
    '''
    transport of received APP DATA Indication from the stack to the application layer
    '''
    def __init__(self):
        super(AppConfDataRx, self).__init__()

        self.primitive_id = 0x0A

        self.indication['indication_status'] = (None, 'uint8')
        self.indication['sequence_number'] = (None, 'uint8')
        self.indication['diagnostic_data_interval'] = (None, 'uint16')
        self.indication['app_config_data'] = (None, 'string 16')

class AppConfDataRx(MeshApiIndication):
    '''
    transport of received APP DATA Indication from the stack to the application layer
    '''
    def __init__(self):
        super(AppConfDataRx, self).__init__()

        self.primitive_id = 0x3F

        self.indication['indication_status'] = (None, 'uint8')
        self.indication['sequence_number'] = (None, 'uint8')
        self.indication['diagnostic_data_interval'] = (None, 'uint16')
        self.indication['app_config_data'] = (None, 'string 80')

class LegacyAppConfDataRx_length80(MeshApiIndication):
    '''
    transport of received APP DATA Indication from the stack to the application layer
    '''
    def __init__(self):
        super(LegacyAppConfDataRx_length80, self).__init__()

        self.primitive_id = 0x0A

        self.indication['indication_status'] = (None, 'uint8')
        self.indication['sequence_number'] = (None, 'uint8')
        self.indication['diagnostic_data_interval'] = (None, 'uint16')
        self.indication['app_config_data'] = (None, 'string 80')

class DsapSDUTx(MeshApiCommand):
    '''
    transport SDUs from the application to the stack
    '''
    def __init__(self,
                 address=ADDRESS_BROADCAST,
                 payload_id=0,
                 reserved=0,
                 ap_sdu=""
                 ):
        '''
        Initializes a new instance of SDU class

        Attributes:
        - address: Destination node address
        - payload_id: payload_id for the service. Can be selected freely in the user application
        - reserved: reserved for future use.
        - ap_sdu_length: The length of the following APSDU in octets
        - apdu: Application payload
        '''

        super(DsapSDUTx, self).__init__()
        self.primitive_id = 0x3c

        self.request['address'] = (address, 'uint32')
        self.request['payload_id'] = (payload_id, 'uint8')
        self.request['reserved'] = (reserved, 'uint32')
        self.request['ap_sdu_length'] = (len(ap_sdu), 'length uint16')
        self.request['ap_sdu'] = (ap_sdu, 'string')

        #self.confirmation['payload_id'] = (payload_id, 'uint8')
        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['reserved'] = (0, 'uint32')

class DsapSDUIndicationTx(MeshApiIndication):
    '''
    transport of received Indication from the stack to the application layer
    '''
    def __init__(self):
        super(DsapSDUIndicationTx, self).__init__()

        self.primitive_id = 0x3d

        self.indication['indication_status'] = (None, 'uint8')
        self.indication['destination_address'] = (None, 'uint32')
        self.indication['payload_id'] = (None, 'uint8')
        self.indication['buffering_delay']= (None, 'uint32')
        self.indication['result'] = (None, 'uint8')

class DsapSDURx(MeshApiIndication):
    '''
    transport of received AP SDUs
    '''
    def __init__(self,):
        super(DsapSDURx, self).__init__()

        self.primitive_id = 0x3e

        self.indication['source_address'] = (None, 'uint32')
        self.indication['payload_id'] = (None, 'uint8')
        self.indication['travel_time'] = (None, 'uint32')
        self.indication['ap_sdu_length'] = (None, 'length uint16')
        self.indication['ap_sdu'] = (None, 'string')

        # This is a ordered dictionary for parsing the payload
        self.payload = OrderedDict()

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        '''
        Parse DataRx specific indication

        Args:
            primitive_id:
            frame_id:
            payload_length:
            payload:
            compare:

        Returns:

        '''
        try:
            [a, b] = super(DsapSDURx, self).parse_indication(primitive_id,
                             frame_id,
                             payload_length,
                             payload,
                             compare)
            # Parse dsap rx specific metadata
            if a:
                self.indication['travel_time_s'] = (self.indication['travel_time'][0] / 128.0, 'metadata')
                self.indication['tx_time'] = (self.indication['rx_time'][0] - \
                                             datetime.timedelta(seconds=self.indication['travel_time_s'][0]), 'metadata')
                self.indication['tx_unixtime'] = (self.indication['rx_unixtime'][0] - self.indication['travel_time_s'][0],
                                                  'metadata')
                a = self._parse_payload()

            return a, b
        except MeshApiException, e:
            return False, False

    def _parse_payload(self):
        '''
        Parse the payload and store results in indication

        returns:
        - True: Parsed successfully (or not at all)
        - False: Parsing failed
        '''
        if len(self.payload) == 0:
            return True
        try:
            if self._generic_parse(self.payload, self.indication['ap_sdu'][0]):
                # Copy these to actual indication values
                self.indication.update(self.payload)
            return True
        except:
            return False

class DsapDiagnostics(DsapDataRx):
    '''
    Abstract class for diagnostics packet
    '''
    def __init__(self, source_endpoint,dest_endpoint=255):
        super(DsapDiagnostics, self).__init__(source_endpoint, dest_endpoint)

    def _convert_ratio_to_percentages(self, ratio, max_value=255.0):
        '''
        Convert ratio (0...255) to percentages

        Args:
            ratio: Value between 0..255
            max_value: Maximum value equaling 100%

        Returns:
            value 0...100%
        '''
        return (float(ratio) / max_value) * 100.0

    def _convert_power_to_db(self, power, stack_profile):
        '''
        Convert radio power to dB

        Args:
            power: Radio power index (0...)
            stack_profile: Stack profile used

        Returns:
            Power in dB
        '''

        if stack_profile==CsapStackProfile.profile_24:
            power_to_db_map = {
                0: -40.0,
                1: -20.0,
                2: -16.0,
                3: -12.0,
                4: -8.0,
                5: -4.0,
                6: 0.0,
                7: 4.0
            }
            return power_to_db_map[power]
        elif stack_profile==CsapStackProfile.profile_868:
            power_to_db_map = {
                0: -10.0,
                1: 0.0,
                2: 5.0,
                3: 10.0
            }
            return power_to_db_map[power]
        elif stack_profile==CsapStackProfile.profile_915:
            return -10.0 + (power * 10.0)
        elif stack_profile==CsapStackProfile.profile_917:
            power_to_db_map = {
                0: -10.0,
                1: 0.0,
                2: 10.0,
                3: 14.0
            }
            return power_to_db_map[power]
        elif stack_profile==CsapStackProfile.profile_ti:
            power_to_db_map = {
                0: -12.0,
                1: -6.0,
                2: 0.0,
                3: 5.0
            }
            return power_to_db_map[power]
        else:
            return 0

    def _convert_rssi_to_dbm(self, rssi, stack_profile):
        '''
        Convert rssi to dB

        Args:
            rssi: Received radio power
            stack_profile: Stack profile used

        Returns:
            Received signal level in dBm
        '''

        # Base formula is Rssi/a - b

        rssi_calc_map = {
            CsapStackProfile.profile_24: (255.0/128.0, 128.0),
            CsapStackProfile.profile_868: (2.0, 130.0),
            CsapStackProfile.profile_915: (2.0, 130.0),
            CsapStackProfile.profile_917: (2.0, 130.0),
            CsapStackProfile.profile_ti: (255.0/128.0, 128.0),

        }

        if stack_profile in rssi_calc_map:
            return (rssi / rssi_calc_map[stack_profile][0]) - rssi_calc_map[stack_profile][1]
        else:
            return 0

class DsapTrafficDiagnostics(DsapDiagnostics):
    '''
    Transport of received traffic diagnostics APDU from the stack
    '''
    def __init__(self):
        super(DsapTrafficDiagnostics, self).__init__(251)

        self.payload['access_cycles'] = (None, 'uint16')
        self.payload['cluster_channel'] = (None, 'uint8')
        self.payload['channel_reliability'] = (None, 'uint8')
        self.payload['rx_amount'] = (None, 'uint16')
        self.payload['tx_amount'] = (None, 'uint16')
        self.payload['aloha_rx_ratio'] = (None, 'uint8')
        self.payload['reserved_rx_success_ratio'] = (None, 'uint8')
        self.payload['data_rx_ratio'] = (None, 'uint8')
        self.payload['rx_duplicate_ratio'] = (None, 'uint8')
        self.payload['cca_success_ratio'] = (None, 'uint8')
        self.payload['broadcast_ratio'] = (None, 'uint8')
        self.payload['failed_unicast_ratio'] = (None, 'uint8')
        self.payload['max_reserved_slot_usage'] = (None, 'uint8')
        self.payload['average_reserved_slot_usage'] = (None, 'uint8')
        self.payload['max_aloha_slot_usage'] = (None, 'uint8')

class DsapNeighborDiagnostics(DsapDiagnostics):
    '''
    Transport of received neighbor diagnostics APDU from the stack
    '''

    NODEINFO_MEMBER = 1
    NODEINFO_SYNCHRONIZED_CLUSTER = 2
    NODEINFO_ASSOCIATED_CLUSTER = 3

    def __init__(self):
        super(DsapNeighborDiagnostics, self).__init__(252)

        #self.neighbor = Ordered

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare=False):

        [a, b] = super(DsapNeighborDiagnostics, self).parse_indication(primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare)
        if a:
            # Successful parsing, parse actual payload
            self._parse_actual_neighbors(self.indication['apdu'][0])

        return a, b

    def _parse_actual_neighbors(self, payload):
        '''
        Parse actual neighbors, this is to handle variable length data
        '''
        i = 0

        remaining_payload = payload
        decode_len = None

        neighbors = []

        while True:
            neighbor = OrderedDict()
            neighbor['address_{}'.format(i)] = (None, 'uint24')
            neighbor['cluster_channel_{}'.format(i)] = (None, 'uint8')
            neighbor['radio_power_{}'.format(i)] = (None, 'uint8')
            neighbor['node_info_{}'.format(i)] = (None, 'uint8')
            neighbor['rssi_{}'.format(i)] = (None, 'uint8')

            if decode_len is None:
                decode_len = self._decode_len(neighbor)

            if len(remaining_payload) >= decode_len:
                self._generic_parse(neighbor, remaining_payload[:decode_len], False)
                self.indication.update(neighbor)
                remaining_payload = remaining_payload[decode_len:]
            else:
                break

            # If address is differs from 0, neighbor exists
            address = neighbor['address_{}'.format(i)][0]

            if address != 0:
                # Actual neighbor

                new_neighbor = {}
                new_neighbor['address'] = address
                new_neighbor['cluster_channel'] = neighbor['cluster_channel_{}'.format(i)][0]
                new_neighbor['radio_power'] = neighbor['radio_power_{}'.format(i)][0]
                new_neighbor['node_info'] = neighbor['node_info_{}'.format(i)][0]
                new_neighbor['rssi'] = neighbor['rssi_{}'.format(i)][0]

                neighbors.append(new_neighbor)

            i += 1

        self.indication['neighbors']=(neighbors, 'metadata')

class DsapNodeDiagnostics(DsapDiagnostics):
    '''
    Transport of received node diagnostics APDU from the stack
    '''

    BASEROLE_SUBNODE = 1
    BASEROLE_HEADNODE = 2
    BASEROLE_SINK = 4
    ROLEMASK_BASE = 0x7
    ROLEMASK_CBMAC = 0x10
    ROLEMASK_RELAY = 0x20
    ROLEMASK_AUTOROLE = 0x80

    EVENT_ROLE_CHANGE_TO_SUBNODE = 8
    EVENT_ROLE_CHANGE_TO_HEADNODE = 9
    EVENT_ROUTE_CHANGE = 16
    EVENT_SCANNING_NO_CHANNEL_SELECTED = 24
    EVENT_SCANNING_FTDMA_CONF_WITH_NEIGHBOR = 25
    EVENT_SCANNING_FTDMA_CONF_WITH_NB_NEIGHBOR = 26
    EVENT_SCANNING_TIMING_CONF_WITH_NEIGHBOR = 27
    EVENT_SCANNING_TIMING_CONF_WITH_MULTIPLE_NEIGHBORS = 28
    EVENT_SCANNING_NEED_MORE_NEIGHBORS = 29
    EVENT_SCANNING_PERIODIC = 30
    EVENT_SCANNING_ROLE_CHANGE = 31
    EVENT_BOOT_POWERON = 32
    EVENT_BOOT_INTENTIONAL = 33
    EVENT_BOOT_SW_FAILURE = 34
    EVENT_BOOT_PROCESSOR_FAILURE = 35
    EVENT_BOOT_WATCHDOG_EXPIRE = 36
    EVENT_BOOT_UNINDENTIFIED_REASON = 37
    EVENT_SYNCLOST_ALTERNATIVE_ROUTE = 40
    EVENT_SYNCLOST_PRIMARY_ROUTE = 41
    EVENT_FTDMA_ADJ_MINOR_BOUNDARY = 48
    EVENT_FTDMA_ADJ_NOT_IN_SLOT_BOUNDARY = 49
    EVENT_FTDMA_ADJ_CONFLICT_WITH_PRIMARY_ROUTE = 50
    EVENT_FTDMA_ADJ_CONFLICT_WITH_ALTERNATIVE_ROUTE = 51
    EVENT_FTDMA_ADJ_CONFLICT_WITH_NEIGHBOR = 52
    EVENT_FTDMA_ADJ_NO_CHANNEL_SELECTED = 53
    EVENT_FTDMA_ADJ_CHANNEL_BLACKLISTED = 54
    EVENT_FTDMA_ADJ_OTHER_REASON = 55
    EVENT_SINK_CHANGED = 56
    EVENT_ROUTING_LOOP = 64
    EVENT_DENSE_REMOVE_SUBNODE = 72

    def __init__(self):
        super(DsapNodeDiagnostics, self).__init__(253)

        self.payload['access_cycle'] = (None, 'uint16')
        self.payload['role'] = (None, 'uint8')
        self.payload['voltage'] = (None, 'uint8')
        self.payload['max_buffer_usage'] = (None, 'uint8')
        self.payload['average_buffer_usage'] = (None, 'uint8')
        self.payload['mem_alloc_fails'] = (None, 'uint8')
        self.payload['normal_priority_buf_delay'] = (None, 'uint8')
        self.payload['high_priority_buf_delay'] = (None, 'uint8')
        self.payload['scans'] = (None, 'uint8')

        # Downlink delay
        for i in range(0,2):
            self.payload['dl_delay_avg_{}'.format(i)] = (None, 'uint16')
            self.payload['dl_delay_min_{}'.format(i)] = (None, 'uint16')
            self.payload['dl_delay_max_{}'.format(i)] = (None, 'uint16')
            self.payload['dl_delay_samples_{}'.format(i)] = (None, 'uint16')

        self.payload['dropped_packets_0'] = (None, 'uint8')
        self.payload['dropped_packets_1'] = (None, 'uint8')

        # Cost info
        self.payload['cost_info_sink'] = (None, 'uint24')
        for i in range(0,2):
            self.payload['cost_info_next_hop_{}'.format(i)] = (None, 'uint24')
            self.payload['cost_info_cost_{}'.format(i)] = (None, 'uint8')
            self.payload['cost_info_link_quality_{}'.format(i)] = (None, 'uint8')

        # Events
        for i in range(0, 15):
            self.payload['events_{}'.format(i)] = (None, 'uint8')

        self.payload['duty_cycle'] = (None, 'uint16')
        self.payload['current_antenna'] = (None, 'uint8')

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare=False):

        [a, b] = super(DsapNodeDiagnostics, self).parse_indication(primitive_id,
                                                                       frame_id,
                                                                       payload_length,
                                                                       payload,
                                                                       compare)
        if a:
            # Successful parsing, parse also happened events to the list
            events = []
            for i in range(0,15):
                if self.indication['events_{}'.format(i)][0] == 0:
                    break
                events.append(self.indication['events_{}'.format(i)][0])

            self.indication['events'] = (events, 'metadata')

        return a, b

class DsapBootDiagnostics(DsapDiagnostics):

    '''
    Transport of received boot diagnostics APDU from the stack
    '''
    def __init__(self):
        super(DsapBootDiagnostics, self).__init__(254)

        self.payload['boot_count'] = (None, 'uint8')
        self.payload['node_role'] = (None, 'uint8')
        self.payload['sw_dev_version'] = (None, 'uint8')
        self.payload['sw_maint_version'] = (None, 'uint8')
        self.payload['sw_minor_version'] = (None, 'uint8')
        self.payload['sw_major_version'] = (None, 'uint8')
        self.payload['scratchpad_sequence'] = (None, 'uint16')
        self.payload['hw_magic'] = (None, 'uint16')
        self.payload['stack_profile'] = (None, 'uint16')
        self.payload['otap_enabled'] = (None, 'uint8')
        self.payload['boot_line_number'] = (None, 'uint16')
        self.payload['file_hash'] = (None, 'uint16')
        self.payload['stack_trace'] = (None, 'string 12')

class DsapBeaconResponse(DsapDataRx):
    '''
    Transport of received beacon confirmations from the stack
    '''
    def __init__(self):
        super(DsapBeaconResponse, self).__init__(source_endpoint=BEACON_ENDPOINT_RESPONSE_SRC,
                                                 dest_endpoint=BEACON_ENDPOINT_RESPONSE_DST)

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare=False):

        [a, b] = super(DsapBeaconResponse, self).parse_indication(primitive_id,
                                                                  frame_id,
                                                                  payload_length,
                                                                  payload,
                                                                  compare)
        if a:
            self._parse_beacon_response(self.indication['apdu'][0])

        return a, b

    def _parse_beacon_response(self, _payload):
        '''
        Parse the payload of the beacon response
        '''
        payload = _payload
        beacon = BeaconCommandMessage()
        # the return is a multi-dimensional array, with variable dimensions and variable lengths
        # for more information about the return format, refer to beacon.beacondata_command.py
        # or define your own parse function according to the usage
        response = beacon.handle_response(payload)

        self.indication['beacon_response'] = (response, 'metadata')


class DsapRemoteApiConfirmation(DsapDataRx):
    '''
    Transport of received remote api confirmations from the stack
    '''
    def __init__(self):
        super(DsapRemoteApiConfirmation, self).__init__(source_endpoint=ENDPOINT_RESPONSE_SRC,
                                                        dest_endpoint=ENDPOINT_RESPONSE_DST)

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare=False):

        [a, b] = super(DsapRemoteApiConfirmation, self).parse_indication(primitive_id,
                                                                         frame_id,
                                                                         payload_length,
                                                                         payload,
                                                                         compare)
        if a:
            self._parse_remoteapi_payload(self.indication['apdu'][0])

        return a, b

    def _parse_remoteapi_payload(self, _payload):
        '''
        Parse the whole payload of the remote api response
        '''
        payload = _payload
        responses = []
        only_cancel = True
        while len(payload) > 1:
            response = {}
            # Parse command id and length
            values = struct.unpack('<BB', payload[0:2])
            payload = payload[2:]
            response['response_id'] = values[0]

            # Check if error values or not
            if values[0] >= 0xf8:
                response['success'] = False
            else:
                response['success'] = True

            # Check if cancel/update command is there
            # If there is something else than just cancel response, it is not
            # 'just cancel'
            if values[0] != (MessageType.CancelRequest | MessageType.ResponseMask):
                only_cancel = False
            if values[0] == (MessageType.Delay | MessageType.ResponseMask):
                self.indication['update'] = (True, 'metadata')

            # Check if payload exists
            if values[1] > 0:
                # Read away the payload as stated
                fmt = '<{}s'.format(values[1])
                length = struct.calcsize(fmt)
                values = struct.unpack(fmt, payload[0:length])
                payload = payload[length:]
                response['payload'] = values[0]
            else:
                # If not payload, result is given in length field
                response['payload'] = None

            responses.append(response)

        if only_cancel:
            self.indication['cancel'] = (True, 'metadata')

        self.indication['responses'] = (responses, 'metadata')

