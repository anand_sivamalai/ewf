# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
MeshApi secondary classes

Created on 20.12.2016

'''

class MeshApiDeviceException(Exception):
    '''
    MeshApiDeviceException class for MeshApiDevice and MeshApiNetwork related exceptions.
    '''
    
    # This contains the possible confirmation result failure code.
    # None if not applicable.
    confirmation_result = None
    def __init__(self, message, result=None):
        super(MeshApiDeviceException, self).__init__('{} result:{}'.format(message, result))
        self.confirmation_result = result
