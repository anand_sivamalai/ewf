# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''The framecodec class for UART interface and associated functions.

'''

import binascii
from framecodec import Framecodec, FramecodecException
from struct import *
import crc16
import debug
import time

#### Constants ####

STX                 = 0xc0      # 192
ESC                 = 0xdb      # 219
ESC_STX             = 0xdc      # 220
ESC_ESC             = 0xdd      # 221

class FrameCodecSerial(Framecodec):
    '''
    The framecodec class for UART interface.
    '''

    # Amount of start bytes
    start_byte_amount = 5

    def __init__(self):
        super(FrameCodecSerial, self).__init__()
        # A string collecting the received data.
        # When whole SLIP frames are encountered they will be removed.
        self.rx_data = ""

        # A string collecting debug prints.
        self.debug_data = ""

        # When the receiving of debug print happened or None
        # if all debug prints have been handled.
        self.debug_print_start_time = None

    def encode(self, msg):
        '''Encode a message into SLIP frame for UART transmission. Adds CRC.

        Args:
           msg (MeshApiCommand): The message as MeshApiCommand instance.

        Returns (bytearray): The encoded SLIP frame.
        '''

        # Use the messages encode instance to convert the message to bytearray.
        frame = msg.encode()

        # Insert CRC.
        crc = crc16.crc16xmodem(frame, 0xffff)
        crcframe = pack('<H', crc)

        frame = frame + crcframe

        # Return the message inside a SLIP frame.
        return self._encode_slip_frame(frame)

    def decode(self, frame):
        '''
        Decode a frame from bytearray.

        Note: This method has a state! Multiple calls with partial or multiple
              frames will construct full frames as the data is collected.
              Call with an empty bytearray to see if there is still a frame in
              the data buffer.

        Parameters:
        - frame, data from the serial interface. Does not have to be a full or single frame.

        Returns: The decoded frame as bytearray without SLIP frame and CRC.
        '''

        # Add the new data to old data.
        self.rx_data = self.rx_data + frame

        # Check if there are frame start symbols
        rx_str = "".join(map(chr, self.rx_data))
        startloc = rx_str.find(chr(STX))
        if startloc == -1:
            # Check if there's data outside the SLIP frames
            if len(rx_str) > 0:
                if self.debug_print_start_time is None:
                    self.debug_print_start_time = time.time()

                self.debug_data += rx_str
                self.rx_data = ""
                self._handle_debug_data(False)

            return None

        # If exist, ensure they are located at the beginning of the string
        if startloc > 0:
            # Put the first part to the debug string and remove from rx_data
            self.debug_data += rx_str[:startloc]

            if self.debug_print_start_time is None:
                self.debug_print_start_time = time.time()

            self._handle_debug_data(True)
            self.rx_data = self.rx_data[startloc:]
            if len(self.rx_data) < 7:
                return None

        # Check if enough incoming data (wake-up symbol + frame + crc + end)
        if len(self.rx_data) < 7:
            return None

        # Ensure frame end symbol exists
        rx_str = "".join(map(chr, self.rx_data[1:]))
        if rx_str.count(chr(STX)) == 0:
            return None
        
        # Ensure that length of the frame is at least 5  
        # (minimum amount of theoretical frame). Could happen in device boot
        rx_str = "".join(map(chr, self.rx_data[0:]))
        endloc = rx_str.find(chr(STX), 1)
        if endloc < 6:
            self.rx_data = self.rx_data[endloc:]
            return None
        
        # Do slip decoding
        frame, items = self._decode_slip_frame(self.rx_data)
        
        # Remove frame from incoming data
        self.rx_data = self.rx_data[items:]
        
        # Check CRC
        crc_fmt = "<H"
        
        # CRCs are two last characters
        try:
            crc_rx = unpack(crc_fmt, frame[-2:])[0]
        except:
            msg = "Frame decoding error: Could not unpack CRC"
            if self.skip_errors:
                # If skip errors is set return None
                debug.error(msg)
                return None
            else:
                raise FramecodecException(msg)

        # Check CRC
        if (crc16.crc16xmodem("".join(map(chr, frame[:-2])), 0xffff) != crc_rx):
            msg = "Frame decoding error: Invalid CRC"
            if self.skip_errors:
                # If skip errors is set return None
                debug.error(msg)
                return None
            else:
                raise FramecodecException(msg)
        
        return frame[:-2]

    def _encode_slip_frame(self, data):
        '''
        Internal function to encode a SLIP frame.
        
        Parameters:
        - data, the data to be encoded as bytearray

        Returns: The data in a SLIP frame.
        '''

        out_data = bytearray()
        
        # Wake up node with multiple STX.
        out_data.extend([STX] * self.start_byte_amount)
        
        # Escape symbols for data.
        for byte in data:
            if byte == chr(ESC):
                out_data.append(ESC)
                out_data.append(ESC_ESC)
            elif byte == chr(STX):
                out_data.append(ESC)
                out_data.append(ESC_STX)
            else:
                out_data.append(byte)

        # Append STX at the end.
        out_data.append(STX)
        
        return out_data

    def _decode_slip_frame(self, data):
        '''Decode a SLIP frame.

        Args:
           data (str): input data

        Returns (tuple): tuple of two values:
           1) decoded frame (tailing and leading separators removed)
           2) amount of bytes parsed
        '''

        amountofbytes = 0

        # Remove leading bytes
        while data[0] == STX:
            data = data[1:]
            amountofbytes = amountofbytes + 1

        # Decode actual frame
        # Find ending location
        endloc = data.find(chr(STX))
        amountofbytes = amountofbytes + endloc + 1

        payload = data[0:endloc]

        retval = bytearray()

        i = 0
        while i < len(payload):
            # Check if 0xDB,0xDC or 0xDB,0xDD combinations exist
            escapechar = None
            if payload[i] == ESC:
                if (i+1) < len(payload):
                    if payload[i+1] == ESC_ESC:
                        escapechar = ESC
                    elif payload[i+1] == ESC_STX:
                        escapechar = STX

            if escapechar is None:
                retval.append(payload[i])
            else:
                i += 1
                retval.append(escapechar)
            i += 1

        return retval, amountofbytes

    def _handle_debug_data(self, end):
        ''' Handles the debug data between the SLIP frames. Calls then
        _handle_debug_print for each separated debug_print.

        Args:
           end (bool): True if reading of debug data ended in a beginning of a
               SLIP frame.
        '''

        # If no newlines are found and no SLIP frame start is found yet
        # then just return.
        if self.debug_data.find('\n') == -1 and not end:
            return

        # Split the debug prints by newline
        debug_prints = self.debug_data.split('\n')

        # Handle all the debug prints except the last one which may be
        # unfinished or a new line.
        for debug_print in debug_prints[:-1]:
            self._handle_debug_print(debug_print+'\n')

        # Handle also the last one if this was the last debug print
        # before slip frame and if it's not just a newline.
        if end and self.debug_data[-1] != '\n':
            self._handle_debug_print(debug_prints[-1])
            # Clear the debug data because it's all handled.
            self.debug_data == ''
        else:
            # Store the last one because it might not be complete.
            self.debug_data = debug_prints[-1]

        # Set the debug print start time to None so it will be set again
        # when new debug data is received.
        self.debug_print_start_time = None

    def _handle_debug_print(self, debug_print):
        '''Adds the debug messages into the debug print queue.

        Args:
            debug_print (str): The debug message.
        '''

        self.debug_print_queue.put((self.debug_print_start_time, debug_print))
