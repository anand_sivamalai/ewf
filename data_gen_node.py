# Symbiotech


'''
Assumes node is properly configured. Node sends test data according to SunSpec.
Node configured as headnode.

'''
import time
import json
# meshapi module contains the MeshApiDevice and MeshApiNetwork classes.
from wirepas.meshapi import *

# meshapicsap module contains the CSAP definitions.
from wirepas.meshapicsap import *
configfile = 'nodes.cfg'
def dataexample():
    # Create a network instance.
    network = MeshApiNetwork()

    # Find MeshApi devices connected to the computer.
    # config.py has the configuration for device and interface types that are used.
    print "Finding devices.."
    network.find_devices()

    print "Found: "+str(network.get_devices())


    # Get the node.
    node = network.get_nodes()[0]
    print "Node: "+str(node)

    # Get node address.
    node_address = node.get_address()
    print "Node address: "+str(node_address)

        
    # Read entire file as a data structure    
    with open(configfile) as json_file:  
        data = json.load(json_file)

    print "[ Data transmission started ]\n "
    ii = 0
    while True:   
        # Change all values by 1    
        ii = ii + 1
        for p in data['TREON_SOLAR_NODE']:
            # Could check the actual type of the data and cast it accordingly
            if (p['Value']):
                value = float(p['Value']) + ii
            else:
                value = ii
            TxStr = "NodeID="+ str(node_address) + " "+ str(p['Name']) + "=" + str(value)
            print(TxStr)
            node.data_tx(TxStr)
            time.sleep(2)
       
       
       


    print "Kill network.."
    network.kill()

if __name__ == "__main__":
    dataexample()
