# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
CSAP commands

Created on 14.10.2015

'''

from meshapicommand import MeshApiCommand

class CsapAttribId:
    NodeAddress = 1
    NetworkAddress = 2
    NetworkChannel = 3
    NodeRole = 4
    MTU = 5
    PDUBufferSize = 6
    ImageSequence = 7
    MeshApiVersion = 8
    FirmwareMajor = 9
    FirmwareMinor = 10
    FirmwareMaintenance = 11
    FirmwareDevelopment = 12
    CipherKey = 13
    AuthenticationKey = 14
    ChannelLimits = 15
    AppConfigDataSize = 16
    HwMagic = 17
    StackProfile = 18
    OfflineScan = 20
    ChannelMap = 21
    FeatureLockBits = 22
    FeatureLockKey = 23

class FeatureLockBit:
    CSAP_LOCK_BITS_DSAP_DATA_TX             = 0x00000001
    #CSAP_LOCK_BITS_MSAP_INDICATION_POLL     = 0x00000002
    CSAP_LOCK_BITS_MSAP_STACK_START         = 0x00000004
    CSAP_LOCK_BITS_MSAP_STACK_STOP          = 0x00000008
    CSAP_LOCK_BITS_MSAP_APP_CONFIG_WRITE    = 0x00000010
    CSAP_LOCK_BITS_MSAP_APP_CONFIG_READ     = 0x00000020
    CSAP_LOCK_BITS_MSAP_ATTR_WRITE          = 0x00000040
    CSAP_LOCK_BITS_MSAP_ATTR_READ           = 0x00000080
    CSAP_LOCK_BITS_CSAP_ATTR_WRITE          = 0x00000100
    CSAP_LOCK_BITS_CSAP_ATTR_READ           = 0x00000200
    #CSAP_LOCK_BITS_UNUSED_10                = 0x00000400
    #CSAP_LOCK_BITS_UNUSED_11                = 0x00000800
    CSAP_LOCK_BITS_CSAP_FACTORY_RESET       = 0x00001000
    CSAP_LOCK_BITS_MSAP_SCRATCHPAD_START    = 0x00002000
    # CSAP_LOCK_BITS_MSAP_SCRATCHPAD_BLOCK    = 0x00004000
    CSAP_LOCK_BITS_MSAP_SCRATCHPAD_STATUS   = 0x00008000
    # CSAP_LOCK_BITS_MSAP_SCRATCHPAD_BOOTABLE = 0x00010000
    # CSAP_LOCK_BITS_MSAP_SCRATCHPAD_CLEAR    = 0x00020000
    CSAP_LOCK_BITS_MSAP_REMOTE_STATUS       = 0x00040000
    CSAP_LOCK_BITS_MSAP_REMOTE_UPDATE       = 0x00080000
    # CSAP_LOCK_BITS_DSAP_DATA_TX_TT          = 0x00100000
    CSAP_LOCK_BITS_MSAP_GET_NBORS           = 0x00200000
    CSAP_LOCK_BITS_MSAP_SCAN_NBORS          = 0x00400000
    # CSAP_LOCK_BITS_UNUSED_23                = 0x00800000
    # CSAP_LOCK_BITS_UNUSED_24                = 0x01000000
    CSAP_LOCK_BITS_MSAP_SINK_COST_WRITE     = 0x02000000
    CSAP_LOCK_BITS_MSAP_SINK_COST_READ      = 0x04000000
    # CSAP_LOCK_BITS_UNUSED_27                = 0x08000000
    # CSAP_LOCK_BITS_UNUSED_28                = 0x10000000
    CSAP_LOCK_BITS_REMOTE_API_TX            = 0x20000000
    CSAP_LOCK_BITS_APP                      = 0x40000000
    CSAP_LOCK_BITS_OTAP                     = 0x80000000

class CsapNodeRole:
    Sink = 1
    Headnode = 2
    Subnode = 3
    BASEROLEMASK = 0x03
    LowLatency = 0x10
    RelayMode = 0x20
    AutoRole = 0x80

class CsapAttributeResult:
    Success = 0
    FailureUnsupportedAttribId = 1
    FailureStackInInvalidState = 2
    FailureInvalidAttributeLength = 3
    FailureInvalidAttributeValue = 4
    FailureWriteOnlyAttribute = 5
    FailureAccessDenied = 6

class CsapFactoryResetResult:
    Success = 0
    FailureStackInInvalidState = 1
    FailureInvalidResetKey = 2
    FailureAccessDenied = 3

class CsapStackProfile:
    profile_24 = 1
    profile_868 = 2
    profile_915 = 3
    profile_870 = 4
    profile_917 = 5
    profile_ti = 6
    profile_870_aidon = 7
    profile_865_indian = 8

class CsapHwMagic:
    HWMagic_NRF51 = 1
    HWMagic_EZR32 = 2
    HWMagic_NRF52 = 3
    HWMagic_CC2650 = 4

class CsapAttribWrite(MeshApiCommand):
    '''
    Write CSAP attribute command
    '''
    def __init__(self, attrib_id, value):
        '''
        Initializes a new instance of CsapAttriWrite class

        Attributes:
        - attrib_id: Attribute id (0...)
        - value
        '''

        super(CsapAttribWrite, self).__init__()
        self.primitive_id = 0x0d
        self.request['attribute_id'] = (attrib_id, 'uint16')
        self.request['attribute_length'] = (0, 'length uint8')
        self.request['attribute_value'] = (value, 'string')

        self.confirmation['result'] = (None, 'uint8')

class CsapAttribRead(MeshApiCommand):
    '''
    Reading of CSAP attribute command
    '''
    def __init__(self, attrib_id):
        '''
        Initializes a new instance of CsapAttribRead class

        Attributes:
        - attrib_id: Attribute id (0...)
        '''

        super(CsapAttribRead, self).__init__()
        self.primitive_id = 0x0e

        self.request['attribute_id'] = (attrib_id, 'uint16')

        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['attribute_id'] = (attrib_id, 'uint16')
        self.confirmation['attribute_length'] = (0, 'length uint8')
        self.confirmation['attribute_value'] = ("", 'string')

class CsapFactoryReset(MeshApiCommand):
    '''
    CSAP Factory Reset Command
    '''

    def __init__(self, reset_key = 0x74496f44):
        '''
        Initializes a new instance of CsapFactoryReset class

        No attributes
        '''

        super(CsapFactoryReset, self).__init__()
        self.primitive_id = 0x16

        self.request['reset_key'] = (reset_key, 'uint32')

        self.confirmation['result'] = (0, 'uint8')
