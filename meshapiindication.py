# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This represents a base class for Mesh Api indication

Created on 20.12.2016

'''
from meshapicommand import *

class MeshApiIndication(MeshApiCommand):
    '''
    This class is a container for indication/response message pairs
    '''
    def __init__(self):
        # No requests nor confirmation
        super(MeshApiIndication, self).__init__()
        self.request = None
        self.confirmation = None
        self.indication = OrderedDict()
        self.response = OrderedDict()
        
        self.indication['indication_status'] = (None, 'uint8')
        self.response['result'] = (1, 'uint8')
        
    def parse_confirmation(self, 
                           primitive_id, 
                           frame_id,
                           payload_length,
                           payload):
        
        self.frame_id = None
        # Use same parsing as in command response
        self.confirmation = self.indication
        retval = super(MeshApiIndication, 
                       self).parse_confirmation(primitive_id,
                                                frame_id,
                                                payload_length,
                                                payload)

        if retval['matches']:
            retval['ready'] = True
        
        self.confirmation = None

        # Next tx is never triggered directly after receiving response
        retval['ready'] = False

        return retval

    def parse_indication(self,
                         primitive_id, 
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        
        # Check if primitive id and frame id matches. If not, we're not 
        # interested. Note that received primitive is xor 0x80
        if primitive_id != self.primitive_id:
            return False, True

        # Check that payload_length matches actual payload length.
        if len(payload) != payload_length:
            raise MeshApiException("Indication Parse Error: Payload length does not match payload. Expected: "+str(payload_length)+" Got: "+str(len(payload)))
        if self._generic_parse(self.indication, payload, compare):
            self.indication['rx_time'] = (datetime.datetime.now(), 'metadata')
            self.indication['rx_unixtime'] = (time.time(), 'metadata')
            return True, True
        else:
            return False, True
        
    def encode(self):
        '''
        Encodes a Mesh API command to the format: 
        - Primitive ID
        - Frame ID
        - Payload length
        - (optional) payload
        
        Note: CRC is not calculated since it is UART specific
        
        Returns: string of the encoded data 
        '''

        return self._generic_encode(self.response, self.primitive_id | 0x80)

    def compare(self, other, frametype):
        '''
        Compare two instances of MeshApiCommand based on frametype. "None" fields are not compared.
        Comparison is symmetric: a.compare(b, c) = b.compare(a, c)

        Parameters:
        - other, the other instance
        - frametype, 'confirmation', 'indication', 'response', etc.

        Returns: True if match, False otherwise.
        '''

        my_od = getattr(self, frametype)
        other_od = getattr(other, frametype)

        # Collect all possible unique keys that exist in both dictionaries.
        keys = list(set(my_od.keys()) & set(other_od.keys()))

        # Go through keys which don't have nonetypes as values in dictionaries.
        for key in filter(lambda x: my_od[x][0] != None and other_od[x][0] != None, keys):
            if my_od[key][0] != other_od[key][0]:
                # Values differ, return false.
                return False

        # No mismatching key/value pairs -> return True.
        return True

    def set_frame_value(self, frametype, key, value):
        '''
        Set a frame value for a frametype.

        Parameters:
        - frametype, the frametype string: 'confirmation', 'indication', etc.
        - key, the name of the value
        - value, the value to be set
        '''

        od = getattr(self, frametype)
        od[key]= (value, od[key][1])
    
    def __repr__(self):
        return "MeshAPIIndication Primitive ID: "+hex(self.primitive_id)

class MeshApiGenericIndicationResponse(MeshApiIndication):
    '''
    A class to create a generic indication response.
    '''
    def __init__(self, primitive_id, frame_id, response):
        '''
        Needs the primitive id in creation.
        '''
        super(MeshApiGenericIndicationResponse, self).__init__()        
        self.primitive_id = primitive_id
        self.frame_id = frame_id

        # Always ask for more results.
        self.response['result'] = (response, 'uint8')
