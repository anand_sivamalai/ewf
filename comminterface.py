# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''

General communication interface classes.

Created on 17.09.2015

'''

import debug

from comminterface_common import *

from meshapiindication import MeshApiGenericIndicationResponse
from meshapimsap import MsapIndicationPoll

from indicationpollthread import IndicationPollThread

from threading import Thread,Lock, Semaphore, current_thread
from Queue import Queue, Empty
import copy
import time

from configuration import config

class TxItem(object):
    '''
    Tx item container. Contains transmitted data and how to function with it
    '''

    def __init__(self,
                 request,
                 comminterface,
                 wait_for_end=False,
                 sent_only=False,
                 end_rx_upon_sent=False):
        '''
        Constructor.

        Parameters:
            - request: MeshApiCommand- inherited instance
            - wait_for_end: True: create mechanism for waiting the ending of the message
            - sent_only: True: Send only and release immediately after sending
            - end_rx_upon_sent: True: End currently active rx when this is sent
        '''
        # Tx queue contains dictionary for transmitted item and metadata
        self.item = request

        # Store the comminterface to check its status
        self.comminterface = comminterface

        # Create message queues for end information
        if wait_for_end:
            self.ended = Queue()
        else:
            self.ended = None

        self.sent_only = sent_only
        self.release_rx_upon_sent = end_rx_upon_sent

    def wait_for_end(self, timeout=1):
        '''
        Wait for message sending ended
        Args:
            timeout: Timeout in seconds to wait
        '''

        if not self.ended is None:
            if timeout != None:
                self.ended.get(timeout=timeout)

            # If there's no timeout the code can hang forever in the unfortunate
            # case that the interface got closed so we need to check for that
            # periodically.
            else:
                while True:
                    try:
                        self.ended.get(timeout=0.1)
                        break
                    except Empty:
                        if (not self.comminterface.connected) or (not self.comminterface.alive):
                            break
                        else:
                            continue

    def announce_end(self):
        '''
        Announce tx item started
        '''
        if not self.ended is None:
            self.ended.put(True)

    def is_waiting_responses(self):
        '''
        Tell if we're waiting responses for this command
        Returns:
            True: Yes, we're waiting responses. False: just send it

        '''
        return not self.sent_only

    def is_end_rx(self):
        '''
        Tell if RX should be ended
        '''
        return self.release_rx_upon_sent

class CommInterface(Thread):
    '''
    The abstract parent class for communication interfaces.
    '''

    # Timeout for getting one response for indication timeout
    INDICATION_RESPONSE_TIMEOUT = 1

    def __init__(self):
        '''
        Instantiates an CommInterface object

        note: After calling this method and instantiated all the necessary
        subclass parameters, you should call self.start()
        '''
        super(CommInterface, self).__init__(name='CommIfThread')
        self.debug_info('Communication interface created:')

        # Store parent thread information.
        self.parent_thread = current_thread()

        # Framecodec is None and must be defined in derived classes
        self.framecodec = None
        self.connected = False

        # Debug print callback
        self.debug_cb = None

        # Indication that interface is alive
        self.alive = True

        # If we're expecting indications. Only after we have got the response
        # for indication poll, we'll start receiving indications
        self.expecting_indications = False

        # MeshAPI indication registrations
        self.indication_registrations = []
        self.indication_registrations_lock = Lock()

        # Currently active transmission which is waiting for responses
        self.active_tx = None

        # Semaphore for setting active tx
        self.active_tx_sema = Semaphore()
        self.tx_queue = Queue()

        # Last received indication timestamp. If >INDICATION_RESPONSE_TIMEOUT -> expire indication
        self.last_received_indication_timestamp = None

        # Indication generation thread
        self.ind_thread = None

    def set_indication_poll_interval(self, interval):
        '''
        Set indication poll interval to new value

        Attributes:

        - interval: Time in seconds for indication poll interval
        '''
        self.ind_thread.interval = interval

    def register_indication(self, indication):
        '''
        Register an indication listener.

        Arguments:

        - indication: An instance of MeshApiIndication which has the callback set. The callback will be called once the indication is received and it matches to the indication values in the given indication instance.

        '''
        self.indication_registrations_lock.acquire()
        if not indication in self.indication_registrations:
            self.indication_registrations.append(indication)
        self.indication_registrations_lock.release()

    def deregister_indication(self, indication):
        '''
        Deregister an indication listener.

        Arguments:

        - indication: The registered indication instance.

        '''
        self.indication_registrations_lock.acquire()
        if indication in self.indication_registrations:
            self.indication_registrations.remove(indication)
        self.indication_registrations_lock.release()

    def _handle_indication(self, response, synchronous = True):
        '''
        Handle an indication

        Args:
            response (bytearray): Indication as a bytearray
            synchronous (bool): Is this a synchronous interface? Default True.

        '''

        # If we're not expecting indications, just end this
        if synchronous and not self.expecting_indications:
            return

        self.last_received_indication_timestamp = time.time()

        primitive_id = response[0]
        frame_id = response[1]
        payload_length = response[2]

        self.debug_info("handle ind:0x{:x}/0x{:x}"
                        .format(primitive_id, frame_id))

        removed_indications = []
        # Check if this indication has been registered.
        self.indication_registrations_lock.acquire()
        for indication in self.indication_registrations:
            # We need to make a deepcopy for comparison so we
            # don't overwrite the comparison indication.
            ind_copy = copy.deepcopy(indication)
            result = ind_copy.parse_indication(primitive_id,
                                               frame_id,
                                               payload_length,
                                               response[3:],
                                               compare=True)

            # Store the binary data in case user needs the raw data.
            ind_copy.binary_data = response

            # Call the indication callback.
            if result[0]:
                if indication.rx_callback:
                    indication.rx_callback(ind_copy)
                if not result[1]:
                    removed_indications.append(indication)

        # Remove the response registration.
        for indication in removed_indications:
            self.indication_registrations.remove(indication)
        self.indication_registrations_lock.release()

        # Send a response to device if the interface is synchronous
        if synchronous:
            # Tell to remove the rx after sending
            if response[3] == 0:
                item = TxItem(MeshApiGenericIndicationResponse(primitive_id,
                                                               frame_id,
                                                               response[3]),
                              self,
                              sent_only=True,
                              end_rx_upon_sent=True)
            else:
                item = TxItem(MeshApiGenericIndicationResponse(primitive_id,
                                                               frame_id,
                                                               response[3]),
                              self,
                              sent_only=True)
            self.tx_queue.put(item)

    def _handle_response(self, response, synchronous=True):
        '''
        Handle response
        Args:
            response (bytearray): Indication as a bytearray
            synchronous (bool): Is this a synchronous interface? Default True.

        '''

        primitive_id = response[0]
        frame_id = response[1]
        payload_length = response[2]

        # Store active_tx in case it gets set to None by TX thread while the
        # function is running.
        active_tx = self.active_tx

        # Just to be sure check that active_tx is not None.
        if active_tx is None:
            self.debug_error("Active TX was None")
            return

        try:
            # Sometimes the active TX can be None even when we
            # get a response so just return, unless asynchronous
            # communication is used.
            if active_tx.item is None and synchronous:
                self.debug_info("Active TX item was None")
                return

            result = active_tx.item.parse_confirmation(primitive_id,
                                                            frame_id,
                                                            payload_length,
                                                            response[3:])

        except Exception as e:
            self.debug_error("Parse confirmation failed: "+str(e))
            return

        # We have. Call the callback if there is one.
        if result['matches']:
            try:

                # Store the binary data in case user needs the raw data.
                active_tx.item.binary_data = response

                if active_tx.item.rx_callback:
                    active_tx.item.rx_callback(active_tx.item)
            except Exception as e:
                self.debug_error("RX callback for item failed: "+str(e))
                return

            # Check if command is ended directly after receiving this message
            if result['ready']:
                self._end_tx(active_tx)
            else:
                # we're expecting indications if we got the response
                try:
                    if isinstance(active_tx.item, MsapIndicationPoll):
                        self.expecting_indications = True
                except Exception as e:
                    self.debug_error("Instance check failed: "+str(e))

    def _end_tx(self, item):
        '''
        End current tx and start the next one
        '''
        if (self.active_tx == item) or item.is_end_rx():
            current = self.active_tx
            if current is not None:
                current.announce_end()
            self.active_tx = None
            self.expecting_indications = False

            # Ensure that tx queue is all sent
            while not self.tx_queue.empty() and self.connected and self.alive:
                time.sleep(0.05)

            self.active_tx_sema.release()
            if current is not None:
                self.debug_info('Ending tx 0x{0:x}'.format(current.item.primitive_id))

        item.announce_end()

    def _handle_incoming_packet(self, response):
        '''
        Response or indication has been received, should be handled.

        Arguments:

        - response: a frame containing the response.

        '''

        # Fast check. If we're not expecting any responses, just end it
        if self.active_tx is None:
            return

        # Parse primitive id, frame id and payload length from the message
        primitive_id = response[0]

        # Is it an indication?
        if not (primitive_id & 0x80):
            self._handle_indication(response)
        else:
            self._handle_response(response)

    def _send_frame(self, frame):
        '''
        Write packed frame to the interface
        '''
        raise NotImplementedError

    def run(self):
        '''
        TX thread, this is an eternal loop that contains the transmission thread

        **Note:** If RX requires own thread, it must be created seperately, either here or __init__
        '''
        raise NotImplementedError

    def get_connected(self):
        '''
        Just to get connection status.

        - Returns True if connection is open, False if not.
        '''
        raise NotImplementedError

    def is_local(self):
        '''
        To check if connection is local (not over TCP/IP or similar).

        - Return True if connection is local, False if not.
        '''

        raise NotImplementedError

    def close(self):
        '''
        Close the interface.
        '''

        # Release the tx semaphore just in case it was locked to let
        # all send requests to finish and not hang forever.

        try:
            self.active_tx_sema.release()
        except:
            pass

        # Clear the indication registrations
        self.clear_indication_registrations()


    def open(self):
        '''
        Open the interface.
        '''

        # If the port was killed then raise an exception
        # Dead ports can't be reopened. They need to be recreated.
        if not self.alive:
            raise RuntimeError('Trying to reopen a dead connection.')

    def kill(self):
        '''
        Kill the interface.

        Kills all threads. After this the interface cannot be reopened.
        '''
        if not self.alive:
            return

        self.debug_info("Killing interface: "+str(self))

        # Close the interface.
        try:
            self.close()
        except:
            self.debug_info("Closing during killing failed! Already closed?")

        # Inform the subthreads that we are going to die.
        self.alive = False

        # Kill indication thread first.
        self.stop_indication_poll()


    def get_host_name(self):
        '''
        Get the host name (for remote connections).

        Returns the host name or None if not available.
        '''

        # Return None by default.
        return None

    def get_port(self):
        '''
        Get the port number (for remote connections).

        Returns the port number or None if not available.
        '''

        # Return None by default.
        return None

    def __del__(self):
        '''
        If the instance is garbage collected then kill it.
        '''
        self.kill()

    def send_request(self, request, timeout=5):
        '''
        Send a request in synchronous way (atomic)

        Arguments:

        - request: Instance of MeshApiCommand
        - timeout: timeout to be waited
        - priority: priority to use

        Returns:
        - MeshApiCommand instance containing the confirmation
        '''

        # If the port was closed then raise an exception
        if (not self.connected) or (not self.alive):
            raise RuntimeError('Trying to use a closed connection')

        # Tx queue contains dictionary for transmitted item and metadata
        item = TxItem(request=request, comminterface=self,
                      wait_for_end=True,
                      )

        # Wait for startsema. It is released when previous active tx is done
        try:
            self.active_tx_sema.acquire()
            self.active_tx = item
            self.tx_queue.put(item)
            self.last_received_indication_timestamp = time.time()

            # Wait for endsema
            if isinstance(request, MsapIndicationPoll):
                while True:
                    try:
                        item.wait_for_end(timeout=CommInterface.INDICATION_RESPONSE_TIMEOUT)
                        # Indication poll is ready
                        break
                    except Empty as e:
                        if (time.time() - self.last_received_indication_timestamp) > CommInterface.INDICATION_RESPONSE_TIMEOUT:
                            # We haven't got any response for some period of time
                            raise e

            else:
                item.wait_for_end(timeout=timeout)
        except Exception as e:
            self._end_tx(item)
            return None

        # Should require result somewhere
        return request

    def stop_indication_poll(self):
        '''
        Stop the indication polling.
        '''

        if self.ind_thread:
            self.ind_thread.stop()
            self.ind_thread = None

    def start_indication_poll(self):
        '''
        Start the indication polling.
        '''
        self.debug_info("Start indication poll thread")
        self.ind_thread = IndicationPollThread(self)
        self.ind_thread.start()

    def debug_info(self, text):
        '''
        Debug info function to add the interface information.

        Arguments:

        - text: The information text.

        '''

        debug.info(str(self)+": "+text)

    def debug_error(self, text):
        '''
        Debug error function to add the interface information.

        Arguments:

        - text: The error text.

        '''

        debug.error(str(self)+": "+text)

    def clear_indication_registrations(self):
        '''
        Remove indication registrations
        '''
        self.indication_registrations_lock.acquire()
        self.indication_registrations = []
        self.indication_registrations_lock.release()

    def is_connection_alive(self):
        '''
        Tell if connection is alive or not.

        Returns: True: Is alive, False: Is not alive.

        '''
        return self.alive

    def register_debug_handler(self, callback):
        ''' Register debug handler callback function.

        Args:
            callback (func): Callback function, takes string as an argument.

        '''
        self.debug_cb = callback

    def _handle_debug_print(self, item):
        ''' Handle a single debug print calling the registered callback.

        Args:
            item (tuple): Tuple containing a UNIX timestamp and the debug
                print string.
        '''
        if self.debug_cb is not None:
            self.debug_cb(item)

    def _handle_debug_print_queue(self):
        '''Handle the debug print queue from the framecodec.
        '''

        if not self.framecodec.debug_print_queue.empty():
            while True:
                try:
                    self._handle_debug_print(self.framecodec.
                                             debug_print_queue.get_nowait())
                except Empty:
                    break

def _callback_gen(_queue):
    '''Used to create a callback with a queue.put so we can create queue waits
    for callbacks.
    '''

    def f(command):
        _queue.put(command)
    return f
