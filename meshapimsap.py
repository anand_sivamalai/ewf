# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
MSAP commands

Created on 14.10.2015

'''

from meshapicommand import MeshApiCommand
from meshapiindication import MeshApiIndication

class MsapAttribId:
    StackStatus = 1
    PDUBufferUsage = 2
    PDUBufferCapacity = 3
    #NeighbourCount = 4
    Energy = 5
    Autostart = 6
    RouteCount = 7
    SystemTime = 8
    AccessCycleRange = 9
    AccessCycleLimits = 10
    CurrentAccessCycle = 11
    ImageBlockMax = 12
    MulticastGroups = 13

class MsapAttributeResult:
    Success = 0
    FailureUnsupportedAttribId = 1
    FailureStackInInvalidState = 2
    FailureInvalidAttributeLength = 3
    FailureInvalidAttributeValue = 4
    FailureWriteOnlyAttribute = 5
    FailureAccessDenied = 6

class ScanNeighborsResult:
    Success = 0
    StackInvalidStatus = 1
    AccessDenied =2

class NeighborType:
    AssociatedCluster = 0
    Member = 1
    ScannedCluster = 2

class SinkCostResult:
    Success = 0
    NotASink = 1
    AccessDenied = 2

class MsapStackStatus:
    StackRunning = 0
    StackStopped = 1 << 0
    NetworkAddressMissing = 1 << 1
    NodeAddressMissing = 1 << 2
    NetworkChannelMissing = 1 << 3
    RoleMissing = 1 << 4
    ApplicationConfigurationDataMissing = 1 << 5
    TSAPTestMode = 1 << 6

class MsapStackStartResult:
    Success = 0
    StackRemainsStopped = 1 << 0
    FailureNetworkAddressMissing = 1 << 1
    FailureNodeAddressMissing = 1 << 2
    FailureNetworkChannelMissing = 1 << 3
    FailureRoleMissing = 1 << 4
    FailureAppConDataMissing = 1 << 5
    FailureAccessDenied = 1 << 7

class MsapStackStartOptions:
    AutoStartDisabled = 0
    AutoStartEnabled = 1

class MsapStackStopResult:
    Success = 0
    ErrorStackAlreadyStopped = 1
    AccessDenied = 128

class MsapLegacyAppConfigDataWriteResult:
    Success = 0
    FailureNotASink = 1
    FailureInvalidDiagnosticDataInterval = 2
    FailureInvalidSequenceNumber = 3
    FailureAccessDenied = 4

class MsapAppConfigDataWriteResult:
    Success = 0
    FailureNotASink = 1
    FailureInvalidDiagnosticDataInterval = 2
    FailureInvalidSequenceNumber = 3
    FailureAccessDenied = 4

class MsapLegacyAppConfigDataReadResult:
    Success = 0
    NoConfigurationReceived = 1
    AccessDenied = 2

class MsapAppConfigDataReadResult:
    Success = 0
    NoConfigurationReceived = 1
    AccessDenied = 2

class MsapScratchpadStartResult:
    Success = 0
    FailureInvalidStackState = 1
    FailureInvalidLength = 2
    FailureInvalidSequenceNumber = 3
    FailureAccessDenied = 4

class MsapScratchpadBlockResult:
    SuccessBlock = 0
    SuccessAllData = 1
    FailureAllData = 2
    FailureInvalidStackState = 3
    FailureNoStartRequest = 4
    FailureStartAddressInvalid = 5
    FailureNumberOfBytesInvalid = 6
    FailureInvalidScratchpad = 7

class MsapScratchpadType:
    Blank = 0
    Unprocessed = 1
    Processed = 2

class MsapScratchpadStatusPresent:
    Success = 0
    NewScratchpad = 255

class MsapScratchpadUpdateResult:
    Success = 0
    FailureInvalidStackState = 1
    FailureNoValidScratchpad = 2
    FailureAccessDenied = 3

class MsapScratchpadClearResult:
    Success = 0
    FailureInvalidStackState = 1
    FailureAccessDenied = 2

class MsapRemoteStatusResult:
    Success = 0
    FailureInvalidStackState = 1
    FailureNotASink = 2
    FailureOutOfMemory = 3
    FailureAccessDenied = 4

class MsapRemoteUpdateResult:
    Success = 0
    FailureInvalidStackState = 1
    FailureNotASink = 2
    FailureOutOfMemory = 3
    FailureInvalidSequenceNumber = 4
    FailureDelayValueInvalid = 5
    FailureAccessDenied = 6

#---------------------------------
# Commands
#---------------------------------

class MsapIndicationPoll(MeshApiCommand):
    '''
    poll the stack indications
    '''
    def __init__(self):
        '''
        Initializes a new instance of MsapIndicationPoll

        '''

        super(MsapIndicationPoll, self).__init__()
        self.primitive_id = 0x04

        self.confirmation['indication_status'] = (None, 'uint8')

    def parse_confirmation(self,
                           primitive_id,
                           frame_id,
                           payload_length,
                           payload):
        retval = super(MsapIndicationPoll,
                       self).parse_confirmation(primitive_id,
                                                frame_id,
                                                payload_length,
                                                payload)

        # Next tx is triggered only if we got 'no indications'
        if self.confirmation['indication_status'][0] == 1:
            retval['ready'] = False

        return retval


class MsapStackStart(MeshApiCommand):

    def __init__(self, start_options = 0):
        '''
        Initializes a new instance of MsapStackStart

        Parameters:
        - start_options (optional)
        '''

        super(MsapStackStart, self).__init__()
        self.primitive_id = 0x05
        self.request['start_options'] = (start_options, 'uint8')

        self.confirmation['result'] = (0, 'uint8')

class MsapStackStop(MeshApiCommand):

    def __init__(self):
        '''
        Initializes a new instance of MsapStackStop

        '''

        super(MsapStackStop, self).__init__()
        self.primitive_id = 0x06

        self.confirmation['result'] = (0, 'uint8')

class MsapLegacyAppConfigDataWrite(MeshApiCommand):
    '''
    Writing of LegacyAppConfig Data
    '''
    def __init__(self, sequence_number, diagnostic_data_interval, app_config_data):
        '''
        Initializes a new instance of MsapLegacyAppConfigDataRead
        '''
        super(MsapLegacyAppConfigDataWrite, self).__init__()
        self.primitive_id = 0x08

        self.request['sequence_number'] = (sequence_number, 'uint8')
        self.request['diagnostic_data_interval'] = (diagnostic_data_interval, 'uint16')
        self.request['app_config_data'] = (app_config_data, 'string 16')

        self.confirmation['result'] = (0, 'uint8')

class MsapLegacyAppConfigDataRead(MeshApiCommand):
    '''
    Reading of LegacyAppConfig Data
    '''
    def __init__(self):
        '''
        Initializes a new instance of MsapLegacyAppConfigDataRead
        '''

        super(MsapLegacyAppConfigDataRead, self).__init__()
        self.primitive_id = 0x09

        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['sequence_number'] = (0, 'uint8')
        self.confirmation['diagnostic_data_interval'] = (0, 'uint16')
        self.confirmation['app_config_data'] = ('', 'string 16')

class MsapAppConfigDataWrite(MeshApiCommand):
    '''
    Writing of gen config data
    '''
    def __init__(self, sequence_number, diagnostic_data_interval, app_config_data):
        super(MsapAppConfigDataWrite, self).__init__()
        self.primitive_id=0x3A

        self.request['sequence_number'] = (sequence_number, 'uint8')
        self.request['diagnostic_data_interval'] = (diagnostic_data_interval, 'uint16')
        self.request['app_config_data'] = (app_config_data, 'string 80')

        self.confirmation['result'] = (0, 'uint8')

class MsapAppConfigDataRead(MeshApiCommand):
    '''
    Reading of gen config data
    '''

    def __init__(self):
        super(MsapAppConfigDataRead, self).__init__()
        self.primitive_id = 0x3B

        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['sequence_number'] = (0, 'uint8')
        self.confirmation['diagnostic_data_interval'] = (0, 'uint16')
        self.confirmation['app_config_data'] = ('', 'string 80')

class MsapAttribWrite(MeshApiCommand):
    '''
    Write MSAP attribute command
    '''
    def __init__(self, attrib_id, value):
        '''
        Initializes a new instance of MsapAttriWrite class

        Attributes:
        - attrib_id: Attribute id (0...)
        - value
        '''

        super(MsapAttribWrite, self).__init__()
        self.primitive_id = 0x0b
        self.request['attribute_id'] = (attrib_id, 'uint16')
        self.request['attribute_length'] = (0, 'length uint8')
        self.request['attribute_value'] = (value, 'string')

        self.confirmation['result'] = (None, 'uint8')

class MsapAttribRead(MeshApiCommand):
    '''
    Reading of MSAP attribute command
    '''
    def __init__(self, attrib_id):
        '''
        Initializes a new instance of MsapAttribRead class

        Attributes:
        - attrib_id: Attribute id (0...)
        '''

        super(MsapAttribRead, self).__init__()
        self.primitive_id = 0x0c

        self.request['attribute_id'] = (attrib_id, 'uint16')

        self.confirmation['result'] = (0, 'uint8')
        self.confirmation['attribute_id'] = (attrib_id, 'uint16')
        self.confirmation['attribute_length'] = (0, 'length uint8')
        self.confirmation['attribute_value'] = ("", 'string')

class MsapGetNeighbors(MeshApiCommand):
    '''
    MSAP-GET_NBORS request
    '''
    def __init__(self):
        super(MsapGetNeighbors, self).__init__()
        self.primitive_id = 0x20

        self.confirmation['number_of_neighbors'] = (None, 'uint8')

        # Repeat for 8 neighbors
        for i in range(0,8):
            self.confirmation['neighbor_address_{}'.format(i)] = \
                (None, 'uint32')
            self.confirmation['link_reliability_{}'.format(i)] = (None, 'uint8')
            self.confirmation['normalized_rssi_{}'.format(i)] = (None, 'uint8')
            self.confirmation['cost_{}'.format(i)] = (None, 'uint8')
            self.confirmation['channel_{}'.format(i)] = (None, 'uint8')
            self.confirmation['neighbor_type_{}'.format(i)] = (None, 'uint8')
            self.confirmation['tx_power_{}'.format(i)] = (None, 'uint8')
            self.confirmation['rx_power_{}'.format(i)] = (None, 'uint8')
            self.confirmation['last_update_{}'.format(i)] = (None, 'uint16')

class MsapStartScanNeighbors(MeshApiCommand):
    def __init__(self, start_options=0):
        '''
        Start neighbors scanning Serial API request

        '''

        super(MsapStartScanNeighbors, self).__init__()
        self.primitive_id = 0x21

        self.confirmation['result'] = (None,'uint8')

class MsapCostWrite(MeshApiCommand):
    '''
    MSAP-COST_WRITE request
    '''
    def __init__(self, cost):
        '''
        Initializes a new instance of MsapAttriWrite class

        Attributes:
        - cost :0-254
        '''

        super(MsapCostWrite, self).__init__()
        self.primitive_id = 0x38
        self.request['cost'] = (cost, 'uint8')

        self.confirmation['result'] = (None, 'uint8')

class MsapCostRead(MeshApiCommand):
    '''
    MSAP-COST_READ request
    '''
    def __init__(self):
        super(MsapCostRead, self).__init__()
        self.primitive_id = 0x39

        self.confirmation['result'] = (None, 'uint8')
        self.confirmation['cost'] = (None, 'uint8')

class MsapScratchpadStart(MeshApiCommand):
    '''
    Start OTAP Scratchpad
    '''
    def __init__(self, otap_seq, image_length):
        '''
        Initializes a new instance of MsapAttriWrite class

        Attributes:
        - otap_seq: OTAP Sequence Number
        - image_length: Image length in bytes
        '''

        super(MsapScratchpadStart, self).__init__()
        self.primitive_id = 0x17
        self.request['scratchpad_length'] = (image_length, 'uint32')
        self.request['otap_seq'] = (otap_seq, 'uint8')

        self.confirmation['result'] = (None, 'uint8')

class MsapScratchpadBlock(MeshApiCommand):
    '''
    Write OTAP Scratchpad Block
    '''
    def __init__(self, start_address, block, length = None):
        '''
        Write a block of OTAP Image.

        Attributes:
        - start_address: The start address for this block.
        - block: A 1-112 bytes block of image
        - length: Length of the block, default is the given block size.
        '''

        super(MsapScratchpadBlock, self).__init__()
        self.primitive_id = 0x18

        if length == None:
            length = len(block)

        self.request['start_address'] = (start_address, 'uint32')
        self.request['number_of_bytes'] = (length, 'length uint8')
        self.request['block'] = (block, 'string')

        self.confirmation['result'] = (None, 'uint8')

class MsapScratchpadStatus(MeshApiCommand):
    '''
    Get the OTAP Scratchpad status.
    '''
    def __init__(self):
        '''
        A simple request.
        '''

        super(MsapScratchpadStatus, self).__init__()
        self.primitive_id = 0x19

        self.confirmation['scratchpad_length'] = (None, 'uint32')
        self.confirmation['crc'] = (None, 'uint16')
        self.confirmation['otap_seq'] = (None, 'uint8')
        self.confirmation['scratchpad_type'] = (None, 'uint8')
        self.confirmation['scratchpad_status'] = (None, 'uint8')
        self.confirmation['processed_length'] = (None, 'uint32')
        self.confirmation['processed_crc'] = (None, 'uint16')
        self.confirmation['processed_seq'] = (None, 'uint8')
        self.confirmation['fw_mem_area_id'] = (None, 'uint32')
        self.confirmation['fw_major_version'] = (None, 'uint8')
        self.confirmation['fw_minor_version'] = (None, 'uint8')
        self.confirmation['fw_maintenance_version'] = (None, 'uint8')
        self.confirmation['fw_development_version'] = (None, 'uint8')

class MsapScratchpadUpdate(MeshApiCommand):
    '''
    Set OTAP Scratchpad bootable.
    '''
    def __init__(self):
        '''
        A simple request.
        '''

        super(MsapScratchpadUpdate, self).__init__()
        self.primitive_id = 0x1a

        self.confirmation['result'] = (None, 'uint8')

class MsapScratchpadClear(MeshApiCommand):
    '''
    Clear OTAP image.
    '''
    def __init__(self):
        '''
        A simple request.
        '''

        super(MsapScratchpadClear, self).__init__()
        self.primitive_id = 0x1b

        self.confirmation['result'] = (None, 'uint8')

class MsapRemoteStatus(MeshApiCommand):
    '''
    Request remote OTAP status.
    '''
    def __init__(self, address):
        '''
        A simple request.
        '''
        super(MsapRemoteStatus, self).__init__()
        self.primitive_id = 0x1c
        self.request['address'] = (address, 'uint32')

        self.confirmation['result'] = (None, 'uint8')

class MsapRemoteUpdate(MeshApiCommand):
    '''
    Ask OTAP status from a remote node.
    '''
    def __init__(self, address, otap_seq, reboot_delay):
        '''
        Ask OTAP status from a remote node.

        Attributes:
        - address: node_address
        - otap_seq: OTAP sequence number
        - reboot_delay: Seconds how long the scratchpad is going to be processed
        '''

        super(MsapRemoteUpdate, self).__init__()
        self.primitive_id = 0x1e
        self.request['address'] = (address, 'uint32')
        self.request['otap_seq'] = (otap_seq, 'uint8')
        self.request['reboot_delay'] = (reboot_delay, 'uint16')

        self.confirmation['result'] = (None, 'uint8')

#---------------------------------
# Indications
#---------------------------------

class MsapStackState(MeshApiIndication):
    '''
    MSAP stack state indication
    '''

    def __init__(self):
        super(MsapStackState, self).__init__()
        self.primitive_id = 0x07

        self.indication['status'] = (None, 'uint8')

class MsapNeighborScan(MeshApiIndication):

    def __init__(self):
        super(MsapNeighborScan, self).__init__()
        self.primitive_id = 0x22

        self.indication['scan_ready'] = (None, 'uint8')

class MsapRemoteStatusIndication(MeshApiIndication):
    '''
    OTAP image remote status indication.
    '''

    def __init__(self):
        super(MsapRemoteStatusIndication, self).__init__()
        self.primitive_id = 0x1d

        self.indication['source_address'] = (None, 'uint32')
        self.indication['scratchpad_length'] = (None, 'uint32')
        self.indication['crc'] = (None, 'uint16')
        self.indication['otap_seq'] = (None, 'uint8')
        self.indication['scratchpad_type'] = (None, 'uint8')
        self.indication['scratchpad_status'] = (None, 'uint8')
        self.indication['processed_length'] = (None, 'uint32')
        self.indication['processed_crc'] = (None, 'uint16')
        self.indication['processed_seq'] = (None, 'uint8')
        self.indication['fw_mem_area_id'] = (None, 'uint32')
        self.indication['fw_major_version'] = (None, 'uint8')
        self.indication['fw_minor_version'] = (None, 'uint8')
        self.indication['fw_maintenance_version'] = (None, 'uint8')
        self.indication['fw_development_version'] = (None, 'uint8')
        self.indication['seconds_until_update'] = (None, 'uint16')

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare = False):
        # Since legacy uses same id as this, hold the horses if this fails (could be legacy)
        try:
            [a, b] = super(MsapRemoteStatusIndication, self).parse_indication(primitive_id,
                                                                              frame_id,
                                                                              payload_length,
                                                                              payload,
                                                                              compare)
            return a, b
        except:
            # We're not interested
            return False, True

class LegacyMsapRemoteStatusIndication(MeshApiIndication):
    '''
    OTAP image remote status indication.
    '''

    def __init__(self):
        super(LegacyMsapRemoteStatusIndication, self).__init__()
        self.primitive_id = 0x1d

        self.indication['src_addr'] = (None, 'uint32')
        self.indication['status'] = (None, 'uint8')
        self.indication['devel_ver'] = (None, 'uint8')
        self.indication['maintenance_ver'] = (None, 'uint8')
        self.indication['minor_ver'] = (None, 'uint8')
        self.indication['major_ver'] = (None, 'uint8')
        self.indication['otap_seq'] = (None, 'uint8')

    def parse_indication(self,
                         primitive_id,
                         frame_id,
                         payload_length,
                         payload,
                         compare=False):
        # Since legacy uses same id as this, hold the horses if this fails (could be legacy)
        try:
            [a, b] = super(LegacyMsapRemoteStatusIndication, self).parse_indication(primitive_id,
                                                                                    frame_id,
                                                                                    payload_length,
                                                                                    payload,
                                                                                    compare)
            return a, b
        except:
            # We're not interested
            return False, True
