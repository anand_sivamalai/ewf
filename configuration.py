# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
Configuration file parser

Created on 22.12.2016

'''

import ConfigParser
import ast
import os

class Configuration(object):
    '''Configuration class
    '''
    configparser = ConfigParser.ConfigParser()

    def __init__(self):
        self._set_defaults()

    def load_file(self, filename):
        '''Load configuration from a .ini file

        Args:
            filename (str): The .ini filename
        '''
        print 'Loading configuration file:{}'.format(filename)
        if not os.path.isfile(filename):
            print 'Can not load configuration file "' + filename + \
                '"! Aborting..'
            exit(1)

        self.configparser.read(filename)

    def get(self, section, option):
        '''
        Get a configuration value
        Attributes:
          - section: The section name in configuration file
          - option: The option name in the configuration file

        Returns the configuration value in correct type.
        '''
        if not self.configparser.has_option(section, option):
            raise AttributeError('No configuration item "'+option+'" in section "'+section+'"')
        else:
            return ast.literal_eval(self.configparser.get(section, option))

    def set(self, section, option, value):
        '''
        Set a configuration value
        Attributes:
          - section: The section name in configuration file
          - option: The option name in the configuration file
          - value: The value
        '''
        if not self.configparser.has_section(section):
            self.configparser.add_section(section)
        self.configparser.set(section, option, str(value))

    def has_option(self, section, option):
        '''
        Check if configuration option exists.

        Args:
           section (str): The name of the section.
           option (str): The name of the option.

        Return (bool): True if the option existed, otherwise False.
        '''

        return self.configparser.has_option(section, option)

    def __getattr__(self, attr):
        '''
        Overloads the class __getattr__ to make the configuration options appear as class variables.
        NOTE! This is for backwards compatibility with the old config.py style file.

        Attributes:
          - attr: A configuration option. Can be section_option or option if in general section

        Returns the configuration value in correct type.
        '''
        section, option = self._convert_attr_to_section_option(attr)
        return self.get(section, option)

    def __setattr__(self, attr, value):
        '''
        Overloads the class __setattr__ to make the configuration options appear as class variables.
        NOTE! This is for backwards compatibility with the old config.py style file.

        Attributes:
          - attr: A configuration option. Can be section_option or option if in general section

        Returns the configuration value in correct type.
        '''
        section, option = self._convert_attr_to_section_option(attr)
        self.set(section, option, value)

    def _convert_attr_to_section_option(self, attr):
        '''
        Convert attribute name to section and option names.

        Attributes:
          - attr: A configuration option. Can be section_option or option if in general section
        '''

        if attr.split('_')[0] in self.configparser.sections():
            section = attr.split('_')[0]
            option = '_'.join(attr.split('_')[1:])
        else:
            section = "general"
            option = attr

        return section, option

    def _set_defaults(self):
        '''
        Load the default configuration options.
        '''

        self.load_file(os.path.join(os.path.dirname(__file__), 'defaults.ini'))

    def __repr__(self):
        return 'Wirepas Python API configuration object'

# Check if there's a global variable called config and create one if there isn't.
if 'config' in globals():
    global config
else:
    globals()['config'] = Configuration()
