# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
This represents a base class for Mesh Api command

Created on 14.10.2015

'''
from collections import OrderedDict
from struct import *

import binascii
import datetime
import time

class MeshApiCommand(object):
    '''
    This class is a container for request/confirmation pairs
    
    Command representation issue is based on ordered dictionaries, where:
    - key is a string, such as 'rx_endpoint'
    - value is a tuple, containing two values
      - first value is a 'natural' value of the field
      - second value is a string containing length of the value, values are:
        - 'uint8', 'uint16', 'uint32', 'string' 
        - also, it is possible to use 'length' as a length prefix which then
          represents the length of the following string field
    
    There are some 'confirmation only' messages, such as debugs
    '''

    # Common counter for frame id that is increased upon every command instantation
    frame_id = 0

    # Binary request is used when request does not need to be encoded
    binary_request = None

    struct_definitions = {'uint8': 'B',
                          'uint16': 'H',
                          'uint32': 'I',
                          'string': 's',
                          'uint24': '3s',
                          'int8': 'b',
                          'int16': 'h',
                          'int32': 'i',
                          'float32': 'f',
                         }

    def __init__(self):
        '''
        Instantiates a new instance of MeshApiCommand class
        - 
        '''
        # Representation of request payload (host->node)
        self.request = OrderedDict()
        # Representation of confirmation payload (node->host)
        self.confirmation = OrderedDict()
        # Primitive ID of a request, confirmation is xor 0x80
        self.primitive_id = None
        # Frame Id associated with the message
        self.frame_id = MeshApiCommand.frame_id
        MeshApiCommand.frame_id = (MeshApiCommand.frame_id + 1) & 0xff

        #rx_callback: callback to be called when rx has matched, should take
        #    this class instance as an function/method argument
        self.rx_callback = None

    def encode(self):
        '''
        Encodes a Mesh API command if not binary request

        Format:
        - Primitive ID
        - Frame ID
        - Payload length
        - (optional) payload
        
        Note: CRC is not calculated since it is UART specific
        
        Returns: string of the (encoded) data
        '''
        if self.binary_request is None:
            return self._generic_encode(self.request, self.primitive_id)
        else:
            return self.binary_request

    def _generic_encode(self, od, primitive_id):
        # Primitive ID, frame id and payload length
        total_fmt = '<BBB'
        payload_fmt = ''
        total_values = [primitive_id, self.frame_id]
        
        payload_values = []
        
        is_str_len = False
        str_length = 0

        length_key = None
        # Set the lengths to length fields.

        # Go through keys.
        for key in od:
            item = od[key]
            # If value is a length value store the key.
            if item[1].startswith('length'):
                length_key = key
            # Check the length of the next field and put in the place pointed by the stored key.
            elif length_key != None:
                od[length_key] = (len(item[0]), od[length_key][1])
                length_key = None
                
        for value in od.itervalues():
            # For 24-bit value, convert value to string
            if value[1] == 'uint24':
                value[0] = pack('<I', value[0])[:-1]
            # Item 0 contains the value, item 1 the type
            if value[1].startswith('length'):
                length_type = value[1].split(' ')[1]
                is_str_len = True
                str_length = value[0]
                payload_fmt = payload_fmt + self._desc_to_format(length_type)
            elif is_str_len:
                # String field, length defined earlier
                payload_fmt = payload_fmt + '{}{}'.format(len(value[0]), self._desc_to_format('string'))
                is_str_len = False
            elif value[1].startswith('string') and len(value[1]) > len('string'):
                length = value[1].split(' ')[1]
                payload_fmt = payload_fmt + length + self._desc_to_format('string')
            else:
                payload_fmt = payload_fmt + self._desc_to_format(value[1])
            payload_values.append(value[0])
            
        total_fmt = total_fmt + payload_fmt
        # endian mode is required here to correctly calculate the length
        total_values.append(calcsize('<' + payload_fmt))
        total_values = total_values + payload_values

        frame = pack(total_fmt, *total_values)
        return frame
        
    def _desc_to_format(self, desc):
        '''
        Converts a field description to struct string format character
        '''
        
        if desc not in MeshApiCommand.struct_definitions:
            raise MeshApiException("MeshApiCommand._desc_to_format. Unknown description: "+str(desc))
            
        payload_fmt = MeshApiCommand.struct_definitions[desc]
        return payload_fmt
    
    def _desc_to_len(self, desc):
        '''
        Converts a field description to byte length
        '''
        return calcsize(self._desc_to_format(desc))

    def _decode_len(self, od):
        '''

        Calculate decoding length of the message
        Args:
            od: Ordered dictionary

        Returns:
            Amount of bytes to decode

        '''

        total_length = 0
        for value in od.itervalues():
            total_length += self._desc_to_len(value[1])

        return total_length

    def parse_confirmation(self, 
                           primitive_id, 
                           frame_id, 
                           payload_length, 
                           payload,
                           compare = False):
        '''
        Parses confirmation
        
        Arguments:
        - payload, unpacked payload data, excluding crc and so
        
        - Returns dictionary with following keys
            - 'matches' : True -> Value matches, call callback
                          False -> Value is not match
            - 'ready' :   True -> Message is ready and next message can be sent (only if matches)
                          False -> Message is not ready yet
        '''
        # Check if primitive id and frame id matches. If not, we're not 
        # interested. Note that received primitive is xor 0x80
        if (primitive_id ^ 0x80) != self.primitive_id:
            return {'matches':False}

        # If this is a binary request skip frame id check and parsing
        if (self.binary_request is not None):
            return {'matches': True, 'ready': True}

        # If frame id is defined and it does not match, we're not interested
        if (not self.frame_id is None) and (frame_id != self.frame_id):
            return {'matches': False}

        # Check that payload_length matches actual payload length.
        if len(payload) != payload_length:
            raise MeshApiException("Confirmation Parse Error: Payload length does not match payload. Expected: "+str(payload_length)+" Got: "+str(len(payload)))
        
        if self._generic_parse(self.confirmation, payload, compare):
            self.confirmation['rx_time'] = (datetime.datetime.now(), 'metadata')
            self.confirmation['rx_unixtime'] = (time.time(), 'metadata')

            return {'matches': True, 'ready':True}
        else:
            return {'matches': False}

    def _generic_parse(self, od, payload, compare=False):
        '''
        The generic parse function from a payload to ordered dictionary.
        The values are stored in od UNLESS compare is turned on and original 
        values don't match to new ones in the payload

        Parameters:
        - od, the ordered dictionary where data is stored and which contains the comparison values
        - payload, the bytearray frame (without SLIP, CRC, etc)
        - compare, True if compared to original values in od, False otherwise

        Returns: True if values stored, False otherwise (see compare parameter above)
        '''
        
        payload_fmt = '<'
        str_length = None
        pointer = 0
        
        for value in od.itervalues():
            # Item 0 contains the value, item 1 the type
            if value[1].startswith('length'):
                lentype = value[1].split()[1]
                fmt = self._desc_to_format(lentype)
                length_length = self._desc_to_len(lentype)
                try:
                    str_length = unpack('<' + fmt, payload[pointer:pointer+length_length])[0]
                except:
                    raise MeshApiException("Parse Error: Could not parse datafield length")
                
                pointer += length_length
            else:
                if not value[1].startswith('string'):
                    fmt = self._desc_to_format(value[1])
                    pointer += self._desc_to_len(value[1])
                else:
                    # String parsing
                    if str_length == 0:
                        str_length = None
                        continue 
                    if (not str_length):
                        str_length = int(value[1].split()[1])
                    fmt = str(str_length)+'s'                    
                    pointer += str_length
                    str_length = None
    
            payload_fmt = payload_fmt + fmt

        try:
            unpacked = unpack(payload_fmt, payload)
        except:
            raise MeshApiException("Parse Error: Invalid payload. (format: "+payload_fmt+", payload: "+binascii.hexlify(payload)+")")

        # Comparison in case it was asked.
        j = 0
        if compare:
            for key, value in od.iteritems():
                if value[0] != None and value[0] != unpacked[j]:
                    # If mismatch then return false. Don't store values.
                    return False
                j += 1

        # Parse actual values
        i = 0

        for key, value in od.iteritems():
            # Keep type intact

            # If partial packet received, mark remaining as 'None'
            if len(unpacked) <= i:
                od[key] = (None, value[1])
            elif value[1] == 'uint24':
                # Convert 24-bit values back
                od[key] = (unpack('<I', unpacked[i] + '\x00')[0], value[1])
            else:
                od[key] = (unpacked[i], value[1])
            i = i + 1

        return True
    
    def register_rx_callback(self, callback):
        self.rx_callback = callback

    def __repr__(self):
        return "MeshAPICommand: "+hex(self.primitive_id)
    
class MeshApiException(Exception):
    def __init__(self, message):
        super(MeshApiException, self).__init__(message)
