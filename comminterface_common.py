# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
Common definitions and helper classes shared by CommInterface and inherited classes

Created on 20.12.2015

'''

class CommInterfaceException(Exception):
    def __init__(self, message):
        super(CommInterfaceException, self).__init__(message)

