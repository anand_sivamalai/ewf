# Copyright 2017 Wirepas Ltd. All Rights Reserved.
#
# See file LICENSE.txt for full license details.
#

'''
MeshApiDevice class for connection abstraction

Created on 20.12.2016

'''

from commutils import *
from meshapicsap import *
from meshapimsap import *
from meshapidsap import *
from meshapitsap import *
import datetime
import random
from struct import *
from Queue import Queue
from configuration import config
import time

try:
    from spi_interface import SpiInterface
except ImportError:
    pass

from beacon.beacondata_command import *
from meshapi_common import *

class MeshApiDevice:
    def __init__(self, comminterface):
        '''
        Initializes the MeshApiDevice class.

        Attributes:

        - comminterface: A CommInterface instance that has the connection to the device.

        '''

        debug.info("Test device created on communication interface "+str(comminterface))
        self.comminterface = comminterface
        self._create_interface_queues()
        self.diagnostics_handlers = []
        self.killed = False

    '''
    Basic functions
    '''

    def open_connection(self):
        '''
        Opens the communication interface to the device.
        '''
        self.comminterface.open()

    def close_connection(self):
        '''
        Closes the communication interface to the device.
        '''

        # Close the Communication Interface.
        self.comminterface.close()

        # Clear the queues.
        self._create_interface_queues()

    def kill_interface(self):
        '''
        Kills the communication interface to the device.

        This kills all the threads associated with the interface.
        After this the interface cannot be reopened but a new CommInterface instance is needed.
        '''
        self.comminterface.kill()

    def kill(self):
        '''
        Kills the device and all associated threads.
        '''
        # Kill diagnostics handlers
        for diagnostics_handler in self.diagnostics_handlers:
            diagnostics_handler.abort()
        self.kill_interface()

        self.killed = True

    def register_indication_callback(self, message, callback):
        '''
        Register callback to be called when indication is received

        Attributes:

        - message: Instance of MeshApiIndication type message
        - callback: Callback to be called, takes message as an argument

        '''
        message.register_rx_callback(callback)
        self.comminterface.register_indication(message)

    def remove_indication_callbacks(self):
        '''
        Unregister all the indication callbacks to this device
        '''
        self.comminterface.clear_indication_registrations()

    def send_serial_frame(self, frame, timeout=30):
        '''
        Send serial frame.

        Attributes:

        - frame: serial frame
        - timeout: timeout to wait until failure

        Returns: the response serial frame
        '''
        starttime = datetime.datetime.now()

        # Create Mesh API Command
        command = MeshApiCommand()
        command.binary_request = frame

        # Command's primitive id is set from binary data ( first byte )
        # Without this lower levels does not pass the response higher
        command.primitive_id = int(ord(command.binary_request[0]))

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(command)

        return response.binary_data

    '''
    CSAP related functions
    '''

    def get_address(self, timeout=30):
        '''
        Returns the node address after reading it from the device.

        Attributes:

        - timeout: timeout to wait until failure

        Return the address in integer
        '''
        response = self._get_csap_attribute(CsapAttribId.NodeAddress, timeout)
        return uint32_to_int(response.confirmation['attribute_value'][0])

    def set_address(self, address, timeout=30):
        '''
        Sets the node address.

        Attributes:

        - address: 32-bit node address.
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.NodeAddress, int_to_uint32(address), timeout)
        return response

    def get_role(self, timeout=30):
        '''
        Returns the node role after reading it from the device.

        Attributes:

        - timeout: timeout to wait until failure

        Returns: The role in integer
        '''
        response = self._get_csap_attribute(CsapAttribId.NodeRole, timeout)
        return uint8_to_int(response.confirmation['attribute_value'][0])

    def get_diagnostics_role(self, timeout=30):
        '''
        Returns the node role in diagnostics numbering system format.

        Attributes:

        - timeout: timeout to wait until failure

        Returns: The role in integer
        '''

        meshapi_role = self.get_role(timeout)

        map_baserole_to_diagnostic_map = {
            CsapNodeRole.Sink : DsapNodeDiagnostics.BASEROLE_SINK,
            CsapNodeRole.Headnode : DsapNodeDiagnostics.BASEROLE_HEADNODE,
            CsapNodeRole.Subnode : DsapNodeDiagnostics.BASEROLE_SUBNODE}

        meshapi_baserole = meshapi_role & 0x3

        diagnostics_role = map_baserole_to_diagnostic_map[meshapi_baserole]

        mask_to_diagnostics_map = {
            CsapNodeRole.LowLatency : DsapNodeDiagnostics.ROLEMASK_CBMAC,
            CsapNodeRole.RelayMode : DsapNodeDiagnostics.ROLEMASK_RELAY,
            CsapNodeRole.AutoRole : DsapNodeDiagnostics.ROLEMASK_AUTOROLE}

        for mask in mask_to_diagnostics_map:
            if meshapi_role & mask:
                diagnostics_role |= mask_to_diagnostics_map[mask]

        return diagnostics_role


    def set_role(self, role, timeout=30):
        '''
        Sets the node role.

        Attributes:

        - role: 8-bit role definition (see cNodeRole in WP-RM-100 - Wirepas Connectivity Dual-MCU API Reference Manual).
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.NodeRole, int_to_uint8(role), timeout)
        return response

    def get_network_address(self, timeout=30):
        '''
        Returns the node address after reading it from the device.

        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._get_csap_attribute(CsapAttribId.NetworkAddress, timeout)
        return uint24_to_int(response.confirmation['attribute_value'][0])

    def set_network_address(self, address, timeout=30):
        '''
        Set the network address to the device

        Arguments:

        - address: Address of the device
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.NetworkAddress, int_to_uint24(address), timeout)
        return response

    def set_network_channel(self, channel, timeout=30):
        '''
        Set the network channel in the device

        Arguments:
        - channel: network channel
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.NetworkChannel, int_to_uint8(channel), timeout)
        return response

    def get_network_channel(self, timeout=30):
        '''
        Returns the network channel

        - timeout: timeout to wait until failure

        Returns: Network channel used
        '''
        response = self._get_csap_attribute(CsapAttribId.NetworkChannel, timeout)
        return uint8_to_int(response.confirmation['attribute_value'][0])

    def get_network_channel_limits(self, timeout=30):
        '''
        Returns the network channel limits.

        Attributes:

        - timeout: timeout to wait until failure

        Returns the channel limits in a tuple (minimum, maximum).
        '''
        response = self._get_csap_attribute(CsapAttribId.ChannelLimits, timeout)
        try:
            min = uint8_to_int(response.confirmation['attribute_value'][0][0])
            max = uint8_to_int(response.confirmation['attribute_value'][0][1])
        except Exception as e:
            print "The channel limit is {}".format(response)
            raise e
        return (min, max)

    def get_app_config_data_size(self, timeout=30):
        '''
        Return the app config data size

        Arguments:
        - timeout: timeout to wait until failure

        Return: the app config data length.
        '''

        response = self._get_csap_attribute(CsapAttribId.AppConfigDataSize, timeout)
        return uint8_to_int(response.confirmation['attribute_value'][0])

    def get_offline_scanning_interval(self, timeout=30):
        '''
        Return 16 bit offline scanning interval value in seconds

        Arguments:
        - timeout: timeout to wait until failure

        Return: offline scanning interval value in seconds.
        '''

        response = self._get_csap_attribute(CsapAttribId.OfflineScan, timeout)
        return uint16_to_int(response.confirmation['attribute_value'][0])

    def get_stack_profile(self, timeout=30):
        '''
        Get the stack profile used in the device

        Arguments:
        - timeout: timeout to wait until failure

        Return: stack profile used
        '''
        response = self._get_csap_attribute(CsapAttribId.StackProfile, timeout)
        return uint16_to_int(response.confirmation['attribute_value'][0])

    def get_channel_map(self, timeout=30):
        '''
        Get the currently used channel map in the device

        Arguments:
        - timeout: timeout to wait until failure

        Return: Channel map as raw 32-bit number
        '''
        response = self._get_csap_attribute(CsapAttribId.ChannelMap, timeout)
        return uint32_to_int(response.confirmation['attribute_value'][0])

    def set_channel_map(self, channel_map, timeout=30):
        '''
        Sets channel map value

        Arguments:

        - channel_map: Raw 32-bit value for channel map
        - timeout: timeout to wait until failure

        Return: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.ChannelMap, int_to_uint32(channel_map), timeout)
        return response

    def set_offline_scanning_interval(self, scan, timeout=30):
        '''
        Sets maximum offline scanning interval value

        Arguments:

        - scan: 16 bit offline scanning interval value
        - timeout: timeout to wait until failure

        Return: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.OfflineScan, int_to_uint16(scan), timeout)
        return response

    def set_cipher_key(self, key, timeout=30):
        '''
        Set the cipher key in the device

        Arguments:

        - key: the cipher key in a 16 byte bytearray or string
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.CipherKey, str(key), timeout)
        return response

    def clear_cipher_key(self, timeout=30):
        '''
        Clear the cipher key in the device

        Arguments:

        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.CipherKey,
                                           chr(0xff) * 16,
                                           timeout)
        return response

    def has_cipher_key(self, timeout=30):
        '''
        Checks if the device has a cipher key set.

        Arguments:

        - timeout: timeout to wait until failure

        Returns: True if device has a cipher key set, otherwise False.
        '''

        try:
            response = self._get_csap_attribute(CsapAttribId.CipherKey, timeout)
        except MeshApiDeviceException as e:

            if e.confirmation_result == CsapAttributeResult.FailureWriteOnlyAttribute:
                return True

            if e.confirmation_result == CsapAttributeResult.FailureInvalidAttributeValue:
                return False

            raise e

    def has_authentication_key(self, timeout=30):
        '''
        Checks if the device has a authentication key set.

        Arguments:

        - timeout: timeout to wait until failure

        Returns: True if device has a authentication key set, otherwise False.
        '''

        try:
            response = self._get_csap_attribute(CsapAttribId.AuthenticationKey, timeout)
        except MeshApiDeviceException as e:

            if e.confirmation_result == CsapAttributeResult.FailureWriteOnlyAttribute:
                return True

            if e.confirmation_result == CsapAttributeResult.FailureInvalidAttributeValue:
                return False

            raise e

    def set_authentication_key(self, key, timeout=30):
        '''
        Set the authentication key in the device

        Arguments:

        - key: the authentication key in a 16 byte bytearray or string
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.AuthenticationKey, str(key), timeout)
        return response

    def clear_authentication_key(self, timeout=30):
        '''
        Clear the authentication key in the device

        Arguments:

        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_csap_attribute(CsapAttribId.AuthenticationKey,
                                           chr(0xff) * 16,
                                           timeout)
        return response

    def set_feature_lockbits(self,lockbit,timeout=30):
        '''
        Attributes:

        - lockbit:  the feature bit which is going to be locked
        - timeout: timeout for the mesh api failure

        Returns: The response frame
        '''
        response = self._set_csap_attribute(CsapAttribId.FeatureLockBits, int_to_uint32(0xffffffff-lockbit), timeout)
        return response

    def set_lock_key(self,key,timeout=30):
        '''
        Attributes:
        - key: the key for locking and unlock the feature, when the key is all F, means unlocked
        - timeout: timeout in seconds

        Returns: The response frame
        '''
        response = self._set_csap_attribute(CsapAttribId.FeatureLockKey, str(key), timeout)
        return response

    def get_mtu(self, timeout=10):
        '''
        Get the MTU in the device

        Returns:
        Dictionary with following keys:

        - 'result' : return value
        - 'mtu' : MTU size

        '''
        starttime = datetime.datetime.now()

        request = CsapAttribRead(CsapAttribId.MTU)
        response = None
        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = {}
        retval['result'] = response.confirmation['result'][0]
        # Convert string value to numeric value
        retval['mtu'] = unpack('B',
                               response.confirmation['attribute_value'][0])[0]
        return retval

    def factory_reset(self, reset_key = None, timeout=30):
        '''
        Send the factory reset command to the interface.

        Attributes:

        - reset_key: 32bit key (None for default (correct) key).
        - timeout: Amount of time to wait for response

        '''

        starttime = datetime.datetime.now()

        if reset_key is None:
            request = CsapFactoryReset()
        else:
            request = CsapFactoryReset(reset_key)

        response = None

        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError("Timeout expired")
            response = self.comminterface.send_request(request)
        if response.confirmation['result'][0] == CsapFactoryResetResult.Success:
            return
        elif response.confirmation['result'][0] == CsapFactoryResetResult.FailureStackInInvalidState:
            raise MeshApiDeviceException('Stack in invalid state', response.confirmation['result'][0])
        elif response.confirmation['result'][0] == CsapFactoryResetResult.FailureInvalidResetKey:
            raise MeshApiDeviceException('Invalid reset key', response.confirmation['result'][0])
        elif response.confirmation['result'][0] == CsapFactoryResetResult.FailureAccessDenied:
            raise MeshApiDeviceException('Access Denied', response.confirmation['result'][0])
        else:
            raise MeshApiDeviceException('Unknown error', response.confirmation['result'][0])

    '''
    MSAP related functions
    '''

    def start_stack_state_indication_msap(self):
        '''
        Start receiving of incoming stack state indication
        '''
        self.register_indication_callback(MsapStackState(), self._ind_callback_gen(self.stack_state_indication_queue))

    def get_stack_state_indication_msap(self, timeout=30, autoremove=False):
        '''
        Get indication (started with start_stack_state_indication_msap)

        Returns:
        Dictionary with following keys:

        - 'indication_status'    0 or 1
        - 'status'

        '''
        indication = MsapStackState()
        rxpkt = self.stack_state_indication_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        if autoremove:
            self.comminterface.deregister_indication(indication)

        return retval

    def set_autostart_msap(self,autostart, timeout=30):
        '''
        Set the autostart to the device

        Arguments:

        - autostart: 0(Auto-start disabled) or 1(Auto-start enabled)
        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''
        response = self._set_msap_attribute(MsapAttribId.Autostart, int_to_uint8(autostart), timeout)
        return response

    def get_route_count(self):
        '''
        Get route count from the device.

        Returns route count in an integer.
        '''

        response = self._get_msap_attribute(MsapAttribId.RouteCount)
        return uint8_to_int(response.confirmation['attribute_value'][0])

    def set_access_cycle_range(self, min_access_cycle, max_access_cycle, timeout=30):
        '''
        set access cycle range to the device.

        Returns: The response frame.
        '''

        access_range = ((max_access_cycle & 0xffff) << 16) | \
                       ((min_access_cycle & 0xffff))

        response = self._set_msap_attribute(MsapAttribId.AccessCycleRange,
                                            int_to_uint32(access_range),
                                            timeout)
        return response

    def get_access_cycle_range(self):
        '''
        Get access cycle range from the device.

        Returns (dict): Access cycle range in a dictionary with keys "min"
                        and "max".
        '''
        response = self._get_msap_attribute(MsapAttribId.AccessCycleRange)
        max = uint16_to_int(response.confirmation['attribute_value'][0][-2:])
        min = uint16_to_int(response.confirmation['attribute_value'][0][:2])
        return {'min':min, 'max':max}

    def get_access_cycle_limits(self):
        '''
        Get access cycle limits from the device.

        Returns (dict): Access cycle limits in a dictionary with keys "min"
                        and "max".
        '''
        response = self._get_msap_attribute(MsapAttribId.AccessCycleLimits)
        max = uint16_to_int(response.confirmation['attribute_value'][0][-2:])
        min = uint16_to_int(response.confirmation['attribute_value'][0][:2])
        return {'min':min, 'max':max}

    def set_multicast_groups(self, groups=[], timeout=30):
        """Set Multicast groups that device belongs to

        Args:
            groups (list): List of multicast groups addresses (max:10) to associate to
            timeout (float): Amount of time to wait for response

        Returns:
            The response frame.
        """
        while (len(groups) < 10):
            groups.append(0)

        attr_payload = ''
        for _group in groups:
            attr_payload = attr_payload + int_to_uint32(_group)

        response = self._set_msap_attribute(MsapAttribId.MulticastGroups,
                                            attr_payload,
                                            timeout)
        return response

    def get_multicast_groups(self):
        """Get multicast groups that device belongs to

        Returns:
            list: Multicast addresses of the groups
        """
        retval = []

        response = self._get_msap_attribute(MsapAttribId.MulticastGroups)
        grouplist = response.confirmation['attribute_value'][0]
        while len(grouplist) >= 4:
            address = uint32_to_int(grouplist[:4])
            grouplist = grouplist[4:]
            # Skip 0 addresses that indicate 'no group'
            if address != 0:
                retval.append(address)

        return retval

    def get_stack_state(self):
        '''
        Get stack state from the node.

        Returns stack state in an integer.
        '''

        response = self._get_msap_attribute(MsapAttribId.StackStatus)
        return uint8_to_int(response.confirmation['attribute_value'][0])

    def stack_start(self, autostart=False, timeout=30):
        '''
        Start the device Pino stack.

        Attributes:

        - autostart: True if stack is started automatically after a reboot. False if not.

        Returns: The response frame.
        '''
        starttime = datetime.datetime.now()

        if autostart:
            start_options = MsapStackStartOptions.AutoStartEnabled
        else:
            start_options = MsapStackStartOptions.AutoStartDisabled

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(MsapStackStart(start_options=start_options))

        result = response.confirmation['result'][0]
        failures = []
        if result == MsapStackStartResult.Success:

            try:
                # HACK: With the SPI connection the stack needs some time to start.
                if isinstance(self.comminterface, SpiInterface):
                    time.sleep(1)
            except NameError:
                # The SPI interface was missing so just skip..
                pass

            return response
        if result & MsapStackStartResult.StackRemainsStopped:
            failures.append("Stack remains stop")
        if result & MsapStackStartResult.FailureNetworkAddressMissing:
            failures.append("Network address missing")
        if result & MsapStackStartResult.FailureNodeAddressMissing:
            failures.append("Node address missing")
        if result & MsapStackStartResult.FailureNetworkChannelMissing:
            failures.append("Network channel missing")
        if result & MsapStackStartResult.FailureRoleMissing:
            failures.append("Role missing")
        if result & MsapStackStartResult.FailureAppConDataMissing:
            failures.append("App configuration data missing")
        if result & MsapStackStartResult.FailureAccessDenied:
            failures.append("Access Denied")
        if result & 0b01000000:
            failures.append("Unknown error")

        exception_string = ', '.join(failures)
        exception = MeshApiDeviceException(exception_string, result)
        raise exception

    def stack_start_wait_until_success(self, autostart=False, timeout=30):
        '''
        Start the device Pino stack, wait until the stack status changes to started.
        '''

        self.stack_start(autostart = autostart,timeout=timeout)
        self.wait_until_started()

    def stack_stop(self, timeout=30):
        '''
        Stop the device Pino stack.

        Returns: The response frame.
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(MsapStackStop())

        result = response.confirmation['result'][0]
        failures = []
        if result == MsapStackStopResult.Success:
            # HACK: With the SPI connection the stack needs some time to stop.
            try:
                if isinstance(self.comminterface, SpiInterface):
                    time.sleep(1)
            except NameError:
                # The SPI interface was missing so just skip..
                pass

            return response
        if result & MsapStackStopResult.ErrorStackAlreadyStopped:
            failures.append("Stack already stopped")
        if result &  MsapStackStopResult.AccessDenied:
            failures.append("Access Denied")
        if result & 0b01111110:
            failures.append("Unknown error")

        exception_string = ', '.join(failures)
        exception = MeshApiDeviceException(exception_string, result)
        raise exception

    def stack_stop_wait_until_success(self, timeout=30):
        '''
        Stop the device Pino stack and then wait until the status changes to stopped
        '''
        self.stack_stop(timeout=timeout)
        self.wait_until_stopped()

    def wait_until_started(self, timeout=30):
        '''
        Waits until device has been started.

        Attributes:

        - timeout: Timeout in seconds for stack state request
        '''

        starttime = datetime.datetime.now()

        stack_state = MsapStackStatus.StackStopped
        while (stack_state & MsapStackStatus.StackStopped) == MsapStackStatus.StackStopped:
            stack_state = self.get_stack_state()
            # If device is not running, continue waiting
            if (stack_state & 0b11111110) == 0:
                if (datetime.datetime.now() - starttime).total_seconds() < timeout:
                    continue
                else:
                    raise MeshApiDeviceException("Stack not started upon timeout",
                                                 stack_state)
            failures = []
            if stack_state & MsapStackStatus.NetworkAddressMissing:
                failures.append("Network address missing")
            if stack_state & MsapStackStatus.NodeAddressMissing:
                failures.append("Node address missing")
            if stack_state & MsapStackStatus.NetworkChannelMissing:
                failures.append("Network channel missing")
            if stack_state & MsapStackStatus.RoleMissing:
                failures.append("Role missing")
            if stack_state & MsapStackStatus.ApplicationConfigurationDataMissing:
                failures.append("Application configuration data missing")
            exception_string = ', '.join(failures)
            exception = MeshApiDeviceException(exception_string, stack_state)
            exception.confirmation_result = stack_state
            raise exception

    def wait_until_stopped(self, timeout=30):
        '''
        Waits until device has been stopped

        Attribute:

        - timeout: Timeout in seconds for stack state request
        '''
        starttime = datetime.datetime.now()

        stack_state = 0
        while (stack_state & MsapStackStatus.StackStopped) != MsapStackStatus.StackStopped:
            stack_state = self.get_stack_state()
            # If device is not stopped, continue waiting
            if (stack_state & 0b11111110) == 1:
                if (datetime.datetime.now() - starttime).total_seconds() < timeout:
                    continue
                else:
                    raise MeshApiDeviceException("Device not stopped",
                                                 stack_state)

    def get_neighbors(self, timeout=30):
        '''
        Get neighbors info from the node

        Arguments:

        - timeout: Timeout to query the command

        Returns:
        Array of neighbors, where each item is an dictionary with following keys:

        - neighbor_address
        - link_reliability_raw: value 0-255
        - link_reliability: value 0-1
        - normalized_rssi_raw: value 0-255
        - normalized_rssi: value 0-1
        - cost: value 1-255
        - channel: value 1-x
        - neighbor type: 0->next hop,1->member,2->scan
        - txpower: 0-7
        - rxpower: 0-7
        - lastupdate_raw: Amount of seconds since last update
        - lastupdate: Timestamp of the last update
        '''
        response = None
        starttime = datetime.datetime.now()
        request = MsapGetNeighbors()

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = []

        for i in range(0, response.confirmation['number_of_neighbors'][0]):
            nodeinfo = {}
            nodeinfo['neighbor_address'] = \
                response.confirmation['neighbor_address_{}'.format(i)][0]
            nodeinfo['link_reliability_raw'] = \
                response.confirmation['link_reliability_{}'.format(i)][0]
            nodeinfo['link_reliability'] = \
                float(nodeinfo['link_reliability_raw']) / 255.0
            nodeinfo['normalized_rssi_raw'] = \
                response.confirmation['normalized_rssi_{}'.format(i)][0]
            nodeinfo['normalized_rssi'] = \
                float(nodeinfo['normalized_rssi_raw']) / 255.0
            nodeinfo['cost'] = response.confirmation['cost_{}'.format(i)][0]
            nodeinfo['channel'] = response.confirmation['channel_{}'.format(i)][0]
            nodeinfo['neighbor_type'] = \
                response.confirmation['neighbor_type_{}'.format(i)][0]
            nodeinfo['tx_power'] = \
                response.confirmation['tx_power_{}'.format(i)][0]
            nodeinfo['rx_power'] = \
                response.confirmation['rx_power_{}'.format(i)][0]
            nodeinfo['last_update_raw'] = \
                response.confirmation['last_update_{}'.format(i)][0]
            nodeinfo['last_update'] = datetime.datetime.now() - \
                datetime.timedelta(seconds=nodeinfo['last_update_raw'])

            retval.append(nodeinfo)

        return retval

    def start_scan_neighbor_indication_rx(self):
        '''
        Start receiving of incoming scan neighbor indication
        '''
        self.register_indication_callback(MsapNeighborScan(), self._ind_callback_gen(self.scan_neighbor_queue))

    def get_scan_neighbor_indication_rx(self, timeout=30, autoremove=False):
        '''
        Get indication (started with start_scan_neighbor_indication_rx)

        Returns:
        Dictionary with following keys:

        - 'indication_status': 0 or 1
        - 'status'

        '''
        rxpkt = self.scan_neighbor_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def start_scan_neighbors(self, timeout=30):
        '''
        Start neighbors scanning Serial API request

        Arguments:

        - timeout: timeout to wait until failure

        Returns: The response frame.
        '''

        response = None
        starttime = datetime.datetime.now()
        request = MsapStartScanNeighbors()

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        result = response.confirmation['result'][0]

        if result == ScanNeighborsResult.Success:
            return response
        elif result == ScanNeighborsResult.StackInvalidStatus:
            raise MeshApiDeviceException('The stack is in invalid status', result)
        elif result == ScanNeighborsResult.AccessDenied:
            raise MeshApiDeviceException('Access Denied', result)
        else:
            raise MeshApiDeviceException('Unknown Error', result)

    def write_sink_cost(self, cost, timeout=30):
        '''
        Write the sink cost

        Arguments:

        - timeout: Timeout to execute the command
        - cost: The sink cost

        Returns: the confirmation frame as MeshApiCommand instance.
        '''

        response = None
        starttime = datetime.datetime.now()
        request = MsapCostWrite(cost)

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        result = response.confirmation['result'][0]

        if result == SinkCostResult.Success:
            return response
        elif result == SinkCostResult.NotASink:
            raise MeshApiDeviceException('The device is not a sink', result)
        elif result == SinkCostResult.AccessDenied:
            raise MeshApiDeviceException('Access Denied', result)
        else:
            raise MeshApiDeviceException('Unknown error', result)

    def read_sink_cost(self, timeout=30):
        '''
        Read the sink cost

        Arguments:

        - timeout: Timeout to execute the command

        Returns:

        - result: success or not
        - cost: the sink cost

        '''

        response = None
        starttime = datetime.datetime.now()
        request = MsapCostRead()

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        result = response.confirmation['result'][0]

        if result == SinkCostResult.Success:
            return response
        elif result == SinkCostResult.NotASink:
            raise MeshApiDeviceException('The device is not a sink', result)
        elif result == SinkCostResult.AccessDenied:
            raise MeshApiDeviceException('Access Denied', result)
        else:
            raise MeshApiDeviceException('Unknown error', result)

    '''
    Indication poll related functions
    '''

    def stop_indication_poll(self):
        '''
        Stops the indication poll mechanism
        '''
        self.comminterface.stop_indication_poll()

    def set_indication_poll_interval(self, interval):
        '''
        Set indication poll interval to new value

        Attributes:

        - interval: Time in seconds for indication poll interval

        '''
        self.comminterface.set_indication_poll_interval(interval)

    def start_indication_poll(self):
        '''
        Starts indication poll
        '''
        self.comminterface.start_indication_poll()

    def indication_poll(self, timeout = 10):
        '''
        Send indication poll.
        NOTE! For testing only. This isn't normally needed as indication poll thread takes care of this.

        Return:
        Dictionary with following keys:

        - 'result' : return value

        '''
        response = None
        starttime = datetime.datetime.now()

        request = MsapIndicationPoll()

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = {}

        retval['result'] = response.confirmation['indication_status'][0]

        return retval

    '''
    DSAP related functions
    '''

    def data_tx(self,
                data,
                pdu_id = None,
                dest_address=ADDRESS_ANYSINK,
                src_endpoint=0,
                dst_endpoint=0,
                qos = 0,
                Req_tx_ind = False,
                timeout = 10):
        '''
        Send data frame

        Parameters:

        - data: Data payload
        - PDU ID: ID, valid value is 0-65534
        - dest_address: Address of the destination
        - src_endpoint: Source endpoint
        - dst_endpoint: Destination endpoint
        - Req_tx_ind: Indicate whether the Indication will be sent
        - timeout: Timeout in seconds for sending

        Return:
        Dictionary with following keys:

        - 'result' : return value
        - 'capacity': Amount of free items
        - 'pduid' : Generated pdu id

        '''

        response = None
        starttime = datetime.datetime.now()
        if pdu_id == None:
            pdu_id = random.randint(0,65534)
        request = DsapDataTx(pdu_id,
                             src_endpoint,
                             dest_address,
                             dst_endpoint,
                             qos,
                             Req_tx_ind,
                             data)

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = {}

        retval['result'] = response.confirmation['result'][0]
        retval['pduid'] = response.confirmation['pdu_id'][0]
        retval['capacity'] = response.confirmation['capacity'][0]

        return retval

    def start_data_rx(self, callback=None):
        '''
        Start receiving of incoming data
        '''
        if self.data_rx_indication != None:
            raise MeshApiDeviceException('Data RX already started', None)

        if callback is None:
            callback=DsapDataRx()
        indication = callback
        self.register_indication_callback(indication, self._ind_callback_gen(self.data_rx_queue))
        self.data_rx_indication = indication

    def stop_data_rx(self):
        '''
        Stop receiving of incoming data.
        '''
        if self.data_rx_indication != None:
            self.comminterface.deregister_indication(self.data_rx_indication)
            self.data_rx_indication = None
            # Clear the queue also.
            self.data_rx_queue = Queue()

    def get_data_rx(self, timeout=30):
        '''
        Get receive data (started with start_data_rx)

        Returns:
        Dictionary with following keys:

        - 'source_address'
        - 'source_endpoint'
        - 'destination_address'
        - 'destination_endpoint'
        - 'qos'
        - 'travel_time': Travel time in 1/128 seconds
        - 'travel_time_s': Travel time in seconds
        - 'apdu': Payload
        - 'rx_time': Reception time

        '''
        rxpkt = self.data_rx_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def start_data_indication_tx(self):
        '''
        Start receiving of incoming data indication
        '''
        self.register_indication_callback(DsapIndicationTx(), self._ind_callback_gen(self.data_tx_indication_queue))

    def get_data_indication_tx(self, timeout=30, autoremove=False):
        '''
        Get indication (started with start_data_indication)

        Returns:
        Dictionary with following keys:

        - 'indication_status'    0 or 1
        - 'pdu_id'
        - 'source_endpoint'
        - 'destination_address'
        - 'destination_endpoint'
        - 'buffering_delay'
        - 'result'           0 or 1

        '''
        indication = DsapIndicationTx()
        rxpkt = self.data_tx_indication_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        retval['buffering_delay_s'] = rxpkt['ind'].indication['buffering_delay'][0]/128.0

        if autoremove:
            self.comminterface.deregister_indication(indication)

        return retval

    def data_tx_tt(self,
                data,
                pdu_id = None,
                dest_address=ADDRESS_ANYSINK,
                src_endpoint=0,
                dst_endpoint=0,
                qos = 0,
                Req_tx_ind = False,
                buf_delay =0,
                timeout = 10):
        '''
        Send data frame

        Parameters:

        - data: Data payload
        - PDU ID: ID, valid value is 0-65534
        - dest_address: Address of the destination
        - src_endpoint: Source endpoint
        - dst_endpoint: Destination endpoint
        - qos: The quallity of the service
        - Req_tx_ind: Indicate whether the Indication will be sent
        - buf_delay: The time the PDU has been in the application buffers
        - timeout: Timeout in seconds for sending

        Return:
        Dictionary with following keys:

        - 'result' : return value
        - 'capacity': Amount of free items
        - 'pduid' : Generated pdu id

        '''

        response = None
        starttime = datetime.datetime.now()
        if pdu_id == None:
            pdu_id = random.randint(0,65534)
        request = DsapDataTxTT(pdu_id,
                             src_endpoint,
                             dest_address,
                             dst_endpoint,
                             qos,
                             Req_tx_ind,
                             buf_delay,
                             data)

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = {}

        retval['result'] = response.confirmation['result'][0]
        retval['pduid'] = response.confirmation['pdu_id'][0]
        retval['capacity'] = response.confirmation['capacity'][0]

        return retval

    '''
    DSAP SDU related functions
    '''
    def sdu_tx(self,
                sdu,
                payload_id = None,
                address=ADDRESS_BROADCAST,
                reserved = 0,
                timeout = 10):
        '''
        Send data frame

        Parameters:

        - sdu: Data payload
        - payload_id: ID, valid value is 0-255
        - address: Address of the destination
        - reserved: reserved
        - timeout: Timeout in seconds for sending

        Return:
        Dictionary with following keys:

        - 'result' : return value
        - 'capacity': Amount of free items
        - 'payloadid' : Generated payload id

        '''

        response = None
        starttime = datetime.datetime.now()
        if payload_id == None:
            payload_id = random.randint(0,255)
        request = DsapSDUTx(address,
                            payload_id,
                            reserved,
                            sdu)

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        retval = {}

        retval['result'] = response.confirmation['result'][0]
        #retval['payloadid'] = response.confirmation['pdu_id'][0]
        retval['reserved'] = response.confirmation['reserved'][0]

        return retval

    def start_sdu_rx(self):
        '''
        Start receiving of incoming SDU data
        '''
        if self.sdu_rx_indication != None:
            raise MeshApiDeviceException('SDU RX already started', None)

        indication = DsapSDURx()
        self.register_indication_callback(indication, self._ind_callback_gen(self.sdu_rx_queue))
        self.sdu_rx_indication = indication

    def stop_sdu_rx(self):
        '''
        Stop receiving of incoming SDU data
        '''
        if self.sdu_rx_indication != None:
            self.comminterface.deregister_indication(self.sdu_rx_indication)
            self.sdu_rx_indication = None
            # Clear the queue also.
            self.sdu_rx_queue = Queue()

    def get_sdu_rx(self, timeout=30):
        '''
        Get receive SDU (started with start_sdu_rx)

        Returns:
        Dictionary with following keys:

        - 'source_address'
        - 'payload_id'
        - 'reserved'
        - 'ap_sdu_length'
        - 'ap_sdu'

        '''
        rxpkt = self.sdu_rx_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def start_sdu_indication_tx(self):
        '''
        Start receiving of incoming sdu indication
        '''
        self.register_indication_callback(DsapSDUIndicationTx(), self._ind_callback_gen(self.sdu_tx_indication_queue))

    def get_sdu_indication_tx(self, timeout=30, autoremove=False):
        '''
        Get indication (started with start_sdu_indication)

        Returns:
        Dictionary with following keys:

        - 'indication_status'    0 or 1
        - 'destination_address'
        - 'payload_id'
        - 'reserved'
        - 'result'           0 or 1

        '''
        indication = DsapSDUIndicationTx()
        rxpkt = self.sdu_tx_indication_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        retval['buffering_delay_s'] = rxpkt['ind'].indication['buffering_delay'][0]/128.0

        if autoremove:
            self.comminterface.deregister_indication(indication)

        return retval

    '''
    App/gen config related functions
    '''

    def get_app_config_data_indication_rx(self, timeout=30):
        '''
        Get receive data (started with start_app_config_data_rx)

        Returns:
        Dictionary with following keys:

        - 'indication_status'
        - 'sequence_number'
        - 'diagnostic_data_interval'
        - 'app_config_data'

        '''
        rxpkt = self.app_config_data_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def start_app_config_data_indication_rx(self):
        '''
        Start receiving of incoming data indication
        '''
        self.register_indication_callback(AppConfDataRx(), self._ind_callback_gen(self.app_config_data_queue))

    def get_app_config_data(self, timeout=10):
        '''
        Get gen config data from the device

        Arguments:
        - timeout: How long time to ask for response

        Returns:
        Dictionary with following keys:

        - 'sequence_number': Sequence number
        - 'diagnostic_data_interval': Data interval in seconds
        - 'app_config': App config data, note: string is NOT trimmed!

        '''
        starttime = datetime.datetime.now()
        response = None

        version_data = self.get_version_numbers()

        if version_data['major'] >= 3 and version_data['minor'] >= 2:
            request = MsapAppConfigDataRead()
        else:
            request = MsapLegacyAppConfigDataRead()

        while (response == None):
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError('Timeout expired')
            response = self.comminterface.send_request(request)

        if response.confirmation['result'][0] != 0:
            raise \
                MeshApiDeviceException('Unable to get app config data, '
                                       'reason:{}'\
                                       .format(response.confirmation['result']
                                               [0]),
                                       response.confirmation['result'][0])
        retval = {}
        retval['sequence_number'] = response.confirmation['sequence_number'][0]
        retval['diagnostic_data_interval'] = \
            response.confirmation['diagnostic_data_interval'][0]
        retval['app_config_data'] = response.confirmation['app_config_data'][0]

        return retval

    def set_app_config_data(self, sequence_number, diagnostic_data_interval,
                            app_config, timeout=10):
        '''
        Set app config data for the device.

        Arguments:

        - sequence_number: Sequence number
        - diagnostic_data_interval: Diagnostic data intrval
        - app_config_data: The gen config data including
        - timeout: Amount of seconds to wait for success
        '''

        version_data = self.get_version_numbers()

        # Check the stack version and user the correct Dual-MCU primitive.
        if version_data['major'] >= 3 and version_data['minor'] >= 2:
            legacy = False
        else:
            legacy = True

        if not legacy:
            request = MsapAppConfigDataWrite(sequence_number,
                                             diagnostic_data_interval,
                                             app_config)
        else:
            request = MsapLegacyAppConfigDataWrite(sequence_number,
                                                   diagnostic_data_interval,
                                                   app_config)

        starttime = datetime.datetime.now()

        response = None

        while response is None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise RuntimeError("Timeout expired")
            response = self.comminterface.send_request(request)

        self._generate_app_config_write_exception(response, legacy)

    def _generate_app_config_write_exception(self, response, legacy):
        '''
        Internal function to convert the app config write response to an
        exception.

        Parameters:

        - response: response from the comminterface.send_request
        - legacy(bool): Legacy = True, Non-Legacy = False

        '''

        # Non-legacy result check
        if not legacy:
            if response.confirmation['result'][0] == \
               MsapAppConfigDataWriteResult.Success:
                return

            elif response.confirmation['result'][0] == \
            MsapAppConfigDataWriteResult.FailureNotASink:
                raise MeshApiDeviceException('Not a sink',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapAppConfigDataWriteResult.FailureInvalidDiagnosticDataInterval:
                raise MeshApiDeviceException('Invalid diagnostic data interval',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapAppConfigDataWriteResult.FailureInvalidSequenceNumber:
                raise MeshApiDeviceException('Invalid sequence number',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapAppConfigDataWriteResult.FailureAccessDenied:
                raise MeshApiDeviceException('Access Denied',
                                             response.confirmation['result'][0])
            else:
                raise MeshApiDeviceException('Unknown error',
                                             response.confirmation['result'][0])
        # Legacy result check
        else:
            if response.confirmation['result'][0] == \
               MsapLegacyAppConfigDataWriteResult.Success:
                return

            elif response.confirmation['result'][0] == \
            MsapLegacyAppConfigDataWriteResult.FailureNotASink:
                raise MeshApiDeviceException('Not a sink',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapLegacyAppConfigDataWriteResult.\
            FailureInvalidDiagnosticDataInterval:
                raise MeshApiDeviceException('Invalid diagnostic data interval',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapLegacyAppConfigDataWriteResult.FailureInvalidSequenceNumber:
                raise MeshApiDeviceException('Invalid sequence number',
                                             response.confirmation['result'][0])
            elif response.confirmation['result'][0] == \
            MsapLegacyAppConfigDataWriteResult.FailureAccessDenied:
                raise MeshApiDeviceException('Access Denied',
                                             response.confirmation['result'][0])
            else:
                raise MeshApiDeviceException('Unknown error',
                                             response.confirmation['result'][0])

    '''
    Diagnostics related functions
    '''

    def start_traffic_diag_rx(self):
        '''
        Start receiving of traffic diagnostics
        '''
        self.register_indication_callback(DsapTrafficDiagnostics(), self._ind_callback_gen(self.traffic_diag_queue))

    def get_traffic_diag_rx(self, timeout=30):
        '''
        Get received traffic diagnostics data

        Returns:
        Dictionary with following keys:

        - 'source_address'
        - 'source_endpoint'
        - 'destination_address'
        - 'destination_endpoint'
        - 'qos'
        - 'travel_time': Travel time in 1/128 seconds
        - 'travel_time_s': Travel time in seconds
        - 'rx_time': Reception time
        - 'access_cycles'
        - 'cluster_channel'
        - 'channel_reliability'
        - 'rx_amount'
        - 'tx_amount'
        - 'aloha_rx_ratio'
        - 'reserved_rx_success_ratio'
        - 'data_rx_ratio'
        - 'rx_duplicate_ratio'
        - 'cca_success_ratio'
        - 'broadcast_ratio'
        - 'failed_unicast_ratio'
        - 'max_reserved_slot_usage'
        - 'average_reserved_slot_usage'
        - 'max_aloha_slot_usage'

        '''
        rxpkt = self.traffic_diag_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def start_nbor_diag_rx(self):
        '''
        Start receiving of neighbor diagnostics
        '''
        self.register_indication_callback(DsapNeighborDiagnostics(), self._ind_callback_gen(self.nbor_diag_queue))

    def get_nbor_diag_rx(self, timeout=30):
        '''
        Get received neighbor diagnostics data

        Returns, the dictionary of following keys:
        'neighbors': Array of dictionary per neighbor with following keys

        - 'address'
        - 'cluster_channel'
        - 'radio_power'
        - 'node_info'
        - 'rssi'

        '''
        rxpkt = self.nbor_diag_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        # Parse through the neighbors
        return retval

    def start_node_diag_rx(self):
        '''
        Start receiving of node diagnostics
        '''
        if self.node_diag_rx_indication != None:
            raise MeshApiDeviceException('Node diagnostics RX already started', None)

        indication = DsapNodeDiagnostics()
        self.register_indication_callback(indication, self._ind_callback_gen(self.node_diag_queue))
        self.node_diag_rx_indication = indication

    def get_node_diag_rx(self, timeout=30):
        '''
        Get received node diagnostics data

        Returns, the dictionary with following keys:

        - 'access_cycle'
        - 'role'
        - 'voltage'
        - 'max_buffer_usage'
        - 'average_buffer_usage'
        - 'mem_alloc_fails'
        - 'normal_priority_buf_delay'
        - 'high_priority_buf_delay'
        - 'scans'

        Downlink delay for both high and normal QOS

        - 'dl_delay_avg_0'
        - 'dl_delay_min_0'
        - 'dl_delay_max_0'
        - 'dl_delay_samples_0'
        - 'dl_delay_avg_1'
        - 'dl_delay_min_1'
        - 'dl_delay_max_1'
        - 'dl_delay_samples_1'
        - 'dropped_packets_0'
        - 'dropped_packets_1'

        Cost info for both high and normal QOS

        - 'cost_info_sink'
        - 'cost_info_next_hop_0'
        - 'cost_info_cost_0'
        - 'cost_info_link_quality_0'
        - 'cost_info_next_hop_1'
        - 'cost_info_cost_1'
        - 'cost_info_link_quality_1'

        15 events

        - 'events_0'   0-14

        - 'duty_cycle'
        - 'current_antenna'

        '''

        rxpkt = self.node_diag_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def stop_node_diag_rx(self):
        '''
        Stop the node diagnostics receiving
        '''

        if self.node_diag_rx_indication != None:
            self.comminterface.deregister_indication(self.node_diag_rx_indication)
            self.node_diag_rx_indication = None
            # Clear the queue also.
            self.node_diag_queue = Queue()

    def start_boot_diag_rx(self):
        '''
        Starts receiving the boot diagnostics
        '''
        self.register_indication_callback(DsapBootDiagnostics(), self._ind_callback_gen(self.boot_diag_queue))

    def get_boot_diag_rx(self, timeout=30):
        '''
        Get received boot diagnostics data

        Attributes:
            timeout: Timeout in seconds to wait for diagnostics data

        Returns:
        The dictionary with following keys:

        - 'boot_count'
        - 'node_role'
        - 'sw_dev_version'
        - 'sw_maint_version'
        - 'sw_minor_version'
        - 'sw_major_version'
        - 'scratchpad_sequence'
        - 'hw_magic'
        - 'stack_profile'
        - 'otap_enabled'
        - 'boot_line_number'
        - 'file_hash'
        - 'stack_trace'

        '''
        rxpkt = self.boot_diag_queue.get(timeout=timeout)
        retval = {}

        for key, val in rxpkt['ind'].indication.iteritems():
            retval[key] = val[0]

        return retval

    def register_diagnostics_handler(self, handler):
        '''
        Register diagnostics handler

        Attributes:

        - handler: Diagnostics handler instance

        '''
        self.diagnostics_handlers.append(handler)

    '''
    Remote connection related functions
    '''

    def get_host_name(self):
        '''
        Get the host name (for remote connections).

        Returns the host name or None if not available.
        '''

        return self.comminterface.get_host_name()

    def get_port(self):
        '''
        Get the port number (for remote connections).

        Returns the port number or None if not available.
        '''

        return self.comminterface.get_port()

    def is_local(self):
        '''
        Is the connection local? (not over TCP/IP or similar)

        - Return True if connection is local, False if not.

        '''
        return self.comminterface.is_local()

    '''
    Internal functions
    '''

    def _ind_callback_gen(self, _queue):
        '''
        Used to create a callback with a queue.put so we can create queue waits for callbacks.
        '''
        def f(command):
            value = {}
            value['time'] = datetime.datetime.now()
            value['ind'] = command
            _queue.put(value)
        return f

    def _create_interface_queues(self):
        '''
        An internal function to create queues for the receiving and sending data.
        '''

        # The queues themselves.
        self.data_rx_queue = Queue()
        self.sdu_rx_queue = Queue()
        self.data_rx_tt_queue = Queue()
        self.data_tx_indication_queue = Queue()
        self.sdu_tx_indication_queue = Queue()
        self.stack_state_indication_queue = Queue()
        self.traffic_diag_queue = Queue()
        self.app_config_data_queue = Queue()
        self.nbor_diag_queue = Queue()
        self.node_diag_queue = Queue()
        self.boot_diag_queue = Queue()
        self.remote_status_queue = Queue()
        self.scan_neighbor_queue = Queue()

        # Indications related to the queues.
        self.data_rx_indication = None
        self.sdu_rx_indication = None
        self.remote_status_indication = None
        self.data_rx_tt_indication = None
        self.node_diag_rx_indication = None

    def _csap_attribute_exception_generator(self, result):
        '''
        Creates an exception based on the CSAP Attribute commands return value.

        Attributes:

        - result, The Result value from the frame.

        '''

        if result == CsapAttributeResult.Success:
            # The operation was a success. Just return.
            return
        elif result == CsapAttributeResult.FailureUnsupportedAttribId:
            exception = MeshApiDeviceException('Unsupported Attrib ID', result)
        elif result == CsapAttributeResult.FailureStackInInvalidState:
            exception = MeshApiDeviceException('Stack in invalid state', result)
        elif result == CsapAttributeResult.FailureInvalidAttributeLength:
            exception = MeshApiDeviceException('Invalid attribute length', result)
        elif result == CsapAttributeResult.FailureInvalidAttributeValue:
            exception = MeshApiDeviceException('Invalid attribute value', result)
        elif result == CsapAttributeResult.FailureAccessDenied:
            exception = MeshApiDeviceException('Access Denied', result)
        else:
            exception = MeshApiDeviceException('Unknown error: '+str(result), result)

        raise exception

    def _msap_attribute_exception_generator(self, result):
        '''
        Creates an exception based on the MSAP Attribute commands return value.

        Attributes:
        - result, The Result value from the frame.

        '''

        if result == MsapAttributeResult.Success:
            # The operation was a success. Just return.
            return
        elif result == MsapAttributeResult.FailureUnsupportedAttribId:
            exception = MeshApiDeviceException('Unsupported Attrib ID', result)
        elif result == MsapAttributeResult.FailureStackInInvalidState:
            exception = MeshApiDeviceException('Stack in invalid state', result)
        elif result == MsapAttributeResult.FailureInvalidAttributeLength:
            exception = MeshApiDeviceException('Invalid attribute length', result)
        elif result == MsapAttributeResult.FailureInvalidAttributeValue:
            exception = MeshApiDeviceException('Invalid attribute value', result)
        elif result == MsapAttributeResult.FailureWriteOnlyAttribute:
            exception = MeshApiDeviceException('Write only attribute', result)
        elif result == MsapAttributeResult.FailureAccessDenied:
            exception = MeshApiDeviceException('Access Denied', result)
        else:
            exception = MeshApiDeviceException('Unknown error: '+str(result), result)

        exception.confirmation_result = result
        raise exception

    def _tsap_exception_generator(self, result, expectedResult):
        '''
        Creates an exception based on the TSAP command return value.

        Attributes:

        - result, The Result value from the frame.
        '''
        if result == expectedResult:
            # The operation was a success. Just return.
            return
        else:
            exception = MeshApiDeviceException('Expected: '+str(expectedResult), result)

        exception.confirmation_result = result

        raise exception

    def _get_msap_attribute(self, msap_attrib_id, timeout=30):
        '''
        Gets a MSAP attribute.

        Attributes:

        - msap_attrib_id: The attribute ID.
        - timeout: timeout to wait until failure

        Returns: the confirmation frame as MeshApiCommand instance
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(MsapAttribRead(msap_attrib_id))

        # If there was an error generate an exception.
        self._msap_attribute_exception_generator(response.confirmation['result'][0])

        return response

    def _set_msap_attribute(self, msap_attrib_id, value, timeout=30):
        '''
        Sets a MSAP attribute.

        Attributes:

        - msap_attrib_id: The attribute ID.
        - value: value in binary packed format.
        - timeout: timeout to wait until failure

        Returns: the confirmation frame as MeshApiCommand instance.
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(MsapAttribWrite(msap_attrib_id, value))

        self._msap_attribute_exception_generator(response.confirmation['result'][0])
        return response

    def _get_csap_attribute(self, csap_attrib_id, timeout=30):
        '''
        Gets a CSAP attribute.

        Attributes:

        - csap_attrib_id: The attribute ID.
        - timeout: timeout to wait until failure

        Returns: the confirmation frame as MeshApiCommand instance
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(CsapAttribRead(csap_attrib_id))

        # If there was an error generate an exception.
        self._csap_attribute_exception_generator(response.confirmation['result'][0])

        return response

    def _set_csap_attribute(self, csap_attrib_id, value, timeout=30):
        '''
        Sets a CSAP attribute.

        Attributes:

        - csap_attrib_id: The attribute ID.
        - value: value in binary packed format.
        - timeout: timeout to wait until failure

        Returns: the confirmation frame as MeshApiCommand instance.
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(CsapAttribWrite(csap_attrib_id, value))

        self._csap_attribute_exception_generator(response.confirmation['result'][0])
        return response

    def _send_tsap_command(self, cmd, expectedResult=-1, timeout=30):
        '''
        Sends a TSAP command.

        Attributes:

        - cmd: command.
        - timeout: timeout to wait until failure

        Returns: the confirmation frame as MeshApiCommand instance.
        '''
        starttime = datetime.datetime.now()

        response = None
        while response == None:
            if (datetime.datetime.now() - starttime).total_seconds() > timeout:
                raise MeshApiDeviceException('Timeout expired', None)
            response = self.comminterface.send_request(request=cmd, timeout=timeout)

        if (expectedResult != -1):
            self._tsap_exception_generator(response.confirmation['result'][0], expectedResult )
        return response

    def is_connection_alive(self):
        '''
        Tells if device and connection is alive
        Returns: True: is alive, False: is not alive
        '''
        return self.comminterface.is_connection_alive()

    def get_version_numbers(self, timeout=30):
        '''
        Get all the version numbers of the device
        Args:

        - timeout: Timeout in seconds.

        Returns:

        - A dictionary with following keys: 'major', 'minor', 'maintenance','development'

        '''
        starttime = datetime.datetime.now()
        response = {}
        response['major'] = uint16_to_int(self._get_csap_attribute(CsapAttribId.FirmwareMajor, timeout=timeout).confirmation['attribute_value'][0])
        response['minor'] = uint16_to_int(self._get_csap_attribute(CsapAttribId.FirmwareMinor, timeout=timeout).confirmation['attribute_value'][0])
        response['maintenance'] = uint16_to_int(self._get_csap_attribute(CsapAttribId.FirmwareMaintenance, timeout=timeout).confirmation['attribute_value'][0])
        response['development'] = uint16_to_int(self._get_csap_attribute(CsapAttribId.FirmwareDevelopment, timeout=timeout).confirmation['attribute_value'][0])

        return response

    def __repr__(self):
        return "MeshApiDevice (Interface: "+str(self.comminterface)+")"

    # Import the BLE Beacon functions
    from meshapi_beacon_functions import beacon_request_tx, set_beacon_url, set_beacon_mode

    # Import the OTAP related functions
    from meshapi_otap_functions import get_otap_image_status, clear_otap_image, load_otap_image, \
        set_otap_image_bootable, otap_remote_status, wait_remote_status_indication_rx, \
        stop_remote_status_indication_rx, get_remote_status_indication_rx, otap_remote_update_request
